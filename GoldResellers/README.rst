CompleteDemo
============

Installation
--------------------

1) At the file includes/config.php you should do the following changes:
$Papaki_apikey="myapikey";//place your apikey here
$papaki_Post_url="https://api.papaki.gr/register_url2.aspx";


$creditpayment=true; // true if you want to recieve payments through credit cards, otherwise set it to false
$bankpayment=true; // true if you want to recieve payments through money tranfer, otherwise set it to false
$nameservers=true; // true if you want to give the option to your customers to set nameservers during the registration, otherwise set it to false.


$admin_username="admin"; // set the username of resellers Admin Panel through
which you can view the domain registrations. For security reasons set a strong username

$admin_password="admin"; // set the password of resellers Admin Panel through
which you can view the domain registrations. For security reasons set a strong  password

$admin_path="http://localhost/papaki_url/complete_demo_php/admin/admin.php";// set the path of admin.php at resellers Admin Panel (change it for security reasons)


// If you use credit card payments, set the following parameters (works only with Greek Eurobank:
$bank_merchantId="merchantid"; // It's given by the bank

$bank_password="password"; // It's given by the bank
 
$youremail="myemail@test.gr"; //your email address

$yourfax="2103333333"; //fax


// If you use money transfer payments, set the following parameters
$account1="11111111111111";

$accountname1="Citibank";

$account2="22222222222222";

$accountname2="National Bank";

$account3="33333333333333";

$accountname3="Eurobank";

 
class db_data{

var $db_conn;

function db_data() {

// Database parameters

$root="localhost"; // the host

$db_username="username"; // The database username

$db_password="password"; // The database password

$db_name="databasename"; //  The database name $this->db_conn =mysql_connect($root,$db_username,$db_password);

mysql_select_db($db_name,$this->db_conn);

}

}



2) On your browser type http://www.mydomain.gr/complete_demo_php_onlyowners/index.html , and you will see 4 useful links:

Database installation

Domain Search

Domain Renew

Admin Control Panel

If you want to install the database or to update the database of an old version click the "Database installation" button



3) Open the files header.php, footer.php, admin/header_admin.php, admin/footer_admin.php and change them according to you site design.



4) You may also change the file mail1.php. This script sends you an mail for each domain registration.



5) For the script to work without problems you have to set your servers IP through your reseller's profile. Change your profile here



6) Test environment

If you want to use the test environment open the file config.php: :

place your test apikey and change the papaki_Post_url to https://api-test.papaki.gr/register_url2.aspx

