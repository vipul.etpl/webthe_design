var base_ur = 'http://localhost/happyEdit/';
//var base_ur = 'http://globcox.com/omr/';
// Resent Email Confirmation email - YK
function resed_confirmation_email(e){                                     
   $.post(base_ur+"script/email/confirmation_email.php", {user_id: e}, function(data, status){
      if(data == "1"){
           alert("Confirmation email successfully sent, please check your inbox.");  
      }
   });
}

// Left Menu Height
function set_left_height(){
	var widht_height = $(window).height();
	widht_height = widht_height - 81;
	var widht_width = $(window).width();
	if(widht_width >= 992){
		$("#left_menu").css("height", widht_height+"px");
	}else{
		$("#left_menu").css("height", 'auto');
	}
}

$(document).ready(function(){
	set_left_height();
	$(window).resize(function(){
		set_left_height();
	});
});
