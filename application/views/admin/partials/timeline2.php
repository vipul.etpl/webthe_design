<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Dashboard</a>
    </li>
    <li class="active">Users</li>
</ol>
<div class="row" ng-controller="UserList">
    <div class="col-md-12">
        <div class="row">
            <input type="text" class="form-control" placeholder="Search Users.." ng-model="userSearch.first_name" style="margin: 0 0 12px;">

            <div class="col-sm-3 col-lg-3 col-md-3" ng-repeat="data in list | filter:userSearch">
                <div class="thumbnail no-padding">
                    <img class="img-responsive" src="http://placehold.it/320x280" alt="">
                    <div class="caption">
                        <h4><a href="javascript:void(0);">{{data.first_name}} {{data.last_name}}</a>
                        </h4>
                        <p><b>Email: </b>{{data.email}}</p>
                        <p><b>Username: </b>{{data.username}}</p>
                        <p><b>Registered Date: </b>{{data.register_date}}</p>
                    </div>
                    <div class="ratings pad btn btn-danger" ng-click="deleteThisUser({{data.user_id}})" style="width:100%;border-radius:0;">
                        <i class="fa fontello-folder-delete"></i>&nbsp;Delete                 
                    </div>
                </div>

            </div>
             <p ng-show="(list | filter:userSearch).length == 0" style="font-size: 16px;text-align: center;">No data found</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="list == 101"> 
                  No data found  
            </div>

        </div>
        <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
        </uib-pagination>

    </div>
</div>


