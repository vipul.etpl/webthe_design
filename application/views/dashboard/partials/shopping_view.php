
<div class="cart_fullwidth_table_outer_tblwrp " style="margin: 50px 20px 20px 20px;" ng-controller="cartPage" ng-init="app.settings.pagetitle = 'Cart';">
  <!-- Default panel contents -->
  <!-- <div class="panel-heading">Panel heading</div> -->

  <!-- Table -->
  <!-- <table class="table"> -->
    <?php  $cart_check = $this->cart->contents();
            
            // If cart is empty, this will show below message.
             
               ?> <!-- </div> -->
                <div>
                  <div class="col-lg-12 tpCTSLink">
                      <div class="col-lg-8 firstbx">
                        <?php
                        if(empty($cart_check)) {
                         echo  ' <p class="cartDatamsg"><span class="icon-addproduct_msg"><i class="fa fa-info-circle"></i> </span>  To add products to your shopping cart click on "Add to Cart" Button </p> '; 
             
                         }
                        ?>
                      </div>
                      <div class="col-lg-4 secondbx">
                          <a  href="<?php echo base_url()?>index.php/dashboard">  <button class="btn btn-primary pull-right tpCTS">Buy More</button></a>
                                                  
                      </div>
                  </div>
                </div>
				<div class="clearfix"></div>
				<div class="table-responsive">
                <table class="table table-bordered table-striped  cart_fullwidth_table" >
                  <?php
                  // All values of cart store in "$cart". 
                  if ($cart = $this->cart->contents()): ?>
                    <tr id= "main_heading" class="bordered_heading_tbl" >
                        <td width="10%" >Serial</td>
                        <td width="45%" >Name</td>
                        <td width="12%" >Price</td>
                        <td width="10%" >Qty</td>
                        <td width="12%" >Cost</td>
                        <td width="10%" > </td>
                    </tr>
                    <?php
                     // Create form and send all values in "shopping/update_cart" function.
                    echo form_open('shopping/update_cart');
                    $grand_total = 0;
                    $i = 1;

                    foreach ($cart as $item):
                        //   echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
                        //  Will produce the following output.
                        // <input type="hidden" name="cart[1][id]" value="1" />
                        
                        echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
                        echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
                        echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
                        echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
                        echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
                        ?>
                        <tr >
                            <td>
                       <?php echo $i++; ?>
                            </td>
                            <td>
                      <?php echo $item['name']; ?>
                            </td>
                            <td>
                                $ <?php echo number_format($item['price'], 2); ?>
                            </td>
                            <td>
                            <?php echo form_input('cart[' . $item['id'] . '][qty]', $item['qty'], 'maxlength="3" size="1" disabled style="text-align: right"'); ?>
                            </td>
                        <?php $grand_total = $grand_total + $item['subtotal']; ?>
                            <td>
                                $ <?php echo number_format($item['subtotal'], 2) ?>
                            </td>
                            <td>
                              <a ng-click="deleteSelectedRow('<?php echo $item['rowid'];?>')"><img src="<?php echo base_url()?>img/cart_cross.png" width='25px' height='25px'></a> 
                            <?php 
                            // cancle image.
                            // $path = "<img src=".base_url()."img/cart_cross.jpg width='25px' height='20px'>";
                            // echo anchor('shopping/remove/' . $item['rowid'], $path); ?>
                            </td>
                     <?php endforeach; ?>
                    </tr>
                    <tr class="bordered_footer_tbl ">
                        <td colspan='5'><b class="priceRight">Order Total: $<?php 
                        
                        //Grand Total.
                        echo number_format($grand_total, 2); ?></b></td>
                        
                        <?php // "clear cart" button call javascript confirmation message ?>
                       <td></td>
                    </tr>
					
					<tr>
					
					 <td colspan="6" class="clearCart_placeOrder_btns" > <input type="button" class ='btn btn-info' value="Clear Cart" ng-click="deleteSelectedRow('all')">
                            
                            <?php //submit button. ?>
                            <!-- <input type="submit" class ='fg-button teal' value="Update Cart"> -->
                            <?php echo form_close(); ?>
                            
                            <!-- "Place order button" on click send "billing" controller  -->
                            <!-- <input type="button" class ='btn btn-primary' value="Place Order" onclick="window.location = 'shopping/billing_view'"></td> -->
                            <a ui-sref="app.orderpage">  <input type="button" class ='btn btn-primary' value="Place Order">
						</td>
					</tr>
					
<?php endif; ?>
            </table>
  <!-- </table> -->
  
  </div>
  
  
</div>