<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Project extends CI_Controller{    

  function  __construct() {
    parent::__construct();
    $errors = array();
    $data = array();
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
   $this->load->model('projects_model');
  }
  public function index()
  {
    $this->load->view('admin/partials/components/header');
    $this->load->view('admin/partials/components/sidebar');
    $this->load->view('admin/partials/dashboard');
  }

  public function get_project_list()
  {
  	$datas = array();
	$datas=$this->projects_model->get_project_list();
      if($datas=="101")
      {
        echo "101";
      }
      else
      {
        echo json_encode($datas);
      }
  }
  public function delete_this_project($value)
  {
  	$this->projects_model->delete_this_project($value);
  }
  public function get_files($pro_id)
  {
  	$data['files']=$this->projects_model->get_project_files($pro_id);
  	//print_r($data);exit();
  	$this->load->view('admin/partials/masonry', $data);
  }

  public function get_project_files_list($dats)
  {
     echo json_encode($this->projects_model->get_project_fiels_list($dats));
  }
  public function file_price($price)
  {
    $this->projects_model->decide_file_cost($price);
  }
  public function best_design($designData)
  {
    echo $this->projects_model->best_designs($designData);
  }

  public function get_project_price($pro_name)
  {
    $data=$this->projects_model->get_projects_price($pro_name);
    echo json_encode($data);
  }

  public function getBase64String($proname,$user_id)
  {
    $data=$this->projects_model->getBase64String($proname,$user_id);
    echo $data; 
  }
  
}