
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="padding-top: 20px; padding-bottom: 5px;text-align: center;"> 
       <!-- <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>"> -->
       <h2>Happy Edit</h2>
    </div> 
</div>

</div>&nbsp;
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
             style="border: 1px solid #ccc;background-color:white">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
             
             <?php //php for register form
                    $data = array('class' => 'form-signin');
                    echo form_open('login/loginme',$data); 
            ?>
                <h2 class="form-signin-heading" align="center" style="border-bottom:1px solid #bdc5c8; color:#737369;font-size: 20px;padding-bottom:20px">Log In</h2>
                <div align="center">
                    <?php //php for show error  and succ msg

                         if(validation_errors())
                         {
                            echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                         }    
                        //end php for show error  and succ msg
                     ?>    
                    <table>
                        <tr>
                            <td>
                               <?php
                                
                                $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?> 
                            </td>
                            <td>
                                <?php
                                $data = array('type'=>'text','name' => 'email', 'class' => 'form-control', 'placeholder' => 'email/username','required' => 'autofocus', 'value' => set_value('email'));
                                echo form_input($data);
                                echo '<span class="hint">You can use your email address or username.<span class="hint-pointer">&nbsp;</span></span>';

                            ?>
                            </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                
                                    $data = '<label style="color:#737369;">Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?>
                                
                            </td>
                            <td>
                                
                                <?php
                                $data = array('type'=>'password','name' => 'password', 'class' => 'form-control', 'placeholder' => '********','required' => 'autofocus', 'value' => '');
                                echo form_input($data);
                                echo '<span class="hint">Enter your valid password.<span class="hint-pointer">&nbsp;</span></span>';

                                ?>
                                </br>
                            </td>

                        </tr>
                        <tr align="center" >
                            <td align="center" >                  
                            </td>
                            <td align="left" style="padding-bottom: 10px;">
                                <a href="<?php echo base_url(); ?>index.php/forgotpassword">Forgot your Password?</a>
                            </td>
                        </tr>

                    </table>
                   
               </div>
               <div align="center">
                 <button class="btn btn-primary" type="submit">Log In</button>
               </div>&nbsp;
          </form>
               
      
     
         </div>
   </div>
</div>
<div class="container" align="center"> 
    <div class="row" >
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            style="border: 1px solid #ccc;background-color:#f9f9f1">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="color:#737369;font-size: 15px">
                    Or Log In with:
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    &nbsp;
                </div>
            </div>
            <img class="img-circle" title="Facebook" style="cursor: pointer;" onclick="do_login()" width="40" height="30" src="<?= base_url('img/fb.png'); ?>">
            <a href="<?php echo base_url(); ?>index.php/signup/twitter"><img class="img-circle" title="Twitter" width="40" height="30" src="<?= base_url('img/t.png'); ?>"></a>
            <!-- <span  class="g-signin"
                  data-callback="loginFinishedCallback"
                  data-approvalprompt="force"
                  data-clientid="816096103751-0a3kc8l75e5e9c38a1di9f2kmppi5qe9.apps.googleusercontent.com"
                  data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read"
                  data-height="long"
                  data-cookiepolicy="single_host_origin"
                  >
                </span> -->
            <div class="row">
                <div class="col-lg-12">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            align="center" style="font-size: 13px; height: 32px; padding-top: 8px;background-color:#bfb89c;text-align:center">
            <p style="color:white">Not Registered? <a href="<?php echo base_url(); ?>index.php/signup">Create an account</a>

            </p>  
        </div>
    </div>
</div>
