<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Forgot_password_model extends CI_Model{
    // Get user id using email address 
    public function get_user_id($email){
        $this->db->select("user_id")->from("ant_lgn_details")->where('email', $email);
        $query = $this->db->get();
        $row = $query->row_array();
        return $row["user_id"];
    }
    
    // SEND PASSWORD RESET EMAIL
    public function send_password_reset_email($data, $enc_user_id){
        $salt = urldecode($data["salt"]);
        require_once("script/libmail.php");
        //Init. user email
        $mail = $data["email"];
        $message = file_get_contents("media/emails/password_reset.html");
        $message = preg_replace('/{user}/', $data["name"], $message);
        $message = preg_replace('/{user_id}/', $enc_user_id, $message);
        $message = preg_replace('/{salt}/', $data["salt"], $message);
        //Send email actions;
        $m = new Mail ();
        //Sender email
        $m->From ("admin@happyedit.com");
        //Registration user mail
        $m->To ($mail);
        //Theme
        $m->Subject ("Happy Edit Password Reset");
        //Mail content
        $m->Body ($message,"html");
        $m->Send ();
    }
    
    // Add Forgot Password record 
    public function add_forgot_password_record($user_id, $fp_id){
       $data = array();
       $data["fp_id"] = $fp_id;  
       $data["user_id"] = $user_id;  
       $data["request_date"] = date("Y-m-d h:i:s", time());  
       $this->db->insert('ant_forgot_password', $data);
       return true;
    }
    
    // Reset Password status 
    public function reset_password_status($user_id){
        $this->db->select("status")->from('ant_forgot_password')->where("user_id", $user_id);
        $query = $this->db->get();
        $row = $query->row_array();
        $data = 0;
        if($query->num_rows() > 0){
            $data = $row['status'];
        }else{
          $data = 0;
        }
        return $data;
    } 
    
    // Update the Password - YK
    public function update_my_password($user_id, $password, $salt){
        $data = array();
        $data["password"] = $password;
        $data["last_changed_by"] = $user_id;
        $data["salt"] = $salt;
        $this->db->where('user_id',$user_id);
        $this->db->update('ant_lgn_details',$data);
        
        $data1 = array();
        $data1["status"] = '2';
        $data1["reset_date"] = date("Y-m-d h:i:s", time());
        $this->db->where('user_id',$user_id);
        $this->db->update('ant_forgot_password',$data1);
        return true;
    }              
    
    // Cancel the password change request 
    public function  cancel_password_change_request($user_id){
        $data = array();
        $data["status"] = '3';
        $data["cancel_date"] = date("Y-m-d h:i:s", time()); 
        $this->db->where('user_id',$user_id);
        $this->db->update('ant_forgot_password',$data);
        return true;
    }           
}