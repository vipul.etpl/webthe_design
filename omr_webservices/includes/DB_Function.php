<?php
class DB_Functions { 
    public $db;  
    function __construct() {
        require_once 'DB_Connect.php'; 
        $this->db = new DB_Connect();
        $this->db->connect();
    } 
    function __destruct() {
         
    } 
	// Get MAX ID Foreach table- YK
	public function get_max_id($table, $id){
		$data = 0;
		$query = "SELECT MAX(".$id.") FROM ".$table."";
		if(!$result = mysql_query($query)){
			exit(mysql_error());
		}
		if(mysql_num_rows($result) > 0){
			while($row = mysql_fetch_array($result)){
				$data = $row[0] + 1;
			}
		}else{
			$data = 1;
		}
		return $data;
	} 
	
	// Validate the email  - YK
   public function validate_email($email){
	       if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email)){
  		  return false;	
		  }else{
			  return true;
		  }
	}
	
	// Validate name - YK
	public function validate_name($name){
		/*if (!preg_match("/^[a-zA-Z]*$/",$name)){ 
			 return false;
	    }else{
			return true;
		} */
        return true;
	}
	
	// Validate Password - YK
	public function validate_password($password){
		if(!preg_match("/^(?=.*\d.*\d)[0-9A-Za-z!@#$%*]{8,}$/", $password)){
			return false;
		}else{
			return true;
		}
	}
	
	// Check email uniquness - YK
	public function check_email_exist($email){
		$query = "SELECT email from `omr_lgn_details` WHERE email='$email'";
		if(!$result = mysql_query($query)){
			exit(mysql_error());
		}
		if(mysql_num_rows($result) > 0){
			return false;
		}else{
			return true;
		}
	}
    
    // Encrypt Password 
    public function encrypt_password($password){
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "password" => $encrypted);
        return $hash; 
    }
    
    // get user data - YK
    public function user_details($user_id){
        $data = array();
        $query = "SELECT * 
        FROM  `omr_register` 
        LEFT JOIN  `omr_lgn_details` ON  `omr_register`.`user_id` =  `omr_lgn_details`.`user_id` 
        WHERE  `omr_register`.`user_id` =  '$user_id'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        if(mysql_num_rows($result) > 0){ 
            while($row = mysql_fetch_array($result)){
                $data["user_id"] = $row["user_id"];
                $data["first_name"] = $row["first_name"];
                $data["last_name"] = $row["last_name"];
                $data["email"] = $row["email"];
            } 
        }
        return $data;
    }
						
    public function storeUser($name, $email, $password) {
            $uuid = uniqid('', true);
             $hash = $this->hashSSHA($password);
                $encrypted_password = $hash["encrypted"]; 
             $salt = $hash["salt"]; 
        $result = mysql_query("INSERT INTO users(unique_id, name, email, encrypted_password, salt, created_at) VALUES('$uuid', '$name', '$email', '$encrypted_password', '$salt', NOW())");
       
        if ($result) {
           
            $uid = mysql_insert_id(); 
            $result = mysql_query("SELECT * FROM users WHERE uid = $uid");
           
            return mysql_fetch_array($result);
        } else {
            return false;
        }
    }

     public function getUserByEmailAndPassword($email, $password) {
        $result = mysql_query("SELECT * FROM users WHERE email = '$email'") or die(mysql_error());
        
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            $result = mysql_fetch_array($result);
            $salt = $result['salt'];
            $encrypted_password = $result['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
           
            if ($encrypted_password == $hash) {
               
                return $result;
            }
        } else {
           
            return false;
        }
    }
 
    
    public function isUserExisted($email) {
        $result = mysql_query("SELECT email from users WHERE email = '$email'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
           
            return true;
        } else {
           
            return false;
        }
    }

    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
   
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }

    public function update_email($email,$user_id){
        $query = mysql_query("UPDATE omr_lgn_details SET email = '$email' WHERE user_id = '$user_id'");
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
    }
}


?>