<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<!--
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="text-align:center"> 
      <h2>Happy Edit</h2> 
       <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>"> 
    </div> 
</div>

</div>-->
<div class="tw-contnr">
<div class="container "> 
    <div class="row">
        <div class="col-md-12 twitter-signup-scnd-wrapp">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
       
           <?php //php for register form
                    $data = array('class' => 'form-signin twitter-signup-scnd-frm');
                    echo form_open('signup/twitter_register_user',$data); 
            ?>
                <h2 class="form-signin-heading"  >Twitter Signup</h2>
                <div align="center">
                <p style="color:#8F8F85;font-size: 14px;">Please enter your email to complete signup process .</p>
                     <?php //php for show error  and succ msg

                     if(validation_errors())
                     {
                        echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                     }    
                    //end php for show error  and succ msg
                 ?>
                    <div class="col-md-12">
                        <div class="">
                            <!--<div class="">
                               <?php
                                
                                $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?> 
                            </div>-->
                            <div class="">
                               <span class="input_icon_login"><i class="fa fa-envelope"></i></span>
                                <?php
                                $data = array('type'=>'email','name' => 'email', 'class' => 'form-control', '
                                placeholder' => 'demo@gmail.com','value' => set_value('email'));
                                echo form_input($data);
                                echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';

                            ?>
                            </br>
                            </div>
                        </div>
                    </div> 
               </div>
                
                <div align="center">
                 <button class="btn-tw-org btn-tw-snd" type="submit" >Submit</button>
                </div>

            </form>
         <div class="clearfix"></div>
         </div>
   
<div class="clearfix"></div>
   </div>
</div>
</div>
