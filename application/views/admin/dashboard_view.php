    <header class="header" ng-include src="'<?php echo base_url()?>index.php/admin/pageview/header'">
        <!-- header content -->
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left Side Column. Contains Sidebar -->
        <aside class="hidden-xs leftmenu sidebar-offcanvas" ng-include src="'<?php echo base_url()?>index.php/admin/pageview/sidebar'">
            <!-- side bar content -->
        </aside>
        <!-- End of Left Side Column. Contains Sidebar -->
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- MAIN CONTENT -->
            <section class="content">

                <div style="position:relative">
                    <div style="width:100%" ng-view ng-animate="{enter: 'view-enter', leave: 'view-leave'}"></div>

                </div>
                <!-- ./Modal profile pop up -->
                <div ng-include src="'views/profile.html'"></div>

            </section>
            <!-- ./MAIN CONTENT -->
            <footer id="footer" ng-include src="'views/footer.html'">
                <!-- footer content -->
            </footer>
        </aside>
        <!-- /.right-side -->
    </div>
    <!-- ./wrapper -->