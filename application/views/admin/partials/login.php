<div ng-controller="logiCtrl">
   
<div class="form-box" id="login-box">
        <div class="header bg-green">
            <div class="">
                <strong>Happy Edit- Admin Login</strong>
            </div>
        </div>
       
            <div class="body bg-white">
                {{errors}}
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="fontello-user-1"></i>
                        </button>
                    </span>
                    <input type="text" placeholder="User Email" id="adminUser" class="form-control">
                </div>

                <br>
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="fontello-lock"></i>
                        </button>
                    </span>
                    <input type="password" name="password" id="adminPass" class="form-control" placeholder="Password" />
                </div>
                <br>
                <button type="submit" ng-click="chkAdminInof()" class="pull-right btn btn-success ">Sign In</button>

                <div class="form-group">
                    <input type="checkbox" name="remember_me" />Remember me
                </div>
                   <hr class="timeline-hr">
            </div>

    </div>
</div>