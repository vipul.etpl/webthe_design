<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Dashboard</a>
    </li>
    <li><a href="#">Project</a>
    </li>
    <li class="active">File price Cost</li>
</ol>

<div class="row" ng-controller="FileCost">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box ">
            <div class="alert alert-success" ng-show="infovar">{{infomsg}}</div>
            <div class="box-header" style="">
                <p class="box-title">Decide each file price rate for project's whole amount.</p>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Each file cost</label>
                        <input type="text" class="form-control" id="filePrice" placeholder="Enter Amount">
                    </div>
                   
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" ng-click="filecost()">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.box -->
    
    </div>
    <!--/.col (left) -->
 
    <!--/.col (right) -->
</div>
<!-- /.row -->
