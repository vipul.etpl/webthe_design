

    <!-- jQuery UI 1.10.3 -->
    <script src="<?php echo base_url()?>js/ant_admin_js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()?>js/ant_admin_js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/weather/skycons.js"></script>
    <!-- Bonanzooka App -->
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/bonanzooka/app.js"></script>
    <!-- Bonanzooka dashboard demo (This is only for demo purposes) -->
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/bonanzooka/dashboard.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/jquery.slicknav.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/jquery.style-switcher.js"></script>
    <!-- Mansory -->
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/masonry/jquery.imagesloaded.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/masonry/jquery.wookmark.js"></script>
    <!-- fullCalendar -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
    <!-- bootstrap color picker -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <!-- Editor -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- FLOT -->
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>

    <!-- Map  -->
    <script src="http://jhere.net/<?php echo base_url()?>js/ant_admin_js/jhere-custom.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/ant_admin_js/plugins/map/gmap3.js"></script>
    

</body>

</html>
