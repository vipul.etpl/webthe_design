<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgotpassword extends CI_Controller{

  public function __construct(){
        parent::__construct();  
        $errors = array();
        $data = array();
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
    }
	public function index(){
        $data = array();
        $data['title'] = "Happy Edit - Forgot Password";
		    $this->forgot_pass($data); 
	} 

    // Default view to change password 
	public function forgot_pass(){                              
		    // $this->load->view('includes/default_header',$data); 
        $this->load->view('dashboard/partials/ui-forgotpwd');
        // $this->load->view('includes/page_end');
	}
    
    // Send email to reset the password 
    public function reset_password_email(){
        
        if($this->check_email_exist($_REQUEST['email']) == FALSE){
            $errors['error'] = 106; 
            echo json_encode($errors); 
             //return 106;
        }else{
           // GET USER ID 
           $this->load->model('forgot_password_model'); 
           $user_id = $this->forgot_password_model->get_user_id($_REQUEST['email']);
           $this->load->model('signup_model');
           $all_data = array();  
           $all_data = $this->signup_model->get_user_details($user_id);
           $enc_user_id = $this->signup_model->enrypt_user_id($user_id, $all_data["salt"]); 
           $enc_user_id = md5($enc_user_id);  
           // Add Record 
           $fp_id = $this->signup_model->get_max_id("ant_forgot_password", "fp_id");                 
           if($this->forgot_password_model->add_forgot_password_record($user_id, $fp_id)){
                $this->forgot_password_model->send_password_reset_email($all_data, $enc_user_id);
                //redirect('forgotpassword/uemail_success'); 
                //return 107;   
                $errors['error'] = 107; 
                echo json_encode($errors); 
           }                                                                                                 
        }            
    }
    
    // check user email is exist or not 
    public function check_email_exist($email){
        $this->load->model('signup_model');
        if($this->signup_model->check_email_exist($email)){
           return true;
        }else{
           //$this->form_validation->set_message("check_email_exist", "There's no Happy Edit account associated with this email address.");
           return false;
        }  
    }
    
    // Send email along with links to changes the password or cancel 
	public function uemail_success(){
		$data = array();
        $data['title'] = "Happy Edit - Mail Confirmation";
		$this->load->view('includes/default_header',$data); 
        $this->load->view('fp_email_success/fp_email_success_view',$data);
        $this->load->view('includes/page_end');
	}

    //  Default view to reset the password 
	public function reset_password($user_id){
		$data = array();
        $data['title'] = "Happy Edit - Reset Password";
        $data['user_id'] = $user_id;
		$this->load->view('includes/home/page_start',$data); 
        $this->load->view('reset_password/reset_password_view',$data);
        $this->load->view('includes/page_end');

	}
    
    // Password reset successfully. 
    public function success(){
		$data = array();
        $data['title'] = "Happy Edit - Password Reset Successfully.";
		$this->load->view('includes/home/page_start',$data); 
        $this->load->view('password_reset_success/password_reset_success_view',$data);
        $this->load->view('includes/page_end');

	}

    // RESET USER PASSWORD 
    public function rst_pwd($enc_user_id, $salt, $sts){
        // Get User ID
         $salt = urldecode($salt);
         $this->load->model('login_model');
         $user_id = $this->login_model->current_id_to_validate($enc_user_id, $salt);
        if($sts == 'true'){
            // Then Check for status 
            $this->load->model('forgot_password_model');
            if($this->forgot_password_model->reset_password_status($user_id) == "1" || $this->forgot_password_model->reset_password_status($user_id) == "2"){
                 $this->reset_password($user_id);
            }else{
                redirect('home');
            }
        }else if($sts == 'false'){
           $this->load->model('forgot_password_model'); 
           if($this->forgot_password_model->cancel_password_change_request($user_id)){
               redirect('home');
           }  
        }    
    }
    
    // Reset User Password 
    public function reset_my_password(){
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[30]');
        $this->form_validation->set_rules('cpassword', 'Re-Password', 'trim|required|matches[password]');
        if($this->form_validation->run() == FALSE){
            $this->reset_password($this->input->post('user_id'));
        }else{
            // Encryp the password 
            $this->load->model('signup_model');  
            $pdata = array();
            $pdata = $this->signup_model->encrypt_password($this->input->post('cpassword'));
            $this->load->model('forgot_password_model');  
            if($this->forgot_password_model->update_my_password($this->input->post('user_id'), $pdata["password"], $pdata["salt"])){
                redirect('forgotpassword/success');
            }else{
               $this->reset_password($this->input->post('user_id'));
            }
        }
    }

}

