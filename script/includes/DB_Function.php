<?php
class DB_Functions { 
    public $db;  
    function __construct() {
        require_once 'DB_Connect.php'; 
        $this->db = new DB_Connect();
        $this->db->connect();
    } 
    function __destruct() {
         
    } 
	// Get MAX ID Foreach table- YK
	public function get_max_id($table, $id){
		$data = 0;
		$query = "SELECT MAX(".$id.") FROM ".$table."";
		if(!$result = mysql_query($query)){
			exit(mysql_error());
		}
		if(mysql_num_rows($result) > 0){
			while($row = mysql_fetch_array($result)){
				$data = $row[0] + 1;
			}
		}else{
			$data = 1;
		}
		return $data;
	} 
	
	// Validate the email  - YK
   public function validate_email($email){
	       if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email)){
  		  return false;	
		  }else{
			  return true;
		  }
	}
    
    // Get First and last name  - YK
    public function get_fl_name($name){
        $name = explode(" ", $name);   
        $data = array(); 
        $data["first_name"] = $name[0];
        $data["last_name"] = $name[1];
        return $data;
    }
	
	// Validate name - YK
	public function validate_name($name){
		/*if (!preg_match("/^[a-zA-Z]*$/",$name)){ 
			 return false;
	    }else{
			return true;
		} */
        return true;
	}
	
	// Validate Password - YK
	public function validate_password($password){
		if(!preg_match("/^(?=.*\d.*\d)[0-9A-Za-z!@#$%*]{8,}$/", $password)){
			return false;
		}else{
			return true;
		}
	}
	
	// Check email uniquness - YK
	public function check_email_exist($email){
		$query = "SELECT email from `ant_lgn_details` WHERE email='$email'";
		if(!$result = mysql_query($query)){
			exit(mysql_error());
		}
		if(mysql_num_rows($result) > 0){
			return false;
		}else{
			return true;
		}
	}     
    
    // Check Social ID is exists
    public function check_social_id($id, $value){
       $query = "SELECT * from `ant_lgn_details` WHERE ".$id."='$value'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        if(mysql_num_rows($result) > 0){
            return true;
        }else{
            return false;
        } 
    }
    
    // Encrypt Password 
    public function encrypt_password($password){
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "password" => $encrypted);
        return $hash; 
    }
    
    // Encrypt user id to send for email verification 
    public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    }
    
    // get user data - YK
    public function user_details($user_id){
        $data = array();
        $query = "SELECT * 
        FROM  `ant_register` 
        LEFT JOIN  `ant_lgn_details` ON  `ant_register`.`user_id` =  `ant_lgn_details`.`user_id` 
        WHERE  `ant_register`.`user_id` =  '$user_id'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        if(mysql_num_rows($result) > 0){ 
            while($row = mysql_fetch_array($result)){
                $data["user_id"] = $row["user_id"];
                $data["first_name"] = $row["first_name"];
                $data["last_name"] = $row["last_name"];
                $data["email"] = $row["email"];
                $data["salt"] = $row["salt"];
                $data["name"] = $row["first_name"]. " " . $row["last_name"]; 
                $data["facebook_id"] = $row["facebook_id"];
                $data["google_id"] = $row["google_id"];
                $data["twitter_id"] = $row["twitter_id"];  
                $data["lgn_id"] = $row["lgn_id"];
                $data["username"] = $row["username"];  
                $data["first_name"] = $row["first_name"];
                $data["register_date"] = $row["register_date"];
                $data["last_login"] = $row["last_login"];
                $data["is_email"] = $row["is_email"];
                $data["last_updated"] = $row["last_updated"];
            } 
        }
        return $data;
    }
    
    // UPDATE Social ID 
    public function update_social_id($field, $id, $user_id){    
        $query = "UPDATE ant_lgn_details SET ".$field."='$id' WHERE user_id='$user_id'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        return true;
    }
    
    // Get User ID using email address 
    public function get_user_id_by_email($email){
        $query = "SELECT user_id FROM ant_lgn_details WHERE email='$email'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        $data = "";
        if(mysql_num_rows($result) > 0){
           while($row = mysql_fetch_array($result)){
               $data = $row["user_id"];
           } 
        }
        return $data;
    }
    
    // Insert Register data
    public function insert_register_data($data){
        $query = "INSERT INTO ant_register(user_id, first_name, last_name, register_date, is_email) VALUES('{$data['user_id']}','{$data['first_name']}','{$data['last_name']}','{$data['register_date']}', '1')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        return true;
    }    
    
    // insert Login Data
    public function insert_login_data($data){
        $query = "INSERT INTO ant_lgn_details(lgn_id, user_id, email, google_id, password, salt) VALUES('{$data['lgn_id']}','{$data['user_id']}','{$data['email']}','{$data['google_id']}','{$data['password']}','{$data['salt']}')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        return true;
    }
    
     // Google - Generate Password to send 
    public function generate_password($length = 8){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ123456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    //**************Insert Vendor Name With Id**********************
        public function store_vender($data){
        $query = "INSERT INTO  omr_vender_master(vender_id,vender_name, added_by, added_date) VALUES('{$data['vender_id']}','{$data['vender_name']}','{$data['added_by']}','{$data['added_date']}')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        return true;
        }

        public function insert_vender_info($data){
            $vender_id = $this->get_max_id("omr_vender_master","vender_id");
            $vender_info = array();
            $vender_info["vender_id"] = $vender_id;
            $vender_info["vender_name"] = $data["vender_name"];
            $vender_info["added_by"] = $data["added_by"];
            $vender_info["added_date"] = date("Y-m-d h:i:s", time());
            $vender_info["last_changed_by"] = $data["user_id"];
            if ($this->store_vender($vender_info)) {
                return $vender_id;
            }
        }

        public function check_vendor_exist($vender_name){
        $query = "SELECT vender_name from `omr_vender_master` WHERE vender_name='$vender_name'";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        if(mysql_num_rows($result) > 0){
            return true;
        }else{
            return false;
        }
    }    
    //**************Insert Vendor Name With Id************************
    
    // ###################################### Google Registration ###########################################
    public function store_google_register_data($data){
        $user_id = $this->get_max_id("ant_register", "user_id");
        $register_data = array();   
        $register_data["user_id"] = $user_id;
        $register_data["first_name"] = $data['first_name'];
        $register_data["last_name"] = $data['last_name'];
        $register_data["register_date"] = date("Y-m-d h:i:s", time());
        if($this->insert_register_data($register_data)){
            return $user_id;
        }                                                   
    } 
    
     // Google Welcome email
        public function google_welcome_email($email, $name , $password){
            require_once("../libmail.php");
            //Init. user email
            $mail = $email;
            $message = file_get_contents("../../media/emails/google_welcome.html");
            $message = preg_replace('/{user}/', $name, $message);
            $message = preg_replace('/{email}/', $email, $message);
            $message = preg_replace('/{password}/', $password, $message);
            //Send email actions;
            $m = new Mail ();
            //Sender email
            $m->From ("anything@admixtion.com");
            //Registration user mail
            $m->To ($mail);
            //Theme
            $m->Subject ("Welcome to One Minute Receipt");
            //Mail content
            $m->Body ($message,"html");
            $m->Send ();
        }
        
        public function google_store_login_data($data){
            $lgn_id = $this->get_max_id("ant_lgn_details", "lgn_id");
            $user_id = $this->store_google_register_data($data);   
            $pwd = $this->generate_password();   
            $pdata = $this->encrypt_password($pwd); 
            $login_data = array();
            $login_data["lgn_id"] = $lgn_id;
            $login_data["user_id"] = $user_id;
            $login_data["email"] = $data["email"];
            $login_data["google_id"] = $data["google_id"];
            $login_data["password"] = $pdata["password"];
            $login_data["salt"] = $pdata["salt"];
            if($this->insert_login_data($login_data)){
                $this->google_welcome_email($data["email"], $data["name"], $pwd); 
                return $user_id;   
            }        
        }
    
    
    // ###################################### /Google Registration ##########################################
	
    //////////////////// ###################### Below functions is unused ############################## ///////
    					
    public function storeUser($name, $email, $password) {
            $uuid = uniqid('', true);
             $hash = $this->hashSSHA($password);
                $encrypted_password = $hash["encrypted"]; 
             $salt = $hash["salt"]; 
        $result = mysql_query("INSERT INTO users(unique_id, name, email, encrypted_password, salt, created_at) VALUES('$uuid', '$name', '$email', '$encrypted_password', '$salt', NOW())");
       
        if ($result) {
           
            $uid = mysql_insert_id(); 
            $result = mysql_query("SELECT * FROM users WHERE uid = $uid");
           
            return mysql_fetch_array($result);
        } else {
            return false;
        }
    }

     public function getUserByEmailAndPassword($email, $password) {
        $result = mysql_query("SELECT * FROM users WHERE email = '$email'") or die(mysql_error());
        
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            $result = mysql_fetch_array($result);
            $salt = $result['salt'];
            $encrypted_password = $result['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
           
            if ($encrypted_password == $hash) {
               
                return $result;
            }
        } else {
           
            return false;
        }
    }
 
    
    public function isUserExisted($email) {
        $result = mysql_query("SELECT email from users WHERE email = '$email'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
           
            return true;
        } else {
           
            return false;
        }
    }

    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
   
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }
 
}


?>