<style type="text/css">
.modal-backdrop{display: none;}
.modal-dialog {position: relative; z-index: 2147483647;}
.fade.in {opacity: 1; position: absolute; width: 100%;}
</style>
	<div class="wrapper">
		<header class="top_header">
			<div class="full-width-video__element">
				<!--<div class="vid"></div>-->
	    		<video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
	    			<source type="video/webm" src=""></source>
	    			<source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
	    		</video>
	    	</div>

	    	<div class="head_wrap">

	    		


		    	<div class="container">
		    		<div class="row">
		    		<!--	<div class="col-md-2 logo"><a href="#">Happy Edit</a></div>-->
		    			

		    			
		    		</div>

		    		<div class="header_content">
			        	<div class="play"><a href="#"><i class="fa fa-play-circle"></i></a></div>
			        	<div class="hand_text">Designing Websites Couldn't be Easier</div>
			        	<!-- <div class="hand_text_btn">
			        		<button class="hvr-shutter-out-horizontal" style="outline:none !important;">Start Designing</button>
			        	</div> -->
			        	<div class="ser_section">
			        		<div class="large-search" am-grid-row="l:start" am-grid-col="l:pre1 l:10">
			        			<input type="text" placeholder="Find a service: eg. 'Logo Design'" name="search[query]" id="search_query" class="large-search__body" vk_16c85="subscribed">
			        			<button type="submit" class="button button--green large-search__button">
			        				<span class="large-search__button__label">Search</span>
			        			</button>
			        		</div>
			        	</div>
			        	<div class="links_wrapper">
			        		<p class="lh-flexed--spaced s-no-below th-size-medium">
		        				<a href="#">Install WordPress Theme</a>
		        				<a href="#">Flyer Design</a>
		        				<a href="#">App Development</a>
		        				<a href="#">Business Cards</a>
		        			</p>
			        	</div>
			        </div>

		         </div>
	        </div> 

	        <div class="clear"></div>
	    </header>    



	    <div class="fixed_wrapper_homepage">
	    	<div class="container">
	    		<ul class="lh-flexed--spaced">
	    			<li class="lh-inline-block"><a href="#">Logo Design &amp; Branding</a></li>
	    			<li class="lh-inline-block"><a href="#">WordPress</a></li>
    				<li class="lh-inline-block"><a href="#">Design &amp; Graphics</a></li>
    				<li class="lh-inline-block"><a href="#">Websites &amp; Programming</a></li>
    				<li class="lh-inline-block"><a href="#">Business &amp; Marketing</a></li>
    				<li class="lh-inline-block"><a href="#">Video &amp; Animation</a></li>
    				<li class="lh-inline-block"><a href="#">Voice-overs</a></li>
	    		</ul>
	    	</div>
	    </div>

	    <section class="cata_wrapper">
	    	<div class="container">
		    	<h3 class="cata_heading">Recent Designs</h3>
		    	<div class="cata_all">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img2.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6  col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img3.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img4.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img2.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img3.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-3 ct">
							<img src="<?php echo base_url()?>img/ant_home_img/img4.jpg">
							<p><a href=""> Voice-overs</a></p>
						</div>
					</div>
	    		</div>
	    	</div>	
	    </section>

	    <section class="pro_wrapper">
	    	<div class="container">
		    	<h3 class="cata_heading">Website of the Month</h3>
			    <div class="cata_all">
			    	<div class="row">
			    		<div class="col-md-12 pr">
			    			<img src="<?php echo base_url()?>img/ant_home_img/pro.jpg">
			    			<div class="popular_lt_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
			    			<div class="popular_rt_text">
			    				<span><a href=""><i class="fa fa-play-circle"></i></a></span>
			    				<span>Lorem Ipsum is simply dummy text</span>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</div>    
	    </section>

	    <section class="pur_wrapper">
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-6 tw">
	    				<div class="lt_img"><img src="<?php echo base_url()?>img/ant_home_img/1.png"></div>
	    				<div class="rt_text">Lorem Ipsum is simply dummy text of the printing and type setting industry. </div>
	    			</div>
	    			<div class="col-md-6 tw">
	    				<div class="lt_img"><img src="<?php echo base_url()?>img/ant_home_img/2.png"></div>
	    				<div class="rt_text">Lorem Ipsum is simply dummy text of the printing and type setting industry. </div>
	    			</div>
	    		</div>
	    	</div>
	    </section>

	    <section class="easy_wrapper">
	    	<div class="container">
				<div class="easy_header">
					<span>Lorem Ipsum is simply dummy</span> text of the printing and typesetting
				</div>
				<div class="row">
					<div class="slider">
						<div id="owl-example" class="owl-carousel">
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="img/pic1.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="img/pic2.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="img/pic3.jpg" alt=""></div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="img/pic4.jpg" alt="">
			                </div>
			                <div class="item">
			                	<div class="overaly"><p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting lorum.</p></div>
			                	<img src="img/pic5.jpg" alt=""></div>
		              	</div>
				    </div>
				</div>    
			</div>		    	
		</section>

		<section class="work_wrapper">
			<div class="container bg">
				<div class="row">
					<div class="col-md-10 wk">
						<h2>Lorem Ipsum is simply dummy</h2>
						<h3>text of the printing <br>and typesetting text of <br>the printing and type setting text</h3>
					</div>
					<div class="col-md-2 rt_oic">
						<img src="<?php echo base_url()?>img/ant_home_img/pic6.jpg" alt="">
						<p>and typesetting text of the printing and type setting text</p>
					</div>
				</div>	
			</div>
		</section>

		<section class="easy_wrapper padbt">
			<div class="container">
				<div class="large-search_bor ">
					<input type="text" vk_16c85="subscribed" class="large-search__body" id="search_query" name="search[query]" placeholder="What job can we help you with?">
					<button class="button button--green large-search__button" type="submit">
						<span class="large-search__button__label">Search</span>
					</button>
				</div>
			</div>
		</section>

		<section class="bg_img_full">
			<div class="container">
				<div class="row">
					<div class="col-md-9 wk">
						<h2>Lorem Ipsum is simply dummy</h2>
						<h3>text of the printing <br>and typesetting text of <br>the printing and typesettingtext</h3>
					</div>
					<div class="col-md3 rt_oic">
						<a href="#">Happy Edit</a>
					</div>
				</div>
			</div>
		</section>

		<footer class="footer_wrapper">
			<div class="container">
		<!---
			
				<ul class="footer-list col-md-3">
					<li class="footer-list_label">About Studio</li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">How It Works</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">FAQ</a></li>
					<li class="footer-list_item"><a href="#" rel="nofollow">Contact Us</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">About Us</a></li>
					<li class="footer-list_item"><a href="#">Testimonials</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Blog</a></li>
					<li class="footer-list_item"><a href="#">Terms &amp; Conditions</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Content Policy</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Privacy Policy</a></li>
				</ul>
				<ul class="footer-list col-md-3">
					<li class="footer-list_label">Categories</li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">How It Works</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">FAQ</a></li>
					<li class="footer-list_item"><a href="#" rel="nofollow">Contact Us</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">About Us</a></li>
					<li class="footer-list_item"><a href="#">Testimonials</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Blog</a></li>
					<li class="footer-list_item"><a href="#">Terms &amp; Conditions</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Content Policy</a></li>
					<li class="footer-list_item"><a href="#" class="footer-list__link">Privacy Policy</a></li>
				</ul>

				<ul class="footer-list col-md-3">
					<li class="footer-list_label">Facebook Feed</li>
					<span class="social">
						<a href=""><span class="soci hi-icon"><i class="fa fa-twitter"></i></span></a>
						<a href=""><span class="soci hi-icon"><i class="fa fa-facebook"></i></span></a>
						<a href=""><span class="soci hi-icon"><i class="fa fa-linkedin"></i></span></a>
						<a href=""><span class="soci hi-icon"><i class="fa fa-google-plus"></i></span></a>
						<a href=""><span class="soci hi-icon"><i class="fa fa-pinterest-p"></i></span></a>
					</span>
					<span class="social">
						<img src="<?php echo base_url()?>img/ant_home_img/fb.jpg">
						<iframe frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width:250px; height:220px;" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/exceptionairetechnologies&amp;width=250&amp;height=220&amp;show_faces=false&amp;colorscheme=light&amp;stream=true&amp;show_border=true&amp;header=true"></iframe>
					</span>

				</ul>

				<ul class="footer-list col-md-3">
					<li class="footer-list_label">RSS Feed</li>
					<img src="<?php echo base_url()?>img/ant_home_img/rs.jpg">
					<iframe width="250" height="220" frameborder="0" hspace="0" vspace="0" marginheight="0" marginwidth="0" name="rssmikle_frame" scrolling="no" src="http://feed.mikle.com/widget/?rssmikle_url=http%3A%2F%2Fwww.searchenginejournal.com%2Ffeed%2F&amp;rssmikle_frame_width=250&amp;rssmikle_frame_height=220&amp;frame_height_by_article=0&amp;rssmikle_target=_blank&amp;rssmikle_font=Arial%2C%20Helvetica%2C%20sans-serif&amp;rssmikle_font_size=12&amp;rssmikle_border=off&amp;responsive=off&amp;text_align=left&amp;text_align2=left&amp;corner=off&amp;scrollbar=on&amp;autoscroll=on&amp;scrolldirection=up&amp;scrollstep=3&amp;mcspeed=20&amp;sort=Off&amp;rssmikle_title=on&amp;rssmikle_title_sentence=Did%20you%20know%3F&amp;rssmikle_title_bgcolor=%23FF531F&amp;rssmikle_title_color=%23FFFFFF&amp;rssmikle_item_bgcolor=%23FFFFFF&amp;rssmikle_item_title_length=55&amp;rssmikle_item_title_color=%230F0502&amp;rssmikle_item_border_bottom=on&amp;rssmikle_item_description=on&amp;item_link=off&amp;rssmikle_item_description_length=150&amp;rssmikle_item_description_color=%23666666&amp;rssmikle_item_date=gl1&amp;rssmikle_timezone=Etc%2FGMT&amp;datetime_format=%25b%20%25e%2C%20%25Y%20%25l%3A%25M%20%25p&amp;item_description_style=text%2Btn&amp;item_thumbnail=full&amp;item_thumbnail_selection=auto&amp;article_num=15&amp;rssmikle_item_podcast=off&amp;"></iframe>

				</ul>-->
								
				<div class="sub_footer">
					<div class="col-md-6 copyright_wrp">Copyright @ 2016 <a href="#"> Happy Edit.</a> All Rights Reserved</div>
					<div class="col-md-6 footer_last_ul"> 
					<ul class="ft">
					<li><a href="#">Home <span> |</span></a> </li>
					<li><a href="#">About us <span> |</span></a> </li>
					<li><a href="#">Term Conditions <span> |</span></a> </li>
					<li><a href="#">Contact us </a> </li>
					
					</ul>
					<div class="clearfix"></div>
					</div>
				</div>

			</div>
		</footer>

	<p id="back-top"><a href="#tops" onclick="goTop()"><i class="fa fa-arrow-up"></i></a></p>
</div>
<script>
function goTop() {
    //location.hash = "#" + hash;
   $('html, body').animate({scrollTop: "0px"}, 1000);
}

</script>