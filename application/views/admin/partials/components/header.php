<!-- TOP NAVBAR -->
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="hidden-xs navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="hidden-xs logo">Happyedit</div>

    <!-- search form -->


    <!-- /.search form -->
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fontello-user-outline"></i>
                    
                </a>
                <ul class="dropdown-menu bg-red">
                 <li>
                        <ul class="menu text-white">
                            <li>

                                <a href="#" style="color:#fff;">
                                   Logout
                                </a>
                            </li>
                            
                        </ul>
                    </li>
                   
                </ul>
            </li> -->
            <!-- Messages: style can be found in dropdown.less-->
            
            <!-- Notifications: style can be found in dropdown.less -->
            
            <!-- Tasks: style can be found in dropdown.less -->
            <!-- <li class="">
                <span class="chat-toggle dropdown-toggle" data-toggle="dropdown">
                    <i class="text-gray fa fontello-th"></i>
                </span>

                <ul class="dropdown-menu pull-right chat-list nano" id="dropdown-right" role="menu">
                    <div class="nano-content">
                        <li class="list-title"><i class="fontello-chat-alt"></i>&nbsp;&nbsp;
                            <strong>Announcement</strong>
                        </li>

                        <li class="list-group-item no-border flat bg-transparent">
                            <span class="fa-stack pull-left list-left fa-lg text-green">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fontello-graduation-cap fa-stack-1x fa-inverse"></i>
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Fill out profile</a> Provide more information for your profile
                                <p>
                                    <small class=" fontello-clock">13 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item no-border flat bg-transparent">
                            <span class="fa-stack pull-left list-left fa-lg text-red">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-share-square-o fa-stack-1x fa-inverse"></i>
                            </span>
                            <span class="clear"> <a class="text-red" href="#">Complete Your Commit</a> please check your lastest job commit
                                <p>
                                    <small class=" fontello-clock">20 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item no-border flat bg-transparent">
                            <span class="fa-stack pull-left list-left fa-lg text-blue">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fontello-ticket fa-stack-1x fa-inverse"></i>
                            </span>
                            <span class="clear">
                                <a class="text-blue" href="#">
                                    <strong>Meeting Invitation</strong>
                                </a>Let's join general meeting on Monday
                                <p>
                                    <small class=" fontello-clock">1 day ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-title margin-center"><i class="fontello-chat-alt"></i>&nbsp;&nbsp;
                            <strong>Chat</strong>
                        </li>
                        <li>
                            <span class="bg-yellow flat-radius">Online</span>
                        </li>

                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/30.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Lester Palmer</a>
                                <p>
                                    <small class=" fontello-clock">45 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/31.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Harold Flores</a>
                                <p>
                                    <small class=" fontello-clock">33 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-circle" src="http://api.randomuser.me/portraits/thumb/men/32.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Jacqueline Williamson</a>
                                <p>
                                    <small class=" fontello-clock">31 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li>
                            <span class="bg-gray flat-radius text-white">Offline</span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-offline  img-circle" src="http://api.randomuser.me/portraits/thumb/women/33.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Logan Knight</a>
                                <p>
                                    <small class=" fontello-clock">28 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-offline  img-circle" src="http://api.randomuser.me/portraits/thumb/men/34.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Virgil Jennings</a>
                                <p>
                                    <small class=" fontello-clock">26 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-offline  img-circle" src="http://api.randomuser.me/portraits/thumb/women/35.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Raisa Ilove You</a>
                                <p>
                                    <small class=" fontello-clock">13 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-offline  img-circle" src="http://api.randomuser.me/portraits/thumb/women/36.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Jeniffer Anintoon</a>
                                <p>
                                    <small class=" fontello-clock">13 minutes ago</small>
                                </p>
                            </span>
                        </li>
                        <li style="margin-bottom:100px" class="list-group-item flat no-border">
                            <span class="pull-left list-left">
                                <img alt="" class="img-chat img-offline  img-circle" src="http://api.randomuser.me/portraits/thumb/men/37.jpg">
                            </span>
                            <span class="clear"> <a class="text-green" href="#">Ryan Wilson</a>
                                <p>
                                    <small class=" fontello-clock">13 minutes ago</small>
                                </p>
                            </span>
                        </li>
                    </div>
                </ul>
            </li> -->
        </ul>
    </div>
</nav>






<!-- /END OF TOP NAVBAR -->
<!-- <script src="js/plugins/nicescroll/jquery.nanoscroller.js"></script> -->

<script>
//Enable sidebar toggle
$("[data-toggle='offcanvas']").click(function() {

    if ($(this).hasClass('active')) {
        $(this).removeClass('active')

        $('.leftmenu').animate({
            left: 0
        }, 500);
        $(".navbar").animate({
            left: 0
        }, 500);

        $(".right-side").animate({
            "margin-left": "220px"
        }, 500);
    } else {
        $(this).addClass('active')
        //Else, enable content streching
        $('.leftmenu').animate({
            left: -220
        }, 500);

        $(".navbar").animate({
            left: -220
        }, 500);
        $(".right-side").animate({
            "margin-left": "0px"
        }, 500);
    }
    return false;
});


// show skin select for a second
setTimeout(function() {
    $("[data-toggle='offcanvas']").addClass('active').trigger('click');
}, 10)

$("[data-toggle='tooltip']").tooltip();

/*
 * ADD SLIMSCROLL TO THE TOP NAV DROPDOWNS
 * ---------------------------------------
 */
$(".navbar .menu").slimscroll({
    height: "200px",
    alwaysVisible: false,
    size: "3px"
}).css("width", "100%");

(function($) {
    $(".nano").nanoScroller();
})(jQuery);

$('.header-changer').styleSwitcher({
             manageCookieLoad: false
        });
</script>
