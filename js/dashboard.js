var base_ur = 'http://localhost/omr/';

 function receipt_title(){

var receipt_var = document.getElementById("create_recipt").value;

document.getElementById("myModalLabel").innerHTML=receipt_var;
document.getElementById("rece_name").value=receipt_var;
document.getElementById("panel").value=receipt_var;

}


function change_text()
{
  var change_text = document.getElementById("panel").value;
  //alert(change_text);
  document.getElementById("myModalLabel").innerHTML=change_text;
  document.getElementById("rece_name").value=change_text;
}


//********************************************************************************************
    $(function() {
    $( "#datepicker" ).datepicker({
      dateFormat : 'yy-mm-dd'
      
    });
  });
   $(function() {
  $( "#datepicker1" ).datepicker({
    dateFormat : 'yy-mm-dd',
     maxDate: "+currentDate",
   
    
  });
});

//**********************Autocomplete****************************************
$(document).ready(function(){
  $(document).click(function(){
    $("#ajax_response").fadeOut('slow');
  });
  $("#keyword").focus();
  var offset = $("#keyword").offset();
  var width = $("#keyword").width()-2;
  $("#ajax_response").css("left",offset.left); 
  $("#ajax_response").css("width",width);
  $("#keyword").keyup(function(event){
     //alert(event.keyCode);
     var keyword = $("#keyword").val();
     if(keyword.length)
     {
       if(event.keyCode != 40 && event.keyCode != 38 && event.keyCode != 13)
       {
         $("#loading").css("visibility","visible");
         $.ajax({
           type: "POST",
           url: "http://localhost/omr/script/autosearch/search.php",
           data: "data="+keyword,
           success: function(msg){  
          if(msg != 0)
            $("#ajax_response").fadeIn("slow").html(msg);
          else
          {
            $("#ajax_response").fadeIn("slow"); 
            $("#ajax_response").html('<div style="text-align:left;">No Matches Found</div>');
          }
          $("#loading").css("visibility","hidden");
           }
         });
       }
       else
       {
        switch (event.keyCode)
        {
         case 40:
         {
            found = 0;
            $("li").each(function(){
             if($(this).attr("class") == "selected")
              found = 1;
            });
            if(found == 1)
            {
            var sel = $("li[class='selected']");
            sel.next().addClass("selected");
            sel.removeClass("selected");
            }
            else
            $("li:first").addClass("selected");
           }
         break;
         case 38:
         {
            found = 0;
            $("li").each(function(){
             if($(this).attr("class") == "selected")
              found = 1;
            });
            if(found == 1)
            {
            var sel = $("li[class='selected']");
            sel.prev().addClass("selected");
            sel.removeClass("selected");
            }
            else
            $("li:last").addClass("selected");
         }
         break;
         case 13:
          $("#ajax_response").fadeOut("slow");
          $("#keyword").val($("li[class='selected'] a").text());
         break;
        }
       }
     }
     else
      $("#ajax_response").fadeOut("slow");
  });
  $("#ajax_response").mouseover(function(){
    $(this).find("li a:first-child").mouseover(function () {
        $(this).addClass("selected");
    });
    $(this).find("li a:first-child").mouseout(function () {
        $(this).removeClass("selected");
    });
    $(this).find("li a:first-child").click(function () {
        $("#keyword").val($(this).text());
        $("#ajax_response").fadeOut("slow");
    });
  });
});
//*************************////Autocomplete************************************************

//********** Save Receipt with validation and ajax**********************
function save_receipt(user_id){
  var expense_type_id = ('#myDropdown').val();
  var category_name = $("#exp_typ").val();
  var receipt_date = $("#datepicker").val();
  var vender_name = $("#keyword").val();
  var description = $("#rec_des").val();
  var total_amount = $("#rec_tot").val();
  var img = $("#rec_img").val();
  
  if(category_name == "")
  {
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Provied expense type.</div>');
    $("#exp_typ").focus();
  }else if(receipt_date == "")
  {
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Select Date First.</div>');
    $("#datepicker").focus();
  }else if(vender_name == ""){
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Select Vendor through Autocomplete box.</div>');
    $("#keyword").focus();
  }else if(description == ""){
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Provide some Description.</div>');
    $("#rec_des").focus();
  }else if(total_amount == ""){
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Provide Amount.</div>');
    $("#rec_tot").focus();
  }else if(img == ""){
    $("#signup_status").html('<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Warning!</strong>Upload Image.</div>');
    $("#rec_img").focus();
  }
  else{
        $.post(base_ur+"script/autosearch/receipt_add.php",{user_id: user_id,expense_type_id: expense_type_id,category_name: category_name,receipt_date: receipt_date,vender_name: vender_name,description: description,total_amount: total_amount,img: img,page:'receipt'},function(data,status){
          $("#signup_status").html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Well Done!</strong>Receipt Save  successfuly.</div>');
          alert(data);
        });
 
  }
}
//********** ////Save Receipt with validation and ajax**********************


//*******************Dropdown list*******************************
  $(document).ready(function() {
    $('div#exp_typ1').load('../script/autosearch/expense_dropdown.php');
  });
//&********************//Dropdown list****************************

function expense_typ_name(){

  var receipt_var = document.getElementById("exp_name").value;
  document.getElementById("exp_val").innerHTML=receipt_var;
}


 //*************************Slid Up and Down in receipt name change***************************************
  $(document).ready(function(){
    $("#flip").click(function(){
      $("#panel").slideDown("slow");
  });

    $("#panel").on('change',function(){
      $("#panel").slideUp("slow");
  });
});
//****************************************************************


function update_receipts_name(receipt_id)
{
  //alert(expense_type_id);
  var receipt_name = document.getElementById('receipt_name'+receipt_id).value;
  //alert(category_name);
  $.post(base_ur+"script/receipts/update_receipts_name.php", {receipt_name: receipt_name,receipt_id: receipt_id}, function(data, status){
    //alert(data);
    $("#signup_status").html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Well Done!</strong>Receipts name changed successfuly.</div>');
    //location.reload();
          window.setInterval("reFresh()",600);
      });
}


function delete_receipts_name(receipt_id)
{
  if(confirm("Are You Sure To Delete This Receipts."))
  {
    delete_receipts_name_com(receipt_id);
  }
}

function delete_receipts_name_com(receipt_id)
{
  //alert(receipt_id);
  $.post(base_ur+"script/receipts/delete_receipts_name.php", {receipt_id: receipt_id}, function(data, status){
    //alert(data);
    $("#signup_status").html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Well Done!</strong>Receipts deleted successfuly.</div>');
    //location.reload();
          window.setInterval("reFresh()",600);
      });
}


function reFresh()
 {
     location.reload(true)
 }