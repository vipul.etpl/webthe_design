<!DOCTYPE html>
<html lang="en" ng-app="happyEditAdmin">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="themesmile" />
    <title><?php echo $title;?></title>
    <meta name="description" content="Happy Edit - Admin">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <!-- add styles -->
    <!-- bootstrap 3.2.0 Latest compiled and minified CSS -->
    <link href="<?php echo base_url()?>css/ant_admin_css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url()?>css/ant_admin_css/slicknav.css">
    <!-- font Awesome -->
    <link href="<?php echo base_url()?>css/ant_admin_css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url()?>css/ant_admin_css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>css/ant_admin_css/icons-payment.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>css/ant_admin_css/flag-icon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>css/ant_admin_css/typicons.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url()?>css/ant_admin_css/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url()?>css/ant_admin_css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="<?php echo base_url()?>css/ant_admin_css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo base_url()?>css/ant_admin_css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url()?>css/ant_admin_css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url()?>css/ant_admin_css/style.css" rel="stylesheet" type="text/css" />
    <!-- Flag -->
    <link href="<?php echo base_url()?>css/ant_admin_css/bonanzooka.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>css/ant_admin_css/mail.css" rel="stylesheet" type="text/css" />

    <link type="text/css" rel="stylesheet" id="theme" href="<?php echo base_url()?>css/ant_admin_css/theme2.css" />
    <!--  Mansory -->
    <link rel="stylesheet" href="<?php echo base_url()?>css/ant_admin_css/masonry/main.css">
    <!-- fullCalendar -->
    <link href="<?php echo base_url()?>css/ant_admin_css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>css/ant_admin_css/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
    <!-- daterange picker -->
    <link href="<?php echo base_url()?>css/ant_admin_css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?php echo base_url()?>css/ant_admin_css/iCheck/all.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Color Picker -->
    <link href="<?php echo base_url()?>css/ant_admin_css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <!-- Bootstrap time Picker -->
    <link href="<?php echo base_url()?>css/ant_admin_css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" />
    <!-- Editor -->
    <link href="<?php echo base_url()?>css/ant_admin_css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?php echo base_url()?>css/ant_admin_css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Morris charts -->
    <link href="<?php echo base_url()?>css/ant_admin_css/morris/morris.css" rel="stylesheet" type="text/css" />


    <!-- Less Style -->
    <!--   <link rel="stylesheet/less" href="less/sidebar.less" type="text/css" />
  <link rel="stylesheet/less" href="less/main.less" type="text/css" />
  <script src="js/less-1.7.3.min.js"></script> -->
    <!-- add javascripts -->
    <!-- jQuery 2.0.2 -->
    <!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>  -->
    <script src="<?php echo base_url()?>js/ant_admin_js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/angular/angular.min.js"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/angular/angular-menu.js"></script>
    <script src="<?php echo base_url()?>js/ant_admin_js/angular/controllers.js"></script>
    <script src="<?php echo base_url()?>js/ant_user_dashboard/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    
</head>

<body  class="main-theme fixed <?php 
if ($this->uri->segment('2') == 'login') {
    echo "bg-black";
}else{
    echo "";
}
?>">
