<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus_model extends CI_Model{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function create_parent_menu($parent_menu_name,$project_id,$user_id)
	{
		$menu_data=array("menu_name"=>str_replace("%20", " ", $parent_menu_name), "project_id"=>$project_id, "user_id"=>$user_id,"status"=>0);
		$this->db->insert("ant_menus",$menu_data);
		return $this->db->insert_id();
	}
	public function get_parent_menu()
	{
		$this->db->select("*");
		$this->db->from("ant_menus");
		$qry=$this->db->get();
		return $qry->result_array();
	}
	public function add_submenu($submenu_names,$parent_menus_ids)
	{
		$menu_data=array("submenu_name"=>str_replace("%20", " ", $submenu_names), "parent_menu_id"=>$parent_menus_ids);
		$this->db->insert("ant_submenus",$menu_data);
		return $this->db->insert_id();
	}
	public function get_submenu($parent_menus_ids)
	{
		$this->db->select("*");
		$this->db->from("ant_submenus");
		$this->db->where("parent_menu_id",$parent_menus_ids);
		$qry=$this->db->get();
		return $qry->result_array();
	}
}