// lazyload config
//var base_url = 'http://exceptionaire.co.in/happyEdit/';
//var base_url = 'http://aish.happyedit.com/';
var base_url = "http://"+window.location.hostname+"/";

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
      easyPieChart:   [   base_url+'js/ant_user_dashboard/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
      plot:           [   base_url+'js/ant_user_dashboard/bower_components/flot/jquery.flot.js',
                          base_url+'js/ant_user_dashboard/bower_components/flot/jquery.flot.pie.js', 
                          base_url+'js/ant_user_dashboard/bower_components/flot/jquery.flot.resize.js',
                          base_url+'js/ant_user_dashboard/bower_components/flot.tooltip/js/jquery.flot.tooltip.js',
                          base_url+'js/ant_user_dashboard/bower_components/flot.orderbars/js/jquery.flot.orderBars.js',
                          base_url+'js/ant_user_dashboard/bower_components/flot-spline/js/jquery.flot.spline.js'],
      knob:           [   base_url+'js/ant_user_dashboard/bower_components/jquery-knob/dist/jquery.knob.min.js', 'js/jq/chart-knobs.js'],
      isotobe:          [  'js/uport_isotobe.js',
                            'js/uport_isotobe_script.js'],
      dataTable:      [   base_url+'js/ant_user_dashboard/bower_components/datatables/media/js/jquery.dataTables.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                          base_url+'js/ant_user_dashboard/bower_components/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
      footable:       [ base_url+'js/ant_user_dashboard/bower_components/footable/dist/footable.all.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/footable/css/footable.core.css'],
      fullcalendar:   [   base_url+'js/ant_user_dashboard/bower_components/moment/moment.js',
                          base_url+'js/ant_user_dashboard/bower_components/fullcalendar/dist/fullcalendar.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/fullcalendar/dist/fullcalendar.css',],
      vectorMap:      [   base_url+'js/ant_user_dashboard/bower_components/bower-jvectormap/jquery-jvectormap-1.2.2.min.js', 
                          base_url+'js/ant_user_dashboard/bower_components/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                          base_url+'js/ant_user_dashboard/bower_components/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                          base_url+'js/ant_user_dashboard/bower_components/bower-jvectormap/jquery-jvectormap-1.2.2.css'],
      sortable:       [   base_url+'js/ant_user_dashboard/bower_components/html5sortable/jquery.sortable.js'],
      nestable:       [   base_url+'js/ant_user_dashboard/bower_components/nestable/jquery.nestable.js'],
      moment:         [   base_url+'js/ant_user_dashboard/bower_components/moment/moment.js'],
      daterangepicker:[   base_url+'js/ant_user_dashboard/bower_components/moment/moment.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css'],
      tagsinput:      [ base_url+'js/ant_user_dashboard/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'],
      jqueryui:        [  base_url+'js/ant_user_dashboard/bower_components/jquery-ui/ui/minified/jquery-ui.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/jquery-ui/themes/smoothness/jquery-ui.css',
                          'js/controllers/ui.slider.js'],
      TouchSpin:      [   base_url+'js/ant_user_dashboard/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
      chosen:         [   base_url+'js/ant_user_dashboard/bower_components/chosen/chosen.jquery.min.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-chosen/bootstrap-chosen.css'],
      wysiwyg:        [   base_url+'js/ant_user_dashboard/bower_components/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                          base_url+'js/ant_user_dashboard/bower_components/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      sparkline:       [   base_url+'js/ant_user_dashboard/bower_components/jquery.sparkline/dist/jquery.sparkline.retina.js']
    }
  )

  // oclazyload config
  .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  true,
          events: true,
          modules: [
              {
                  name: 'ngMorris',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/raphael/raphael.js',
                      base_url+'js/ant_user_dashboard/bower_components/mocha/mocha.js',
                      base_url+'js/ant_user_dashboard/bower_components/morrisjs/morris.js',
                      base_url+'js/ant_user_dashboard/bower_components/ngmorris/src/ngMorris-full.js',
                      base_url+'js/ant_user_dashboard/bower_components/morrisjs/morris.css'
                  ]
              },
              {
                  name:'cgNotify',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-notify/dist/angular-notify.min.js',
                      base_url+'js/ant_user_dashboard/bower_components/angular-notify/dist/angular-notify.min.css'
                  ]
              },
              {
                  name:'countTo',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-count-to/build/angular-count-to.min.js'
                  ]
              },
                                      
              {
                  name:'angularFileUpload',
                  files: [
                    base_url+'js/ant_user_dashboard/bower_components/angular-file-upload/dist/angular-file-upload.min.js'
                  ]
              },
              /*{
                  name: 'textAngular',
                  series: true,
                  files: [
                      '../bower_components/textAngular/dist/textAngular.css',
                      '../bower_components/textAngular/dist/textAngular-rangy.min.js',
                      '../bower_components/textAngular/dist/textAngular.min.js'
                  ]
              },*/
              {
                  name: 'vr.directives.slider',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/venturocket-angular-slider/build/angular-slider.min.js',
                      base_url+'js/ant_user_dashboard/bower_components/venturocket-angular-slider/build/angular-slider.css'
                  ]
              },
              {
                  name: 'ngGrid',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/ng-grid/build/ng-grid.min.js',
                      base_url+'js/ant_user_dashboard/bower_components/ng-grid/ng-grid.min.css',
                      base_url+'js/ant_user_dashboard/bower_components/ng-grid/ng-grid.bootstrap.css'
                  ]
              },
              {
                  name: 'ui.grid',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-ui-grid/ui-grid.min.js',
                      base_url+'js/ant_user_dashboard/bower_components/angular-ui-grid/ui-grid.min.css'
                  ]
              },
              {
                  name: 'chart.js',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-chart.js/dist/angular-chart.js',
                      base_url+'js/ant_user_dashboard/bower_components/angular-chart.js/dist/angular-chart.css'
                  ]

              },
              {
                  name: 'angular-rickshaw',
                  files: [
                    base_url+'js/ant_user_dashboard/bower_components/rickshaw/rickshaw.min.css',
                    base_url+'js/ant_user_dashboard/bower_components/rickshaw/rickshaw.min.js',
                    base_url+'js/ant_user_dashboard/bower_components/angular-rickshaw/rickshaw.js'
                  ]

              },
              {
                  name: 'xeditable',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-xeditable/dist/js/xeditable.min.js',
                      base_url+'js/ant_user_dashboard/bower_components/angular-xeditable/dist/css/xeditable.css'
                  ]
              },
              {
                  name:'ui.calendar',
                  files: [base_url+'js/ant_user_dashboard/bower_components/angular-ui-calendar/src/calendar.js']
              },
              {
                  name: 'ngImgCrop',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/ngImgCrop/compile/minified/ng-img-crop.js',
                      base_url+'js/ant_user_dashboard/bower_components/ngImgCrop/compile/minified/ng-img-crop.css'
                  ]
              },
              {
                  name: 'colorpicker.module',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
                      base_url+'js/ant_user_dashboard/bower_components/angular-bootstrap-colorpicker/css/colorpicker.css'
                  ]
              },
              {
                  name: 'smart-table',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/angular-smart-table/dist/smart-table.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular',
                  files: [
                    base_url+'js/ant_user_dashboard/bower_components/videogular/videogular.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.controls',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/videogular-controls/vg-controls.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.buffering',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/videogular-buffering/vg-buffering.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.overlayplay',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/videogular-overlay-play/vg-overlay-play.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.poster',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/videogular-poster/vg-poster.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.imaads',
                  files: [
                      base_url+'js/ant_user_dashboard/bower_components/videogular-ima-ads/vg-ima-ads.min.js'
                  ]
              }
          ]
      });
  }])
;
