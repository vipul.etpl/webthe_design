<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Dashboard</a>
    </li>
    <li class="active">Users</li>
</ol>
<div class="row" ng-controller="UserList">
    <div class="col-md-12">
        <div class="row">
            <input type="text" class="form-control" placeholder="Search Users.." ng-model="userSearch.first_name" style="margin: 0 0 12px;">

            <div class="col-sm-3 col-lg-3 col-md-3" ng-repeat="data in list | filter:userSearch | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                <div class="thumbnail no-padding">
                    <img class="img-responsive" src="http://placehold.it/320x280" alt="">
                    <div class="caption">
                        <h4><a href="javascript:void(0);" ng-click="userDetails({{data.user_id}})">{{data.first_name}} {{data.last_name}}</a>
                        </h4>
                    </div>
                    <div class="ratings pad btn btn-danger" ng-click="deleteThisUser({{data.user_id}})" style="width:100%;border-radius:0;">
                        <i class="fa fontello-folder-delete"></i>&nbsp;Delete                 
                    </div>
                </div>

            </div>
             <p ng-show="(list | filter:userSearch).length == 0" style="font-size: 16px;text-align: center;">No data found</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="list == 101"> 
                  No data found  
            </div>

            <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
            </uib-pagination>
        </div>
        <!-- <uib-pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()"></uib-pagination>
        <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
        </uib-pagination> -->

    </div>

    <div class="modal fade" id="userDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" >
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="userLabel">User's Details</h4>
          </div>
          <div class="modal-body" style="overflow-y: auto;height:215px;max-height:300px;">
             <div class="" ng-repeat="userdata in userlist" style="padding: 15px;">
               <div class="pull-left">
                  <img class="img-responsive" src="http://placehold.it/320x280" alt="" style="width:200px;">
               </div>
               <div class="pull-left" style="margin: 0 0 0 16px;">
                <p><b>Id: </b>{{userdata.user_id}}</p>
                <p><b>Name: </b>{{userdata.first_name}} {{userdata.last_name}}</p>
                <p><b>Email Id: </b>{{userdata.email}}</p>
                <p><b>Username: </b>{{userdata.username}}</p>
                <p><b>Register Date: </b>{{userdata.register_date}}</p>
               </div>
             </div>
          </div>
          
        </div>
      </div>
    </div>

</div>


