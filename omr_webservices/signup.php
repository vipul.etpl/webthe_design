<?php
//#########################################################################
# User Signup 
# Yogesh Koli
# 1/15/2014
//#########################################################################
//header('Content-type: application/json');
require_once 'includes/DB_Function.php';
class Signup extends  DB_Functions{ 
    function __construct() {
		 require_once 'includes/DB_Connect.php';
        $this->db = new DB_Connect();
        $this->db->connect();
    }
    function __destruct(){ 
    }
    
    // Get First and last name  - YK
    public function get_fl_name($name){
        $name = explode(" ", $name);   
        $data = array(); 
        $data["first_name"] = $name[0];
        $data["last_name"] = $name[1];
        return $data;
    }
    
    // create username string  - YK
    public function username_string($name){
        
    }
    
    // store register data - YK
    public function store_register_data($name){
       $user_id = $this->get_max_id("omr_register", "user_id");
       $flname = $this->get_fl_name($name);
       $query = "INSERT INTO omr_register(user_id, first_name, last_name, register_date) VALUES('$user_id','{$flname['first_name']}','{$flname['last_name']}', now())"; 
       if(!$result = mysql_query($query)){
            exit(mysql_error());
       }
       return $user_id;
    }
    
    // Store Login data - YK
    public function store_login_data($data){
        $lgn_id = $this->get_max_id("omr_lgn_details", "lgn_id");
        $user_id = $this->store_register_data($data["name"]);
        $pdata = $this->encrypt_password($data["password"]);
        $query = "INSERT INTO omr_lgn_details(lgn_id, user_id, email, password, salt) VALUES('$lgn_id','$user_id', '{$data["email"]}','{$pdata['password']}','{$pdata['salt']}')";
        if(!$result = mysql_query($query)){
            exit(mysql_error());
        }
        // Send email
        $this->send_email_confirmation($data["email"], $user_id, $pdata["salt"], $data["name"]);
        $udata = array();
        $udata = $this->user_details($user_id);
        return $udata;
    }
    
    // Encrypt user id to send for email verification 
    public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    }
    
    // Send email confirmation email - 1/16/2013
    public function send_email_confirmation($email, $user_id, $salt, $name){
            $enc_user_id = $this->enrypt_user_id($user_id, $salt);
            $enc_user_id = md5($enc_user_id);
            $salt = urldecode($salt);
            require_once("../script/libmail.php");
            //Init. user email
            $mail = $email;
            $message = file_get_contents("../media/emails/email_confirmation.html");
            $message = preg_replace('/{user}/', $name, $message);
            $message = preg_replace('/{user_id}/', $enc_user_id, $message);
            $message = preg_replace('/{salt}/', $salt, $message);
            //Send email actions;
            $m = new Mail ();
            //Sender email
            $m->From ("anything@admixtion.com");
            //Registration user mail
            $m->To ($mail);
            //Theme
            $m->Subject ("One Minute Receipt Account Confirmation");
            //Mail content
            $m->Body ($message,"html");
            $m->Send ();
    }
      
    public function checkhashSSHA($salt, $password) { 
        $hash = base64_encode(sha1($password . $salt, true) . $salt); 
        return $hash;
    }  
}

$ob = new Signup(); 
$response = array();
$_REQUEST = json_decode(file_get_contents('php://input'), true);
echo $_REQUEST["password"];
exit();

// Intialize the process
if((($_REQUEST["name"] && $_REQUEST["name"] != "") && ($_REQUEST["email"] && $_REQUEST["email"] != "")) &&  ($_REQUEST["password"] && $_REQUEST["password"] != "")){
	 $name = $_REQUEST["name"];
	 $email = $_REQUEST["email"];
	 $password = $_REQUEST["password"];
	 // Validate name
	 if($ob->validate_name($name)){
		 // Validate email 
		 if($ob->validate_email($email)){
			 // vlidate password
			  if($ob->validate_password($password)){
				  // Check email is exist 
				  if($ob->check_email_exist($email)){
                       // Store Login data
                       $login_data = array();
                       $login_data["name"] = $name;
                       $login_data["email"] = $email;
                       $login_data["password"] = $password;
                       $user_data = $ob->store_login_data($login_data);
                         $response = array();
                         $response['success'] = 0;
                         $response['error'] = 0;
                         $response['user_id'] = $user_data["user_id"]; 
                         $response['first_name'] =  $user_data["first_name"];
                         $response['last_name'] = $user_data["last_name"]; 
                         $response['email'] = $user_data["email"];                                        
                       
				  }else{
					  $response["error"] = 7; 
                      $response["error_msg"] = "Email already exist.";  
				  }
			  }else{
				   $response["error"] = 6; 
                   $response["error_msg"] = "Invalid password, it should have combination of letters with symobol or number and 8 digit long";
			  }
		 }else{
			 $response["error"] = 5; 
             $response["error_msg"] = "Invalid email format '".$email."'";
		 }
	 }else{
		  $response["error"] = 4; 
         $response["error_msg"] = "Invalid name, Only letters and white space allowed";
	 }
	 echo json_encode($response);
}else{
	if($_REQUEST["name"] == ""){
		 $response["error"] = 1; 
         $response["error_msg"] = "Name field is required";
	}else if($_REQUEST["email"] == ""){
		 $response["error"] = 2; 
         $response["error_msg"] = "Email field is required";
	}else if($_REQUEST["password"] == ""){
		 $response["error"] = 3; 
         $response["error_msg"] = "Password field is required";
	}
	echo json_encode($response);
}
?>