<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User extends CI_Controller{    

  function  __construct() {
    parent::__construct();
    $errors = array();
    $data = array();
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
   $this->load->model('user_model');
  }

  public function get_user_list()
  {

  	$datas = array();
	$datas=$this->user_model->get_user_list();
	// print_r($datas);exit();
	echo json_encode($datas);
  }
  public function delete_this_user($userids)
  {
    $this->user_model->delete_this_user($userids);
  }
  public function user_details($userids)
  {
    $data = array();
    $data=$this->user_model->get_user_details($userids);
    echo json_encode($data);
  }
}