<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends CI_Controller
{
	function  __construct() {
		parent::__construct();
		$this->load->library('paypal_lib');
		//$this->load->model('product');
        $this->load->library('cart');
		
	}
	
	function index(){
		$data = array();
		//get products data from database
       // $data['products'] = $this->product->getRows();
		//pass the products data to view
		$this->load->view('products/index');
	}
	function buy($id,$dName){
		 $grand_total = 0;
         $i = 1;
         $product = $this->cart->contents();
		//Set variables for paypal form
		$paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //test PayPal api url
		$paypalID = 'amol.patil@otssolutions.com'; //business email
		$returnURL = base_url().'index.php/dashboard/payment_success'; //payment success url
		$cancelURL = base_url().'index.php/Paypal/cancel'; //payment cancel url
		$notifyURL = base_url().'index.php/Paypal/ipn'; //ipn url get particular product data
		//$product = $this->product->getRows($id);
		$userID = 10; //current user id
		$logo = base_url().'assets/images/happy-logo.png';
		$this->paypal_lib->add_field('business', $paypalID);
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('custom', $this->session->userdata('user_id'));
		$this->paypal_lib->image($logo);
        foreach ($product as $item){
        	// echo $item['name']."--";
        	// echo number_format($item['price'],2)."--";
        	  $grand_total = $grand_total + $item['subtotal'];
        	  // echo 'item_name_'.$item['id'], $item['name'].'<br>';
        	  $name = $item['name'];
        	  $pid = $item['id'];
        	  $price = $item['price'];
        	  $item_name = "item_name_".$i;
        	  $item_number = "item_number_".$i;
        	  $amount = "amount_".$i;
        	  $quantity = "quantity_".$i;
		      $this->paypal_lib->add_field($item_name, $name);
		      $this->paypal_lib->add_field($item_number,$pid);
			  $this->paypal_lib->add_field($amount,$price);
			  $this->paypal_lib->add_field($quantity,'1');
        	  $i++;
        }
	    $this->paypal_lib->add_field('amount',  $grand_total);
		$this->paypal_lib->paypal_auto_form();
	}

		
}