<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Login extends CI_Controller{    

  function  __construct() {
    parent::__construct();
    $errors = array();
    $data = array();
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
    //$this->load->model('product');
  }  
    public function index(){
      $this->login_view();
      $this->load->model('login_model');
      $this->login_model->already_login();
     } 
     
     // Main Login - YK
     public function loginme(){
        // $this->form_validation->set_rules('email', 'Email Address', 'trim|required|callback_main_can_log_in');
        // $this->form_validation->set_rules('password', 'Password', 'trim|required'); 
        if($this->main_can_log_in() == false){
              $errors['noUserFound'] = 1;
              $data['errors'] = $errors;
              echo json_encode($errors);                         

        }else{
            // GET USER ID Using Email address 
              $this->load->model('login_model');
              $this->load->model('signup_model');
              $udata = array();
              $udata = $this->signup_model->get_user_details( $this->login_model->get_userid_uname_or_email($_REQUEST['email']));
              
              $test = array();
              //$test['user_id'] = $udata['user_id'];
              $test['username'] = $udata['username'];
              $this->session->set_userdata($test);
              //print_r($test);
              //exit();
              echo $this->session->userdata('username');
              //redirect('dashboard');                          
        }
     } 
     
     public function main_can_log_in(){
           $this->load->model('signup_model'); 
           $this->load->model('login_model'); 
          if($this->signup_model->check_email_exist($_REQUEST['email']) || $this->login_model->check_username_exist($_REQUEST['email']))
            {             
              //     if($this->login_model->validate_me()){
              //         return true;
              //     }else{
              //        return false;  
              //       }
              
                return true;
              }
              else{
                 return false; 
              }
        }
      
      public function chkUserPwd(){
          $this->load->model('login_model'); 
          if($this->login_model->validate_me()){
            $this->load->model('signup_model'); 
            $udata = array();
              $udata = $this->signup_model->get_user_details( $this->login_model->get_userid_uname_or_email($this->session->userdata('username') ));
              $this->session->set_userdata($udata);
                  return true;
            }else{
                $errors['noUserFound'] = 1;
                $data['errors'] = $errors;
              echo json_encode($errors); 
                  //return false;  
              }
      }

     // Login View
     public function login_view(){
       $data = array();
       $data['title'] = "Happy Edit - Home";
       $this->load->view('includes/default_header', $data);  
       $this->load->view('social/signup_with_fb');  
       $this->load->view('login/login_view');
       $this->load->view('includes/page_end');
     }
    
    public function verify_email($enc_user_id, $salt){
         $email = "";
         $enc_user_id = $enc_user_id;
         $salt = urldecode($salt);
         $this->load->model('login_model');
         $user_id = $this->login_model->current_id_to_validate($enc_user_id, $salt); 
         if($this->login_model->is_email_verified($user_id)){
             redirect('home');                           
         }else{
             $uinfo = array();
             $uinfo = $this->login_model->get_user_email_and_name($user_id);
             $udata = array();
             $udata["user_id"] = $user_id;
             $udata["email"] = $uinfo["email"];
             $udata["name"] = $uinfo["name"];
             $udata["salt"] = $salt;
             $this->confirm_email_view($udata);   
         }   
    } 

    // Verify email account with password  - While Verifying Email - YK
    public function verify_crediantials(){                                     
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean|callback_can_log_in');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $data = array();
        $data['email'] = $this->input->post('email');
        $data['user_id'] = $this->input->post('user_id');
        $data['salt'] = $this->input->post('salt');
        $data['name'] = $this->input->post('name');
        $data['password'] = $this->input->post('password');
        if($this->form_validation->run() == FALSE){ 
            $this->confirm_email_view($data);
        }else{
            $this->load->model('login_model');   
            $this->login_model->email_success($this->input->post('user_id'));
            $this->load->model('signup_model');
            $udata = array();
            $udata = $this->signup_model->get_user_details($this->input->post('user_id'));
            $this->session->set_userdata($udata);
            redirect('dashboard');
        }             
    }  
    
    // Validate the user    - YK
    public function can_log_in(){
       $this->load->model('login_model');
        $data = array();
        $data['email'] = $this->input->post('email');
        $data['salt'] = $this->input->post('salt');
        $data['password'] = $this->input->post('password');
        if($this->login_model->validate_user($data)){
              return true;
        }else{           
              $this->form_validation->set_message('can_log_in', 'Invalid login details, please enter valid details.');
              return false;
        }            
    }
    
    public function confirm_email_view($udata){
       $data = array();
       $data['title'] = "Happy Edit - Email Confirmation";
       $this->load->view('includes/default_header', $data);  
       $this->load->view('includes/modal_help');
       $this->load->view('confirm_email/confirm_email', $udata);  
       $this->load->view('includes/page_end');
    } 
    
    // Logout the user
    public function logout(){
        $this->session->sess_destroy(); 
        return true;
        //redirect('/');
    }
  
}