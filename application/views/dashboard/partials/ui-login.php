<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<div class="login_page ">
    <div class="login-wrapper" ng-controller="LoginFormController">
        <div id="login" class="login loginpage col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
            <h1><a href="javascript:void(0);" title="Login Page" tabindex="-1">Happy Edit - Sign In Page</a></h1>
                <form name="form" class="form-validation ui-login_form">
                 <div class="user_login_error error">   {{authError}}</div>
                      <div class="text-danger text-center message" ng-show="authError">
                      </div>
                        <p class="input_wr_lgn">
						    <span class="input_icon_login"><i class="fa fa-envelope"></i></span>
                            <input type="text" name="email" id="user_login" class="input" ng-model="user.email" placeholder="Email / Username" autofocus required/></label>
                        </p>
                       
                        <p class="submit">
                            <button type="submit" ng-click="login()" ng-disabled='form.$invalid' class="btn btn-accent btn-block">Sign In</button>
                        </p>
                </form>
				
				
				
               <div id="nav" class="login_frst_wrp">
                <div class="forgot_pwrd_link_wp">  			
                <a ui-sref="forgotpassword" class="pull-left" title="Password Lost and Found">Forgot Password?</a>
                </div>
                <!-- <a href="<?php echo base_url(); ?>index.php/signup/twitter" class="social-log-reg">
				  <img class="img-circle " title="Tiwtter" width="25" height="25" src="<?= base_url('img/t.png'); ?>">
			    </a> -->
				<div class="sign_up_link_wp">
                <a ui-sref="signup" href="#/signup" class="pull-right" title="Sign Up">Sign Up</a>
				</div>
                
				
				
               </div>
			 
			   <div class="TiwtterL">
			     <span class="social_txt_msg"> Login with -</span>
               
                  <a href="<?php echo base_url(); ?>index.php/signup/twitter" class="social-log-reg">
                    <img class="img-circle " title="Twitter" width="25" height="25" src="<?= base_url('img/t.png'); ?>">
                  </a>
                </div>	
               			


		
            
            
			
			
        </div>
    </div>
</div>
<script>
$(document).ready(function()
{
$("#user_login").focus();
}); 
</script> 




