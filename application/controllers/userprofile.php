<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');   

class Userprofile extends CI_Controller {

    function  __construct() {
        parent::__construct();
        $errors = array();
        $data = array();
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        $this->load->model('userprofile_model');

    }

    public function index(){
      if($this->session->userdata('user_id')=='')
     {
          redirect('login');          
     }
     else
     {
       $data = array();
       $data['title'] = "Happy Edit - User Profile";
       $this->load->view('includes/page_start', $data);  
       $this->load->view('userprofile/userprofile_view',$this->session->all_userdata());

       $this->load->view('includes/page_end');
     }

    }

    public function expense_type(){
      if($this->session->userdata('user_id')=='')
     {
          redirect('login');          
     }
     else
     {
       $this->load->model('expensetype_model');
       $data = array();
       $data['title'] = "Happy Edit - Expense Type";
       $this->load->view('includes/page_start', $data);  
       $udata = array();
       $udata['expense'] = $this->expensetype_model->show_expense_type();
       $this->load->view('dashboard/expensetype_view', $udata);
       $this->load->view('includes/page_end');
     }
    }

    public function profile_setting(){
       $data = array();
       $data['title'] = "Happy Edit - Profile Setting";
       $this->load->view('includes/page_start', $data);  
       $this->load->view('userprofile/profile_setting_view',$this->session->all_userdata());
       $this->load->view('includes/page_end');
    }

    // Change/Update Session
    public function update_session($user_id){
      $this->load->model('signup_model');
      $udata = array();
      $udata = $this->signup_model->get_user_details($user_id);
      $this->session->set_userdata($udata);
    }

    

public function verify_email($enc_user_id, $salt){
         $email = "";
         $enc_user_id = $enc_user_id;
         $salt = urldecode($salt);
         $this->load->model('userprofile_model');
         $user_id = $this->userprofile_model->current_id_to_validate($enc_user_id, $salt); 
         if($this->login_model->is_email_verified($user_id)){
             redirect('home');                           
         }else{
             $uinfo = array();
             $uinfo = $this->login_model->get_user_email_and_name($user_id);
             $udata = array();
             $udata["user_id"] = $user_id;
             $udata["email"] = $uinfo["email"];
             $udata["name"] = $uinfo["name"];
             $udata["salt"] = $salt;
             $this->confirm_email_view($udata);   
         }   
    } 

  public function getUserInformation($user_id){
      $this->load->model('signup_model');
      $udata = array();
      $udata = $this->signup_model->get_user_details($user_id);
      echo json_encode($udata);
      //$this->session->set_userdata($udata);
    } 

   public function updateUserEmail($user_id)
   {
        $userInfo = array();
        $userInfo['email'] = $_REQUEST['email'];
        $userInfo['user_id'] = $user_id;
        $chkstatus = $this->userprofile_model->updateUserInformation($userInfo);
        if ($chkstatus) {
          $this->update_session($user_id);
          echo '100';
        }
        else{
          echo '500';
        }
        //echo $_REQUEST['email']." ".$_REQUEST['first_name']."".$_REQUEST['last_name']."".$user_id;

    
   } 
   public function updateUserFirstname($user_id)
   {
        $userInfo = array();
        $userInfo['first_name'] = $_REQUEST['first_name'];
        $userInfo['user_id'] = $user_id;
        $chkstatus = $this->userprofile_model->updateUserInformation($userInfo);
        if ($chkstatus) {
          $this->update_session($user_id);
          echo '100';
        }
        else{
          echo '500';
        }
        //echo $_REQUEST['email']." ".$_REQUEST['first_name']."".$_REQUEST['last_name']."".$user_id;

    
   } 
   public function updateUserLastname($user_id)
   {
        $userInfo = array();
        $userInfo['last_name'] = $_REQUEST['last_name'];
        $userInfo['user_id'] = $user_id;
        $chkstatus = $this->userprofile_model->updateUserInformation($userInfo);
        if ($chkstatus) {
          $this->update_session($user_id);
          echo '100';
        }
        else{
          echo '500';
        }
        //echo $_REQUEST['email']." ".$_REQUEST['first_name']."".$_REQUEST['last_name']."".$user_id;

    
   }
   public function check_sessions()
   {
    //echo $this->session->userdata("user_id");exit();
      if($this->session->userdata("user_id") == "")
      {
        echo "404";
      }
      else
      {
        echo "101";
      }
   } 

}


