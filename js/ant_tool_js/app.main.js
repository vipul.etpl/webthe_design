var app=angular.module("mainApp", []);
app.controller('getdomainsearchlist', function($scope, $http) {
    $scope.domainname = {};
    $scope.domainRegisterInfo = {};
    $scope.spinloader = false;
    $scope.domainList = false;

$scope.getUserInformation = function(user_id){

      $http.post(base_url+'index.php/dashboard/getDomainInformation').success(function(response) {
            $scope.domainData = response;
          });
    }
$scope.senddomainrequest = function  () {
  if($("[type=checkbox]:checked").length==0)
  {
    alert("Please select at least one domain");
  }
  else
  {
    $scope.domainname = {
      'formData' : $("#domainName").val(),
      'ext' : $("#ext").val()
    };
   $scope.extArray = [];
   $scope.priceData = [];
   $scope.dmaintxt = $("#domainName").val();
   $scope.Users = [{ext:'gr',price:'100'},{ext:'in',price:'200'},{ext:'com',price:'400'}];
   $scope.Employments = $("#ext").val();
   $scope.doaminStatus = checkDomain2($scope.dmaintxt);
   if ($scope.doaminStatus) {
    $scope.spinloader = true;
    $http({
          method  : 'POST',
          url     : base_url+'index.php/dashboard/getDomainInformation',
          data    : $scope.domainname, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            $scope.priceData;
            $scope.extArray;  
              
            $scope.jsonData = data;  
            $scope.spinloader = false;
            $scope.domainList = true;
          });
   }else{
    console.log('false');

   }
   }
}  
// Get price for each domain
$scope.chkPriceVal = function(domianName,id) {
   $http.get(base_url+'index.php/dashboard/getPriceValWidDomainExt/'+domianName).success(function(response) {
        $scope.domainExtPrice = response;
        $("#domainprice"+id).attr("class","label label-default priceTag").text($scope.domainExtPrice['price']);
        $(".domainck"+id).removeAttr("disabled");
        
     });
}

// ENd - Get price for each domain
// Code for create chek list ----
 $scope.roles = [
    '.gr', 
    '.eu', 
    '.com.gr', 
    '.org.gr',
    '.net.gr',
    '.gov.gr',
    '.com',
    '.net',
    '.org',
    '.info',
    '.mobi',
    '.name',
    '.ac',
    '.cc','.io','.sh','.tv','.bz','.ws','.ms','.gs','.in','.fm','.la'
  ];
  $scope.user = {
    roles: [".gr"]
  };
   // console.log($scope.user.roles);
  $scope.checkAll = function() {
    $scope.user.roles = angular.copy($scope.roles);
  };
  $scope.uncheckAll = function() {
    $scope.user.roles = [];
  };
  $scope.checkFirst = function() {
    $scope.user.roles.splice(0, $scope.user.roles.length); 
    $scope.user.roles.push('.gr');
  };

// End of chk list.....

// Function for register domian ..
$scope.addDomainTocart = function  () {
  $scope.chkDomainlist = domainSelectionProcess();
  if ($scope.chkDomainlist) {
    //$scope.domainNameVal = $(".chkBoxval:checked").val();
            var allVals = [];
            var i;
            $scope.domainInformation = {};
            $(".chkBoxval:checked").each(function () {
              var domainPriceVal = $(this).parent().find("span").text();
                allVals.push({pName:$(this).val(), pPrice:domainPriceVal,pId:$(this).val()});
            });
             //for(i=0;i<allVals.length;i++){
                var jsonString = JSON.stringify(allVals);
            // console.log(jsonString);return false;
               $http({
                  method  : 'POST',
                  dataType:'json',
                 url     : base_url+'index.php/dashboard/addDomain/',
                 data:{'myData':jsonString}
                 }).success(function(data) {
                    // alert(data);
                      if (data = '100') {
                          $scope.domainMsg = 'You have added - '+allVals.length+'items into your cart'; 
                             $scope.spinloader2 = false;    
                             location.reload();              
                      }
                  });  
                  //}
              // alert('Thanks!');    
            }
}



// End of register domain 
});

function checkDomain2(nname)
{

var arr = new Array(
'.com','.net','.org','.biz','.coop','.info','.museum','.name', '.pro','.edu','.gov','.int','.mil','.ac','.ad','.ae','.af','.ag', '.ai','.al','.am','.an','.ao','.aq','.ar','.as','.at','.au','.aw', '.az','.ba','.bb','.bd','.be','.bf','.bg','.bh','.bi','.bj','.bm', '.bn','.bo','.br','.bs','.bt','.bv','.bw','.by','.bz','.ca','.cc', '.cd','.cf','.cg','.ch','.ci','.ck','.cl','.cm','.cn','.co','.cr', '.cu','.cv','.cx','.cy','.cz','.de','.dj','.dk','.dm','.do','.dz', '.ec','.ee','.eg','.eh','.er','.es','.et','.fi','.fj','.fk','.fm', '.fo','.fr','.ga','.gd','.ge','.gf','.gg','.gh','.gi','.gl','.gm', '.gn','.gp','.gq','.gr','.gs','.gt','.gu','.gv','.gy','.hk','.hm', '.hn','.hr','.ht','.hu','.id','.ie','.il','.im','.in','.io','.iq', '.ir','.is','.it','.je','.jm','.jo','.jp','.ke','.kg','.kh','.ki', '.km','.kn','.kp','.kr','.kw','.ky','.kz','.la','.lb','.lc','.li', '.lk','.lr','.ls','.lt','.lu','.lv','.ly','.ma','.mc','.md','.mg', '.mh','.mk','.ml','.mm','.mn','.mo','.mp','.mq','.mr','.ms','.mt', '.mu','.mv','.mw','.mx','.my','.mz','.na','.nc','.ne','.nf','.ng', '.ni','.nl','.no','.np','.nr','.nu','.nz','.om','.pa','.pe','.pf', '.pg','.ph','.pk','.pl','.pm','.pn','.pr','.ps','.pt','.pw','.py', '.qa','.re','.ro','.rw','.ru','.sa','.sb','.sc','.sd','.se','.sg', '.sh','.si','.sj','.sk','.sl','.sm','.sn','.so','.sr','.st','.sv', '.sy','.sz','.tc','.td','.tf','.tg','.th','.tj','.tk','.tm','.tn', '.to','.tp','.tr','.tt','.tv','.tw','.tz','.ua','.ug','.uk','.um', '.us','.uy','.uz','.va','.vc','.ve','.vg','.vi','.vn','.vu','.ws', '.wf','.ye','.yt','.yu','.za','.zm','.zw');
var mai = nname;
var val = true;
var dname = mai;
if(dname.length>2 && dname.length<57)
{
    for(var j=0; j<dname.length; j++)
        {
          var dh = dname.charAt(j);
          var hh = dh.charCodeAt(0);
          if((hh > 47 && hh<59) || (hh > 64 && hh<91) || (hh > 96 && hh<123)  || (hh > 944 && hh <970) || (hh > 912 && hh < 938) || hh==45 )
            {
                 if((j==0 || j==dname.length-1) && hh == 45)    
                    {
                        alert("Invalid Domain Name");
                        return false;
                    }
            }
          else{
                alert("Invalid Domain Name");
                return false;
            }
        }
}
else
{
     alert("Invalid Domain Name");
     return false;
}   
return true;
}

function domainSelectionProcess()
{
  if ($(".chkBoxval:checked").length == 0)
  {
    alert("Select a domain name");
    return false;
  }
  else{
    return true; 
  }
   
}