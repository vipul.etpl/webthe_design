<div class="content-body" ng-controller="orderPage" ng-init="app.settings.pagetitle = 'Order Page'; orderInit('<?php echo $this->session->userdata('user_id');?>')">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 form-wizard nav-pills">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <uib-progressbar value="steps.percent" class="progress-xs primary" type="success"></uib-progressbar>
            </div>
            <uib-tabset class="tab-container" ng-init="steps={percent:20, step1:true, step2:false, step3:false, step4:false, step5:false, step1stat:false, step2stat:false, step3stat:false, step4stat:false, step5stat:false }">
                <uib-tab active="steps.step1" select="steps.percent=20" class=" {{steps.step1stat ? 'complete' : ''}} ">
                    <uib-tab-heading class="tabhead tab_ms1">General</uib-tab-heading>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 Basic_Information_wrapp"> 
				   <h4 class="form_heading_in_Dashbord">Basic Information</h4>
					
                    <form name="step1" class="form-validation  padding-left-0 Basic_Information_form">
                        <div class="form-group">
                            <label class="form-label" for="field-1">Full Name</label>
                            <div class="controls">
                               <span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>	
							   <!-- <input type="text" name="fname" value="{{userData.first_name}}"> -->
                                <input type="text" placeholder="Full Name" class="form-control" name="fullname" id="fullaname" maxlength="20" ng-model="domainRegisterInfo.fullname" value='<?php echo $myData['first_name']."-".$myData['last_name']; ?>'  required ng-initial>
                                

						   </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">First name</label>
                            <div class="controls">
							  <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" ng-model="domainRegisterInfo.firstname" name="firstname" placeholder="First Name" id="firstname" size="32" class="form-control"  value="<?php echo $myData['first_name']; ?>" required ng-initial>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">Last name</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="lastname" placeholder="Last Name" id="lastname" size="32" class="form-control" value="<?php echo $myData['last_name']; ?>" ng-model="domainRegisterInfo.lastname"  required ng-initial>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="form-label" for="field-1">Company title (Optional)</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="titlos" placeholder="Company title" id="titlos" size="32" class="form-control" ng-model="domainRegisterInfo.titlos">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="form-label" for="field-1">Email</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span> 
                                <input type="email" name="emailText" placeholder="Email" id="emailText" size="32" class="form-control" value="<?php echo $myData['email'];?>"  ng-model="domainRegisterInfo.emailText"  required ng-initial>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">Address</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="address1" placeholder="Address" id="address1" size="32" class="form-control" ng-model="domainRegisterInfo.address1" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">State</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="stateProvince" placeholder="State" id="stateProvince" size="32" class="form-control" ng-model="domainRegisterInfo.stateProvince" required>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="form-label" for="field-1">City</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="city" placeholder="City" id="city" size="32" class="form-control" ng-model="domainRegisterInfo.city" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">Postcode</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="postcode" placeholder="Postcode" id="postcode" size="32" class="form-control" ng-model="domainRegisterInfo.postcode" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="field-1">Country</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                            	<select ng-model="domainRegisterInfo.country" class="form-control" name="country" id="country" size="1" placeholder="Country">
				           <option value="SH">Αγ. Ελένη</option>
						<option value="PM">Άγ. Πέτρος (Σεν Πιέρ) και Μικελόν</option>
						<option value="LC">Αγία Λουκία</option>
						<option value="VC">Άγιος Βικέντιος και Γρεναδίνες</option>
						<option value="ST">Άγιος Θωμάς και Πρίγκιπας (Σάο Τομέ και Πρίντσιπε)</option>
						<option value="SM">Άγιος Μαρίνος</option>
						<option value="KN">Άγιος Χριστόφορος (Σαιντ Κιτς) και Νέβις</option>
						<option value="AO">Αγκόλα</option>
						<option value="AZ">Αζερμπαϊτζάν</option>
						<option value="EG">Αίγυπτος</option>
						<option value="ET">Αιθιοπία</option>
						<option value="HT">Αϊτή</option>
						<option value="CI">Ακτή Ελεφαντοστού</option>
						<option value="AL">Αλβανία</option>
						<option value="DZ">Αλγερία</option>
						<option value="AS">Αμερικανικές Σαμόα</option>
						<option value="TP">Ανατολικό Τιμόρ</option>
						<option value="AI">Ανγκουίλα</option>
						<option value="AD">Ανδόρα</option>
						<option value="AQ">Ανταρκτική</option>
						<option value="AG">Αντίγκουα και Μπαρμπούντα</option>
						<option value="AR">Αργεντινή</option>
						<option value="AM">Αρμενία</option>
						<option value="AW">Αρούμπα</option>
						<option value="AU">Αυστραλία</option>
						<option value="AT">Αυστρία</option>
						<option value="AF">Αφγανιστάν</option>
						<option value="VU">Βανουάτου</option>
						<option value="VA">Βατικανό</option>
						<option value="BE">Βέλγιο</option>
						<option value="VE">Βενεζουέλα</option>
						<option value="BM">Βερμούδες</option>
						<option value="VN">Βιετνάμ</option>
						<option value="BO">Βολιβία</option>
						<option value="BA">Βοσνία Ερζεγοβίνη</option>
						<option value="BG">Βουλγαρία</option>
						<option value="BR">Βραζιλία</option>
						<option value="IO">Βρετανικά Εδάφη Ινδικού Ωκεανού</option>
						<option value="FR">Γαλλία</option>
						<option value="GF">Γαλλική Γουιάνα</option>
						<option value="PF">Γαλλική Πολυνησία</option>
						<option value="DE">Γερμανία</option>
						<option value="GE">Γεωργία</option>
						<option value="GI">Γιβραλτάρ</option>
						<option value="YU">Γιουγκοσλαβία</option>
						<option value="GM">Γκάμπια</option>
						<option value="GA">Γκαμπόν</option>
						<option value="GH">Γκάνα</option>
						<option value="GU">Γκουάμ</option>
						<option value="GP">Γουαδελούπη</option>
						<option value="GT">Γουατεμάλα</option>
						<option value="GY">Γουιάνα</option>
						<option value="GN">Γουινέα</option>
						<option value="GW">Γουινέα-Μπισάου</option>
						<option value="GD">Γρενάδα</option>
						<option value="GL">Γροιλανδία</option>
						<option value="DK">Δανία</option>
						<option value="CZ">Δημοκρατία της Τσεχίας</option>
						<option value="CD">Δημοκρατία του Κονγκό (Ζαΐρ)</option>
						<option value="DO">Δομινικανή Δημοκρατία</option>
						<option value="EH">Δυτική Σαχάρα</option>
						<option value="SV">Ελ Σαλβαδόρ</option>
						<option value="CH">Ελβετία</option>
						<option selected="selected" value="GR">Ελλάδα</option>
						<option value="ER">Ερυθραία</option>
						<option value="EE">Εσθονία</option>
						<option value="ZM">Ζάμπια</option>
						<option value="ZW">Ζιμπάμπουε</option>
						<option value="AE">Ηνωμένα Αραβικά Εμιράτα</option>
						<option value="US">Ηνωμένες Πολιτείες</option>
						<option value="GB">Ηνωμένο Βασίλειο</option>
						<option value="JP">Ιαπωνία</option>
						<option value="IN">Ινδία</option>
						<option value="ID">Ινδονησία</option>
						<option value="JO">Ιορδανία</option>
						<option value="IQ">Ιράκ</option>
						<option value="IR">Ιράν</option>
						<option value="IE">Ιρλανδία</option>
						<option value="GQ">Ισημερινή Γουινέα</option>
						<option value="EC">Ισημερινός</option>
						<option value="IS">Ισλανδία</option>
						<option value="ES">Ισπανία</option>
						<option value="IL">Ισραήλ</option>
						<option value="IT">Ιταλία</option>
						<option value="KZ">Καζαχστάν</option>
						<option value="CM">Καμερούν</option>
						<option value="KH">Καμπότζη</option>
						<option value="CA">Καναδάς</option>
						<option value="QA">Κατάρ</option>
						<option value="CF">Κεντροαφρικανική Δημοκρατία</option>
						<option value="KE">Κένυα</option>
						<option value="CN">Κίνα</option>
						<option value="KG">Κιργιστάν</option>
						<option value="KI">Κιριμπάτι</option>
						<option value="CO">Κολομβία</option>
						<option value="KM">Κομόρες</option>
						<option value="CG">Κονγκό</option>
						<option value="KR">Κορέα</option>
						<option value="CR">Κόστα Ρίκα</option>
						<option value="CU">Κούβα</option>
						<option value="KW">Κουβέιτ</option>
						<option value="HR">Κροατία</option>
						<option value="CY">Κύπρος</option>
						<option value="KP">Λαϊκή Δημοκρατία της Κορέας</option>
						<option value="LA">Λάος</option>
						<option value="LS">Λεσότο</option>
						<option value="LV">Λετονία</option>
						<option value="BY">Λευκορωσία</option>
						<option value="LB">Λίβανος</option>
						<option value="LR">Λιβερία</option>
						<option value="LY">Λιβύη</option>
						<option value="LT">Λιθουανία</option>
						<option value="LI">Λίχτενσταϊν</option>
						<option value="LU">Λουξεμβούργο</option>
						<option value="YT">Μαγιότ</option>
						<option value="MG">Μαδαγασκάρη</option>
						<option value="MO">Μακάο</option>
						<option value="MY">Μαλαισία</option>
						<option value="MW">Μαλάουι</option>
						<option value="MV">Μαλδίβες</option>
						<option value="ML">Μάλι</option>
						<option value="MT">Μάλτα</option>
						<option value="MA">Μαρόκο</option>
						<option value="MQ">Μαρτινίκα</option>
						<option value="MU">Μαυρίκιος</option>
						<option value="MR">Μαυριτανία</option>
						<option value="MX">Μεξικό</option>
						<option value="MM">Μιανμάρ</option>
						<option value="UM">Μικρά απομονωμένα νησιά Ηνωμένων Πολιτειών</option>
						<option value="FM">Μικρονησία</option>
						<option value="MN">Μογγολία</option>
						<option value="MZ">Μοζαμβίκη</option>
						<option value="MD">Μολδαβία</option>
						<option value="MC">Μονακό</option>
						<option value="MS">Μονσεράτ</option>
						<option value="BD">Μπανγκλαντές</option>
						<option value="BB">Μπαρμπάντος</option>
						<option value="BS">Μπαχάμες</option>
						<option value="BH">Μπαχρέιν</option>
						<option value="BZ">Μπελίσε</option>
						<option value="BJ">Μπενίν</option>
						<option value="BW">Μποτσουάνα</option>
						<option value="BF">Μπουρκίνα Φάσο</option>
						<option value="BI">Μπουρούντι</option>
						<option value="BT">Μπουτάν</option>
						<option value="BN">Μπρουνέι</option>
						<option value="NA">Ναμίμπια</option>
						<option value="NR">Ναούρου</option>
						<option value="NZ">Νέα Ζηλανδία</option>
						<option value="NC">Νέα Καληδονία</option>
						<option value="NP">Νεπάλ</option>
						<option value="BV">Νησί Μπουβέ</option>
						<option value="NF">Νησί Νόρφολκ</option>
						<option value="CX">Νησί Χριστουγέννων</option>
						<option value="MP">Νησιά Βόρειες Μαριάννες</option>
						<option value="KY">Νησιά Καϊμάν</option>
						<option value="CC">Νησιά Κόκος (Κίλινγκ)</option>
						<option value="CK">Νησιά Κουκ</option>
						<option value="MH">Νησιά Μάρσαλ</option>
						<option value="WF">Νησιά Ουόλις και Φουτούνα</option>
						<option value="SJ">Νησιά Σβάλμπαρντ και Γιαν Μάγεν</option>
						<option value="SB">Νησιά Σολομώντα</option>
						<option value="TC">Νησιά Ταρκ και Κάικος</option>
						<option value="FO">Νησιά Φερόες</option>
						<option value="FK">Νησιά Φόκλαντ (Μαλβίνας)</option>
						<option value="HM">Νησιά Χερντ και Μακντόναλντ</option>
						<option value="NE">Νίγηρας</option>
						<option value="NG">Νιγηρία</option>
						<option value="NI">Νικαράγουα</option>
						<option value="NU">Νιούε</option>
						<option value="NO">Νορβηγία</option>
						<option value="ZA">Νότια Αφρική</option>
						<option value="TF">Νότια Γαλλικά Εδάφη</option>
						<option value="GS">Νότια Γεωργία και Νότια Νησιά Σάντουιτς</option>
						<option value="DM">Ντομίνικα</option>
						<option value="NL">Ολλανδία</option>
						<option value="AN">Ολλανδικές Αντίλλες</option>
						<option value="OM">Ομάν</option>
						<option value="HN">Ονδούρα</option>
						<option value="HU">Ουγγαρία</option>
						<option value="UG">Ουγκάντα</option>
						<option value="UZ">Ουζμπεκιστάν</option>
						<option value="UA">Ουκρανία</option>
						<option value="UY">Ουρουγουάη</option>
						<option value="PK">Πακιστάν</option>
						<option value="PW">Παλάου</option>
						<option value="PA">Παναμάς</option>
						<option value="PG">Παπούα Νέα Γουινέα</option>
						<option value="PY">Παραγουάη</option>
						<option value="VG">Παρθένα νησιά (Βρετανικά)</option>
						<option value="VI">Παρθένα νησιά (ΗΠΑ)</option>
						<option value="PE">Περού</option>
						<option value="PN">Πίτκερν</option>
						<option value="PL">Πολωνία</option>
						<option value="PT">Πορτογαλία</option>
						<option value="PR">Πουέρτο Ρίκο</option>
						<option value="CV">Πράσινο Ακρωτήριο</option>
						<option value="MK">Πρώην Γιουκοσλαβική Δημοκρατία της Μακεδονίας (F.Y.R.O.M.)</option>
						<option value="RE">Ρεϊνιόν</option>
						<option value="RW">Ρουάντα</option>
						<option value="RO">Ρουμανία</option>
						<option value="RU">Ρωσία</option>
						<option value="WS">Σαμόα</option>
						<option value="SA">Σαουδική Αραβία</option>
						<option value="SN">Σενεγάλη</option>
						<option value="SC">Σεϋχέλλες</option>
						<option value="SG">Σιγκαπούρη</option>
						<option value="SL">Σιέρα Λεόνε</option>
						<option value="SK">Σλοβακία</option>
						<option value="SL">Σλοβενία</option>
						<option value="SI">Σλοβενία</option>
						<option value="SO">Σομαλία</option>
						<option value="SZ">Σουαζιλάνδη</option>
						<option value="SD">Σουδάν</option>
						<option value="SE">Σουηδία</option>
						<option value="SV">Σουηδία</option>
						<option value="SR">Σουρινάμ</option>
						<option value="LK">Σρι Λάνκα</option>
						<option value="SY">Συρία</option>
						<option value="TW">Ταϊβάν</option>
						<option value="TH">Ταϊλάνδη</option>
						<option value="TZ">Τανζανία</option>
						<option value="TJ">Τατζικιστάν</option>
						<option value="JM">Τζαμάικα</option>
						<option value="DJ">Τζιμπουτί</option>
						<option value="TG">Τόγκο</option>
						<option value="TK">Τοκελάου</option>
						<option value="TO">Τόνγκα</option>
						<option value="TV">Τουβάλου</option>
						<option value="TR">Τουρκία</option>
						<option value="TM">Τουρκμενιστάν</option>
						<option value="TT">Τρινιντάντ και Τομπάγκο</option>
						<option value="TD">Τσαντ</option>
						<option value="TN">Τυνησία</option>
						<option value="YE">Υεμένη</option>
						<option value="PH">Φιλιππίνες</option>
						<option value="FI">Φινλανδία</option>
						<option value="FJ">Φίτζι</option>
						<option value="CL">Χιλή</option>
						<option value="HK">Χονγκ Κονγκ</option>
				         </select>
                       </div>
                     </div>
                     <div class="form-group">
                            <label class="form-label" for="field-1">Phone number</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="text" name="phoneNum" placeholder="Phone Number" id="phoneNum" size="32" class="form-control" ng-model="domainRegisterInfo.phoneNum" required>
                            </div>
                        </div>
                       <div class="form-group">
                            <label class="form-label" for="field-1">Domain Password</label>
                            <div class="controls">
							 <span class="write-pen">						   
							    <i class="fa fa-pencil"></i>
							   </span>
                                <input type="password" name="litepsd" placeholder="Domain Password" id="litepsd" size="32" class="form-control" ng-model="domainRegisterInfo.litepsd" required>
                            </div>
                        </div> 
                         
                <div class="">
                    <button type="submit" ng-disabled="step1.$invalid" class="btn btn-primary" ng-click="steps.step2=true;submiteUserdata()">Next</button>
                </div>
                    </form>
                
				
				 </div>
				</uib-tab>
                <uib-tab ng-disabled="step1.$invalid" active="steps.step2" select="steps.percent=40; steps.step1stat=true" class=" {{steps.step2stat ? 'complete' : ''}}">
                    <uib-tab-heading class="tabhead tab_ms2">N/S</uib-tab-heading>
                   
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 Name_Servers_form_wrapp"> 
				  <h4 class="form_heading_in_Dashbord">Name Servers Information</h4>
                    <form name="step2" class="form-validation padding-left-0 Name_Servers_form">
                    	<div class="form-group">
                            <label class="form-label" for="field-1"></label>
                            <div class="controls">
							<div class="input-radio_wrp">
							    <span class="inpt-rd">
                                <input name="nameserversbutton" type="radio" id="et1" onClick="return TrHide(true);" value="1">
								</span>
								<span class="inpt-rd-name">I want to set nameservers too</span>
							</div>	
							 <div class="input-radio_wrp">
							       <span class="inpt-rd">
									<input name="nameserversbutton" type="radio" id="et1" onClick="return TrHide(false);" value="1" checked="checked">
									</span>
									<span class="inpt-rd-name">
									I dont want to set nameservers
									</span>
							</div>
							<div class="clearfix"></div>
									
									<div style="display:none" id="ns" class="nameserver_optional_wrapp" > 
									<div class="fill_nameservers_small">
									Fill the nameservers: (Optional)
									</div>
									<div class="row">
										<!-- NS1 -->
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label"> Name Server  </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
										<input class="form-control" ng-model="domainRegisterInfo.ns1" name="ns1" type="text" />
										</div>
										</div>
										
									</div>
									<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Ip  </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
											<input class="form-control" ng-model="domainRegisterInfo.ip1" name="ip1" type="text"  />
										</div>
										</div>
										
										
									</div>
										<!-- End NS1 -->
										<!-- NS2 -->
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Name Server  </label>
										<div class="controls">
										
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
									      <input name="ns2" ng-model="domainRegisterInfo.ns2" class="form-control" type="text" />  
                                         </div>
										 </div>
									
										</div>
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Ip   </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
										<input class="form-control" name="ip2" type="text" ng-model="domainRegisterInfo.ip2" />
										
										 </div>
										 </div>
										 
										</div>
										<!-- End-NS2 -->
										<!-- NS3 -->
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Name Server    </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
									<input name="ns3" type="text" ng-model="domainRegisterInfo.ns2" class="form-control" /> 

									
									</div>
										 </div>
										</div>
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Ip   </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
										<input name="ip3" ng-model="domainRegisterInfo.ip3" type="text" class="form-control" />
										
										 </div>
										 </div>
										</div>
										<!-- End NS3 -->
										<!-- NS3 -->
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Name Server    </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
									<input name="ns4" type="text" ng-model="domainRegisterInfo.ns4" class="form-control" /> 
                                         </div>
										 </div>
										</div>
										<div class="col-md-6 col-lg-6">
										<div class="form-group">
										<label class="form-label">Ip   </label>
										<div class="controls">
										<span class="write-pen">						   
							   <i class="fa fa-pencil"></i>
							   </span>
										<input class="form-control" name="ip4" ng-model="domainRegisterInfo.ip4"  type="text" />
										 </div>
										 </div>
										
										</div>
										<!-- End NS3 -->
									</div>
							</div>
                            </div>
                        </div>
                       
                        <div class="">
                            <button type="button" class="btn btn-primary" ng-click="steps.step1=true">Prev</button>
                            <button type="submit" ng-disabled="step2.$invalid" class="btn btn-primary" ng-click="steps.step3=true">Next</button>
                        </div>
                    </form>
                
				
				</div>
				</uib-tab>
                <uib-tab disabled="step2.$invalid" active="steps.step3" select="steps.percent=60; steps.step2stat=true" class=" {{steps.step3stat ? 'complete' : ''}}">
                    <uib-tab-heading class="tabhead tab_ms3">Payment</uib-tab-heading>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 Payment_API_form_wrapp">      
				  <h4 class="form_heading_in_Dashbord">Payment API Information</h4>
                    <form name="step3" class="form-validation padding-left-0 Payment_API_form">
                    	 <div class="form-group">
                        
                       <!--  <div class="Sample_Massage_show" >
						Need To integrate payment API
						</div> -->
						<input type="radio" checked> <span>Paypal Payment</span> 
                        </div>
                        
                        <div class="">
                            <button type="button" class="btn btn-primary" ng-click="steps.step2=true">Prev</button>
                            <button type="submit" ng-disabled="step3.$invalid" class="btn btn-primary" ng-click="steps.step4=true">Next</button>
                        </div>
                    </form>
              
             </div>
			  </uib-tab>
                <uib-tab disabled="step3.$invalid" active="steps.step4" select="steps.percent=80; steps.step3stat=true" class=" {{steps.step4stat ? 'complete' : ''}}">
                    <uib-tab-heading class="tabhead tab_ms4">Overview</uib-tab-heading>
                    
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 User_domain_Information_form_wrapp"> 
				<h4 class="form_heading_in_Dashbord" >Domain Register Information</h4>
                    <form name="step4" class="form-validation  padding-left-0 User_domain_Information_form">
                        <div class="">
                       <div class="row"> 	
						<div class="col-lg-6">
							
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Full Name</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.fullname}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
								
						   <div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">First Name</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.firstname}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						  </div>
					
                     </div>
					 <div class="row">
					    <div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Last Name</label>
                        	
                        			<div class="controls">
		                                {{domainRegisterInfo.lastname}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Company</label>
                        	
                        			<div class="controls">
		                                {{domainRegisterInfo.titlos}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
					</div>
						<div class="row">
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Email</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.emailText}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Address</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.address1}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						</div>
						<div class="row">
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">State</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.stateProvince}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">City</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.city}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						</div>
						<div class="row">
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">PostCode</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.postcode}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>
						<div class="col-lg-6">
                        		<!-- FullName -->
                        		<div class="form-group">
                        			<label class="form-label" for="field-1">Country</label>
                        		
                        			<div class="controls">
		                                {{domainRegisterInfo.country}}
		                            </div>
                        		</div>
                        		<!-- End-Full -->
						</div>

                        </div>	
                            
                            
                        </div>
						<div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <button type="button" class="btn btn-primary" ng-click="steps.step3=true">Prev</button>
                            <button type="submit" ng-disabled="step4.$invalid" class="btn btn-primary" ng-click="steps.step5=true">Next</button>
                        </div>
						</div>
                    </form>
					
					</div>
                </uib-tab>
                <uib-tab disabled="step4.$invalid" active="steps.step5" select="steps.percent=100; steps.step4stat=true" class=" {{steps.step5stat ? 'complete' : ''}}">
                    <uib-tab-heading class="tabhead tab_ms5">T&C</uib-tab-heading>
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 Termcondision_Information_form_wrapp">                    
				  <h4 class="form_heading_in_Dashbord">Terms and Conditions</h4> 
                    <form name="step5" class="form-validation padding-left-0 Termcondision_Information_form">
                        <div class="form-group">
                            <label class="form-label" for="formfield1"></label>
                            <div class="controls">
                                <label class="form-label checkbox icheck">
                                    <input type="checkbox" ng-model="txtagree" required><i></i> I agree to Terms & Conditions
                                </label>
                            </div>
                        </div>
                        <div class="">
                            <button type="button" class="btn btn-primary" ng-click="steps.step4=true">Prev</button>
                           <!--  <button type="submit" ng-disabled="step5.$invalid" class="btn btn-primary" ng-click="steps.percent=100;steps.step5=true; steps.step5stat=true;piraeusBankApi()">Finish</button> -->
                           <?php $cart = $this->cart->contents();
                        	foreach ($cart as $item){ ?>
                            <a href="<?php echo base_url()?>index.php/Products/buy/<?php echo base64_encode($item['id']);?>/<?php echo base64_encode($item['name']);?>/<?php echo base64_encode($item['subtotal']);?>" ng-disabled="step5.$invalid" class="btn btn-primary" ng-click="steps.percent=100;steps.step5=true; steps.step5stat=true;">Finish </a> 
                            <?php 
                            break;
                        	}
                        	?>
                        </div>
                    </form>
               
                 </div>
			   </uib-tab>
            </uib-tabset>
        </div>
    </div>
</div>