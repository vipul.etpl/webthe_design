
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="padding-top: 20px; padding-bottom: 5px;text-align:center"> 
       <!-- <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>"> -->
       <h3>Happy Edit</h3>
    </div> 
</div>

</div>&nbsp;
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="padding-top: 0px; padding-bottom: 15px;background-color:#f9f9f1;font-size: 15px;color:#737369;padding-left:100px"> 
       <strong>Please login as <?php echo $name."(".$email; ?>) to confirm the account.</strong>
    </div> 
</div>

</div>
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
             style="border: 1px solid #ccc;background-color:white;">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
            <?php //php for register form
                    $data = array('class' => 'form-signin', 'role' => 'form');
                    echo form_open('login/verify_crediantials',$data); 
            ?>
                <h2 class="form-signin-heading" align="center" style="border-bottom:1px solid #bdc5c8; color:#737369;font-size: 20px;padding-bottom:20px">Log In</h2>
                <div align="center">
                <!-- <input type="email" name="" class="form-control" placeholder="Email address" style="width:50%" required autofocus></br>
                    <input type="password" name="" class="form-control" placeholder="Password" style="width:50%" required ></br>-->
                     <?php //php for show error  and succ msg

                         if(validation_errors())
                         {
                            echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                         }    
                        //end php for show error  and succ msg
                     ?>  
                    <table>
                        <tr>
                            <td>
                               <?php
                                
                                $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?> 
                            </td>
                            <td>
                                <?php
                                $data = array('type'=>'email','name' => 'email', 'class' => 'form-control', '
                                placeholder' => 'demo@gmail.com','required' => 'autofocus', 'value' => $email);

                                echo form_input($data);
                                $data = array('type'=>'hidden','name' => 'user_id', 'class' => 'form-control','value' => $user_id);
                                echo form_input($data);
                                $data = array('type'=>'hidden','name' => 'salt', 'class' => 'form-control','value' => $salt);
                                echo form_input($data);
                                $data = array('type'=>'hidden','name' => 'name', 'class' => 'form-control','value' => $name);
                                echo form_input($data);
                                echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';

                            ?>
                            </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                
                                    $data = '<label style="color:#737369;">Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?>
                                
                            </td>
                            <td>
                                
                                <?php
                                $data = array('type'=>'password','name' => 'password', 'class' => 'form-control', 'placeholder' => '*****','required' => 'autofocus', 'value' => set_value('password'));
                                echo form_input($data);
                                echo '<span class="hint">Use alphanumeric Password,e.g admin123<span class="hint-pointer">&nbsp;</span></span>';

                                ?>
                                </br>
                            </td>

                        </tr>
                        <tr align="center">
                        <td align="left" >   
                        </td>
                        <td align="left" style="padding-bottom:10px;padding-top:0px;">
                        <a href="<?php echo base_url(); ?>index.php/forgotpassword">Forgot Passoword?</a>
                        </td>
                    </tr>

                    </table>
                   
               </div>
               <div align="center">
                 <button class="btn btn-primary" type="submit" >Log In</button>
               </div>&nbsp;
          <?php echo form_close(); ?>
     
         </div>
   </div>
</div>
<!--<div class="container" align="center"> 
    <div class="row" >
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            style="border: 1px solid #ccc;background-color:#f9f9f1">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="color:#737369;font-size: 15px">
                    Or Log In with:
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    &nbsp;
                </div>
            </div>
            <img class="img-circle" title="Facebook" width="40" height="30" src="<?= base_url('img/fb.png'); ?>">
            <img class="img-circle" title="Tiwtter" width="40" height="30" src="<?= base_url('img/t.png'); ?>">
            <img class="img-circle" title="G-mail" width="30" height="30" src="<?= base_url('img/g.png'); ?>">
        </div>
    </div>
</div>-->
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            align="center" style="font-size: 13px; height: 32px; padding-top: 8px;background-color:#bfb89c;text-align:center">
            <p style="color:white">Not Registered? <a href="<?php echo base_url(); ?>index.php/signup">Create an account</a>

            </p>  
        </div>
    </div>
</div>
