<!--<div class="container" style="width:100%">
  <div class="row" style="background-color:#ffcc00">
   <div class="col-lg-6">
     <img class="img-responsive" src="<?= base_url('img/logo.png'); ?> ">
   </div>
   <div class="col-lg-3">
   <div class="pull-right">
     <img class="img-responsive" src="<?= base_url('img/profile-icon.png'); ?> ">
     </div>
     <div class="col-lg-3">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $email; ?> <b class="caret"></b></a>
     </div>

   </div>
  </div>
</div> -->
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="padding-top: 20px; padding-bottom: 5px;"> 
          <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>">
    </div> 
</div>

</div>&nbsp;
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
             style="border: 1px solid #ccc;background-color:white">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  

             <?php //php for register form
                    $data = array('class' => 'form-signin');
                    echo form_open('signup/register_user',$data); 
            ?>

                <h2 class="form-signin-heading" align="center" style="border-bottom:1px solid #bdc5c8; color:#737369;font-size: 15px;padding-top:0px;padding-bottom: 20px;">Create an Account</h2>
                <div align="center">
                 <?php //php for show error  and succ msg

                     if(validation_errors())
                     {
                        echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                     }    
                    //end php for show error  and succ msg
                 ?>

                    
                    <table>
                        <tr>
                            <td>
                                <!--<label style="color:#737369;">Name</label>-->
                                <?php
                                
                                $data = '<label style="color:#737369;">Name</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?>

                            </td>
                            <td>
                                <?php
                                $data = array('type'=>'text','id'=>'fname','name' => 'name', 'value'=> $name,'class' => 'form-control', 'placeholder' => 'John Smith', 'value' => set_value('fname'),'required' => 'autofocus');
                                echo form_input($data);
                                echo form_error('name');
                                echo '<span class="hint">Enter First & Last Name<span class="hint-pointer">&nbsp;</span></span>';

                                ?></br>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                
                                $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?>
                                

                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'email','id'=>'email','name' => 'email', 'value'=> $email, 'class' => 'form-control', '
                                placeholder' => 'demo@gmail.com','required' => 'autofocus', 'value' => set_value('email'));
                                echo form_input($data);
                                echo '<span class="hint">e.g. jsmith@gmail.com<span class="hint-pointer">&nbsp;</span></span>';

                            ?></br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                
                                    $data = '<label style="color:#737369;">Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?>
                                

                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'password','id'=>'pass','name' => 'password', 'class' => 'form-control', 'placeholder' => '********','required' => 'autofocus', 'value' => set_value('password'));
                                echo form_input($data);
                                echo '<span class="hint">Use strong password.<span class="hint-pointer">&nbsp;</span></span>';

                                ?></br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <?php
                                
                                    $data = '<label style="color:#737369;">Confirm-Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                            ?>
                                

                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'password','id'=>'cpp','name' => 'cpassword', 'class' => 'form-control', 'placeholder' => '********','required' => 'autofocus', 'value' => set_value('Confirm-Password'));
                                echo form_input($data);
                                echo '<span class="hint">Confirm your password.<span class="hint-pointer">&nbsp;</span></span>';

                                ?>
                               </br>
                            </td>
                        </tr>




                    </table>
                    
               </div>
               <div class="row">
               <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                   
               </div>
                   
               </div>
               <div align="center">
                    <button class="btn default" type="submit" style="background-color:#ffcc00;background-image:none;width:188px;height:40px" onclick="mail_val()">Create New Account</button>
               </div>&nbsp;
         <?php
          echo form_close();
        ?>
                              
            </div>
   </div>
</div>
<div class="container" align="center"> 
    <div class="row" >
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            style="border: 1px solid #ccc;background-color:#f9f9f1">
            <div class="row">
                   <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="color:#737369;font-size: 15px">
                       Or Sign Up with:
                   </div>
              </div>
              <div class="row">
                  <div class="col-lg-12" style="padding-top:5px;padding-bottom:5px">                                                                        
                  </div>
              </div>
              <img class="img-circle" title="Facebook" width="40" style="cursor: pointer;" onclick="do_login()" height="30" src="<?= base_url('img/fb.png'); ?>">
              <a href="<?php echo base_url(); ?>index.php/signup/twitter"><img class="img-circle" title="Tiwtter" width="40" height="30" src="<?= base_url('img/t.png'); ?>"></a>
              <!-- <img class="img-circle" title="G-mail" width="30" height="30" src="<?= base_url('img/g.png'); ?>">  -->
               
                 <span  class="g-signin"
                  data-callback="loginFinishedCallback"
                  data-approvalprompt="force"
                  data-clientid="816096103751-0a3kc8l75e5e9c38a1di9f2kmppi5qe9.apps.googleusercontent.com"
                  data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read"
                  data-height="long"
                  data-cookiepolicy="single_host_origin"
                  >
                </span>
              
              <div class="row">
                  <div class="col-lg-12" style="padding-top:5px;padding-bottom:5px">                                                                        
                  </div>
              </div>
        </div>
    </div>
</div>
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
            align="center" style="font-size: 13px; height: 32px; padding-top: 10px;background-color:#bfb89c;text-align:center">
            <p style="color:white">Already have an account? <a href="<?php echo base_url(); ?>index.php/login">Login</a>

            </p>  
        </div>
    </div>
</div>
<div class="row" align="center">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="padding-top:  5px;">
        <p style="font-size: 12px; color:gray;">By signing up,you agree to our Privacy Policy 
          and  <a href="#">Terms of Service.</a>
        </p>
    </div>
</div>