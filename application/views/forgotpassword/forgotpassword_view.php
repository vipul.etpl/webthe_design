
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="padding-top: 20px; padding-bottom: 5px;text-align: center;"> 
       <!-- <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>"> -->
       <h2>Happy Edit</h2>
       
    </div> 
</div>

</div>&nbsp;
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " 
             style="border: 1px solid #ccc;background-color:white">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
       
           <?php //php for register form
                    $data = array('class' => 'form-signin');
                    echo form_open('forgotpassword/reset_password_email',$data); 
            ?>
                <h2 class="form-signin-heading" align="center" style="border-bottom:1px solid #bdc5c8; color:#737369;font-size: 20px;padding-bottom:20px">Forgot your Password?</h2>
                <div align="center">
                <p style="color:#737369;font-size: 12px;">Submit your email address and we'll send you a link to reset your password.</p>
                     <?php //php for show error  and succ msg

                     if(validation_errors())
                     {
                        echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                     }    
                    //end php for show error  and succ msg
                 ?>
                    <table>
                        <tr>
                            <td>
                               <?php
                                
                                $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?> 
                            </td>
                            <td>
                                <?php
                                $data = array('type'=>'email','name' => 'email', 'class' => 'form-control', '
                                placeholder' => 'demo@gmail.com','required' => 'autofocus', 'value' => set_value('email'));
                                echo form_input($data);
                                echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';

                            ?>
                            </br>
                            </td>
                        </tr>
                    </table> 
               </div>
                <div align="center">
                 <button class="btn btn-primary" type="submit">Submit</button>
                </div>&nbsp;
            </form>
         </div>
   </div>
</div>

