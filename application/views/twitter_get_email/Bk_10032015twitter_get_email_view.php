<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<div class="container"> 
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " align="
      center" style="text-align:center"> 
      <!-- <h2>Happy Edit</h2> -->
       <!-- <img class="img-responsive" style="" src="<?= base_url('img/logo.png'); ?>"> -->
    </div> 
</div>

</div>&nbsp;
<div class="container"> 
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 twitter_signup_wrapp" 
             style="">
            <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
       
           <?php //php for register form
                    $data = array('class' => 'form-signin');
                    echo form_open('signup/twitter_register_user',$data); 
            ?>
                <h2 class="form-signin-heading"  style="">Twitter Signup</h2>
                <div align="center">
                <p class="plz_enter_email_tw">Please enter your email to complete signup process .</p>
                     <?php //php for show error  and succ msg

                     if(validation_errors())
                     {
                        echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                     }    
                    //end php for show error  and succ msg
                 ?>
                 
                        <div class="email_tw_wrp_outer">
                            <div class="email_tw_left">
                               <?php
                                
                                $data = '<label class="Email_lbl_tw"><span class="icon-e"><i class="fa fa-envelope"></i></span> Email</label>&nbsp;&nbsp;&nbsp;' ;
                                echo form_label($data);
                                ?> 
                            </div>
                            <div class="email_tw_right">
                                <?php
                                $data = array('type'=>'email','name' => 'email', 'class' => 'form-control', '
                                placeholder' => 'demo@gmail.com','value' => set_value('email'));
                                echo form_input($data);
                                echo '<span class="hint"><span class="hint-pointer">&nbsp;</span></span>';

                            ?>
                            </br>
                            </div>
                        </div>
                    
               </div>
                <div align="center">
				<div class="clearfix"></div>
                 <p class="btn-block btn-tw-org" type="submit" >Submit</p>
                </div>&nbsp;
            </form>
         </div>
   </div>
</div>

