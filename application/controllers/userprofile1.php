<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');   

class Userprofile extends CI_Controller {

	public function index(){
       $data = array();
       $data['title'] = "One Minute Receipt - User Profile";
       $this->load->view('includes/default_header', $data);  
       $this->load->view('userprofile/userprofile_view',$this->session->all_userdata());

       $this->load->view('includes/page_end');

    }
    
//******************************
  public function profile_email(){
        $this->load->model('userprofile_model');
        $this->load->model('signup_model');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean');
        $data = array();
        $data['email'] = $this->input->post('email');
        $data['user_id'] = $this->input->post('user_id');
        //$data['password'] = $this->input->post('password');
        if($this->form_validation->run() == FALSE){ 
            $this->update_profile_view($data);
        }else{
          if($this->userprofile_model->update_user_info($data))
          {
            $this->form_validation->set_message('profile_update', 'Update.');
            return TRUE;
          
          }else{ 
              $this->update_profile_view($data);
              $this->form_validation->set_message('profile_update', 'Invalid entry.');
              return FALSE;
          }            
        }

    }

    public function profile_password(){
        $this->load->model('userprofile_model');
        $this->load->model('signup_model');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[30]');
        $this->form_validation->set_rules('cpassword', 'Re-Password', 'trim|required|matches[password]');
        $data = array();
        $data['email'] = $this->input->post('email');
        $data['user_id'] = $this->input->post('user_id');
        $data['password'] = $this->input->post('password');
        //echo $this->signup_model->encrypt_password($data);
        if($this->form_validation->run() == FALSE){ 
            $this->update_profile_view($data);
        }else{

          // Update crediantials 
          

          //if ($this->signup_model->encrypt_password($data)){
          if($this->userprofile_model->update_user_password($data))
          {
            $this->form_validation->set_message('profile_update', 'Update.');
            return TRUE;
          
          }else{ 
              $this->update_profile_view($data);
              $this->form_validation->set_message('profile_update', 'Invalid entry.');
              return FALSE;
          }            
        }

    }
    public function update_profile_view($udata){
       $data = array();
       $data['title'] = "One Minute Receipt - User Profile";
       $this->load->view('includes/default_header', $data);  
       $this->load->view('includes/modal_help');
       $this->load->view('userprofile/userprofile_view', $udata);  
       $this->load->view('includes/page_end');
    }
        public function space_check($str) {
         $pos = strrpos($str, " ");
         if ($pos === false) { // note: three equal signs
          // not found...
          $this->form_validation->set_message('space_check', 'The %s field should contain SPACE between First name and Last name');
          return FALSE;
        }
        else  {
          return true;
        }
  }

  public function profile(){
       $data = array();
       $data['title'] = "One Minute Receipt - User Profile";
       $this->load->view('includes/default_header', $data);
       $this->load->view('profile/profile_view',$this->session->all_userdata());
       $this->load->view('includes/page_end');

     
  }

  function post_action()
  {
  
    if(($_POST['email'] == ""))
    {
      $message = "Please fill up blank fields";
      $bg_color = "#FFEBE8";
    
    }else{
      $message = "Username and password matched.";
      $bg_color = "#FFA";
    }
    
    $output = '{ "message": "'.$message.'", "bg_color": "'.$bg_color.'" }';
    echo $output;
  }

  public function verifyUser()  {
    $userName =  $_POST['userName'];
    $userPassword =  $_POST['userPassword'];
    $status = array("STATUS"=>"false");
    if($userName=='admin' && $userPassword=='admin'){
      $status = array("STATUS"=>"true");  
    }
    echo json_encode ($status) ;  
  }


  function create(){
    $this->load->model('userprofile_model');
  if($_POST['data'])
    {
        $data=$_POST['data'];
        
        $data = mysql_escape_String($data);
        $sql = "update users set email='$data' where user_id='2'";
        mysql_query( $sql);
    }
}
//******************************

  }
    