
<div class="container" style="width: 100%;margin-top: 0px;">
  <div class="row">
      <div class="col-lg-2 col-md-2" style="background-color: #f4f2eb; padding-top: 20px;height:100%">
        <!-- Menu bar - Left-->
            <div class="container">
            <div class="row">
              <div class="col-lg-2" id="left_menu">
                <ul class="nav nav-list">
                    <li class=""><a style="color:#737369" href="<?php echo base_url(); ?>index.php/dashboard"><img src="<?= base_url('img/menubar-dashboard.png'); ?>"> &nbsp;Dashboard</a></li>
                    <li class="" style="background-color:#ffcc00"><a style="color:#737369" href="<?php echo base_url(); ?>index.php/userprofile"><img src="<?= base_url('img/menubar-profile-settings.png'); ?>"> &nbsp;Profile Setting</a></li>
                    <li><a style="color:#737369" href="<?php echo base_url(); ?>index.php/login/logout"><img src="<?= base_url('img/menubar-logout.png'); ?>"> &nbsp;Logout</a></li>
                    
                </ul>
              </div>
              </div>
            </div>
        <!-- /Menu bar -->
      </div>&nbsp;
      <div class="col-lg-10 col-md-10" style="padding-top: 20px;max-height:100%">
      <?php $user = $this->session->userdata('user_id'); ?>
          <!-- Right Content -->
            <div class="container" style="width:100%">
               <div class="row" style="background-color:#eeeeee;height:31px">
                <div class="col-lg-12 col-md-12">
                  <i class="fa fa fa-wrench fa-2x"> Account Setting</i>
                  <a class="pull-right" href="userprofile/profile_setting"><i class="fa fa fa-cog fa-2x"></i></a>  
                </div>
                <div class="col-lg-12 col-md-12" style="padding-top:30px;padding-bottom:30px;">
                <table>
                    <tr>
                      <td>
                     <a data-toggle="modal" href="#myModal2" class="" style="color:#737369">Change Name</a>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                    </tr>
                </table>
                  <table>
                    <tr>
                      <td>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                    </tr>
                </table>&nbsp;&nbsp;&nbsp;

                <table>
                    <tr>
                      <td>
                       <a data-toggle="modal" href="#myModal5" class="" style="color:#737369">Change Username</a>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                    </tr>
                </table>&nbsp;&nbsp;&nbsp;

                 <table>
                  <tr>
                      <td>
                        <a data-toggle="modal" href="#myModal" class="" style="color:#737369">Change EmailId</a>
                      </td>
                      <td id="">
                      </td>
                      <td>
                      </td>
                      <td>
                      </td>
                  </tr>
                </table>&nbsp;&nbsp;&nbsp;

                 <table>
                    <tr>
                        <td>
                          <a data-toggle="modal" href="#myModal1" class="" style="color:#737369">Change Password</a>
                        </td>
                        <td>
                        </td>
                        <td> 
                        </td>
                        <td> 
                        </td>
                    </tr>
                </table>
                  
              </div>
                      
                    </div>
              </div>
          <!-- /Right Conent -->
      </div>
  </div>
</div>

<!--Chnage Email Popup-->
<div class="modal" id="myModal" style="padding-top:20px" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           
          <h4 class="modal-title">Email-Address:</h4>
        </div>
        <div class="modal-body">
        
          <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?> 
         <?php //php for register form
              $data = array('class' => 'form-signin', 'role' => 'form');
              //echo form_open('userprofile/profile_email',$data); 
        ?>
          
            <?php //php for show error  and succ msg

                 if(validation_errors())
                 {
                    echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                 }    
                //end php for show error  and succ msg
             ?> 
     <table>
                <tr>
                    <td>
                       <?php
                            $data = '<label style="color:#737369;">Email</label>&nbsp;&nbsp;&nbsp;' ;
                            echo form_label($data);
                        ?> 
                    </td>
                    <td>
                    <?php
                        $data = array('type'=>'email','name' => 'email', 'class' => 'form-control', '
                        placeholder' => 'demo@gmail.com','required' => 'autofocus', 'value' =>$email,'id' =>'cng_email');
                         echo form_input($data);
                        
                        //echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';
                    ?>
                    </br>
                    </td>
                </tr>
      </table>
        </div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Close</a>
         
          <button class="btn default" type="button" style="background-color:#ffcc00;background-image:none;width:188px;height:40px" onclick="change_email(<?php echo $user_id; ?>)">Update</button>
         <?php
          //echo form_close();
        ?>
        </div>
      </div>
    </div>
</div>
<!--/////Chnage Email Popup-->

<!--Change Password-->
<div class="modal" id="myModal1" style="padding-top:20px">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" style="color:#efbf00">Change Password</h4>
        </div>
        <div class="modal-body"><!--Start Model body-->
         <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?>  
            <?php //php for register form
                  $data = array('class' => 'form-signin', 'role' => 'form');
                  //echo form_open('',$data); 
            ?>
            <?php //php for show error  and succ msg

                         if(validation_errors())
                         {
                            echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                         }    
                        //end php for show error  and succ msg
                     ?>  

                     <div id="message"></div>

                    <table><!--Start Table-->
                       <tr><!--For New Password-->
                       
                            <td>
                               <?php
                                    $data = '<label style="color:#737369;">Old-Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?> 
                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'password','id'=>'password','name' => 'password', 'class' => 'form-control', '
                                placeholder' => '*****','required' => 'autofocus', 'value' =>'');
                                 echo form_input($data);
                                 $data = array('type'=>'hidden','name' => 'user_id', 'id'=>'user_id','class' => 'form-control','value' => $user);
                                echo form_input($data);
                                //echo '<span class="hint">Enter New password<span class="hint-pointer">&nbsp;</span></span>';
                            ?>
                            </br>
                            <td id="txthint"></td>
                            </td>
                        </tr><!--End For New Password-->

                         <tr><!--For New Password-->
                         
                            <td>
                               <?php

                                    $data = '<label style="color:#737369;">New-Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?> 
                            </td>
                            <td>
                                <?php

                                    $data = array('type'=>'password','name' => 'npassword', 'class' => 'form-control', '
                                    placeholder' => '*****','required' => 'autofocus', 'value' =>'','id'=>'npassword');
                                    echo form_input($data);
                                ?>
                            </br>
                            </td>
                        </tr><!--End For New Password-->


                        <tr><!--For Confirm New Password-->
                            <td>
                               <?php
                                    $data = '<label style="color:#737369;">Confirm-Password</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?> 
                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'password','name' => 'cpassword', 'class' => 'form-control', '
                                placeholder' => '*****','required' => 'autofocus', 'value' =>'','id'=>'cpassword');
                                 echo form_input($data);
                                // $data = array('type'=>'hidden','name' => 'user_id', 'class' => 'form-control','value' => $user_id);
                                //echo form_input($data);
                                //echo '<span class="hint">Enter same password<span class="hint-pointer">&nbsp;</span></span>';
                            ?>
                             
                            </br>
                            </td>
                        </tr><!--End For Confirm New Password-->
                    </table><!--End Table-->
          
        </div><!--End Model body-->
        <div class="modal-footer">
         <span id="btn">
              <a  href="#" data-dismiss="modal" class="btn"><button class="btn default"  align="left"  type="submit" style="background-color: #ffcc00;background-image:none;width:77px;height:30px;">Cancle</button></a>
              <button class="btn default" align="left" onclick="change_password(<?php echo $user_id; ?>)"  type="button" style="background-color: #565555;color:white;background-image:none;width:77px;height:30px;">Save</button></span>
        </div>
      </div>
    </div>
</div>
<!--End Change Password-->


<!--Chnage Name Popup-->
<div class="modal" id="myModal2" style="padding-top:20px">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" style="color:#efbf00">Change Name:</h4>
        </div>
        <div class="modal-body"><!--Start Modal body-->
        <div id="shw_msg" align="center"></div> 
          <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?> 

            <?php //php for register form
                  $data = array('class' => 'form-signin', 'role' => 'form');
                  //echo form_open('userprofile/profile_name_update',$data); 
            ?>
            <?php //php for show error  and succ msg

                 if(validation_errors())
                 {
                    echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                 }    
                //end php for show error  and succ msg
             ?> 
             <table><!--Table Start-->
                        <tr><!--First-name-->
                            <td>
                               <?php
                                    $data = '<label style="color:#737369;">First Name</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?> 
                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'text','name' => 'first_name', 'class' => 'form-control', '
                                placeholder' => 'John','required' => 'autofocus', 'value' =>$first_name, 'id' => 'cng_first_name');
                                 echo form_input($data);
                                 $data = array('type'=>'hidden','name' => 'user_id', 'class' => 'form-control','value' => $user_id);
                                echo form_input($data);
                               // echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';
                            ?>
                            </br>
                            </td>
                        </tr><!--////First-name-->

                         <tr><!--Last-name-->
                            <td>
                               <?php
                                    $data = '<label style="color:#737369;">Last Name</label>&nbsp;&nbsp;&nbsp;' ;
                                    echo form_label($data);
                                ?> 
                            </td>
                            <td>
                            <?php
                                $data = array('type'=>'text','name' => 'last_name', 'class' => 'form-control', '
                                placeholder' => 'Willem','required' => 'autofocus', 'value' =>$last_name, 'id' => 'cng_last_name');
                                 echo form_input($data);
                                
                               // echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';
                            ?>
                            </br>
                            </td>
                        </tr><!--//////last-name-->
                    </table><!--/////Table End-->

        </div><!--End Modal body-->
        <div class="modal-footer">
         <span id="btn">
              <a  href="#" data-dismiss="modal" class="btn"><button class="btn default"  align="left"  type="submit" style="background-color: #ffcc00;background-image:none;width:77px;height:30px;">Cancle</button></a>
              <button class="btn default" align="left"  type="button" onclick="change_name(<?php echo $user_id; ?>)" style="background-color: #565555;color:white;background-image:none;width:77px;height:30px;">Save</button></span>
          <?php
         // echo form_close();
        ?>
        </div>
      </div>
    </div>
</div>
<!--/////End Chnage Name Popup-->


<!--Chnage Email Popup-->
<div class="modal" id="myModal5" style="padding-top:20px">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           <div id="container"></div>
          <h4 class="modal-title">Username:</h4>
        </div>
        <div class="modal-body">
        <div id="shw_msg" align="center"></div> 
          <?php //php for succ and error msg
                if($this->session->flashdata('msg') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('msg').'</div>
                    </div>';
                }
                if($this->session->flashdata('success') != '')
                {
                    echo '
                    <div class="row-fluid">
                        <div class="span12 alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$this->session->flashdata('success').'</div>
                    </div>';
                }
                //end php for succ and error msg    
            ?> 
         <?php //php for register form
              $data = array('class' => 'form-signin', 'role' => 'form');
              //echo form_open('userprofile/profile_email',$data); 
        ?>
          
            <?php //php for show error  and succ msg

                 if(validation_errors())
                 {
                    echo'<div class="row"><div class="col-lg-12 alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'.validation_errors().'</div></div>';
                 }    
                //end php for show error  and succ msg
             ?> 
     <table>
                <tr>
                    <td>
                       <?php
                            $data = '<label style="color:#737369;">Username</label>&nbsp;&nbsp;&nbsp;' ;
                            echo form_label($data);
                        ?> 
                    </td>
                    <td>
                    <?php
                        $data = array('type'=>'text','name' => 'username', 'class' => 'form-control', '
                        placeholder' => 'demo@gmail.com','required' => 'autofocus', 'value' =>$username,'id' =>'cng_username');
                         echo form_input($data);
                        
                        //echo '<span class="hint">Enter Email address with proper domain<span class="hint-pointer">&nbsp;</span></span>';
                    ?>
                    </br>
                    </td>
                </tr>
      </table>
        </div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Close</a>
         
          <button class="btn default" type="button" style="background-color:#ffcc00;background-image:none;width:188px;height:40px" onclick="chnage_username(<?php echo $user_id; ?>)">Update</button>
         <?php
          //echo form_close();
        ?>
        </div>
      </div>
    </div>
</div>
<!--/////Chnage Email Popup-->





