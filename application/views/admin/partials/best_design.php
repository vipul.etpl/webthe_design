<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Dashboard</a>
    </li>
    <li class="active">Projects</li>
</ol>
<div class="row" ng-controller="ProjectList">
    <div class="col-md-12">
        <div class="row">
            <input type="text" class="form-control" placeholder="Search projects.." ng-model="projectSearch.project_name" style="margin: 0 0 12px;">
            <div class="col-sm-3 col-lg-3 col-md-3" ng-repeat="data in list | filter:projectSearch">
                <div class="thumbnail no-padding">
                    <img class="img-responsive" src="http://placehold.it/320x280" alt="">
                    <div class="caption">
                        <h4><a href="javascript:void(0);" ng-click="openProjectFile('{{data.project_name}}','{{data.user_id}}')">{{data.project_name}}</a>
                        </h4>
                        <p><b>User:</b> <a href="#">{{data.first_name}} {{data.last_name}}</a></p>
                         <p><b>Date:</b> {{data.project_date}}</p>
                    </div>
                    <div class="ratings pad bg-primary">
                     <input type="checkbox" class="group1" name="bestdesign[]" value="{{data.project_name}}">&nbsp;&nbsp;Select This       
                    </div>
                </div>
            </div>
            <p ng-show="(list | filter:projectSearch).length == 0" style="font-size: 16px;text-align: center;">No data found</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="list == 101"> 
                  No data found  
            </div>

        </div>
        <div class="box-footer" ng-show="(list | filter:projectSearch).length != 0" style="margin:0 0 0 15px">
          <div class="alert alert-success" ng-show="infovars">{{infomsgs}}</div>
          <button class="btn btn-primary" ng-click="makeBestDesigns()">Submit</button>
        </div>

    </div>
      
    

</div>


