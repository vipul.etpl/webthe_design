<h2 class="chatapi-head">Cart

                <a href data-toggle="chatbar" class="toggle_chat" ng-click="app.settings.chatFolded = !app.settings.chatFolded; menuChatToggle('chat',app.settings.chatFolded) ">
                    <i class="material-icons">close</i>
                </a>

</h2>
<perfect-scrollbar class="chat-wrapper" wheel-propagation="true" suppress-scroll-x="true"  min-scrollbar-length="20">
    <div class="">
        <h4 class="group-head caption-heading-cart_tbl">Products on Your Shopping Cart</h4>
        <div id="cart" >
            <div id = "heading">
                <!-- <h2 align="center">Products on Your Shopping Cart</h2> -->
            </div>
            
                <div id="text"> 
            <?php  $cart_check = $this->cart->contents();
            
            // If cart is empty, this will show below message.
             if(empty($cart_check)) {
             echo '<span class="cartMsgBorder"> To add products to your shopping cart click on "Cart symbol" Button </span>'; 
             }  ?> </div>
            
                <table id="table" class="cart_detail_table " border="0" cellpadding="5px" cellspacing="1px" class="group-head">
                  <?php
                  // All values of cart store in "$cart". 
                  if ($cart = $this->cart->contents()): ?>
                    <tr id= "main_heading">
                        <!-- <td>Serial</td> -->
                        <th width="42%" >Name</td>
                        <th width="24%">Price</td>
                        <th width="10%">Qty</td>
                        <th width="24%">Amount</td>
                        <!-- <td>Cancel Product</td> -->
                    </tr>
                    <?php
                     // Create form and send all values in "shopping/update_cart" function.
                    echo form_open('shopping/update_cart');
                    $grand_total = 0;
                    $i = 1;

                    foreach ($cart as $item):
                        //   echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
                        //  Will produce the following output.
                        // <input type="hidden" name="cart[1][id]" value="1" />
                        
                        echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
                        echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
                        echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
                        echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
                        echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
                        ?>
                        <tr>
                            <!-- <td>
                       <?php echo $i++; ?>
                            </td> -->
                            <td>
                      <?php echo $item['name']; ?>
                            </td>
                            <td>
                                $ <?php echo number_format($item['price'], 2); ?>
                            </td>
                            <td>
                            <?php echo form_input('cart[' . $item['id'] . '][qty]',$item['qty'], 'maxlength="3" size="1" disabled style="text-align: right"'); ?>
                            </td>
                        <?php $grand_total = $grand_total + $item['subtotal']; ?>
                            <td>
                                $ <?php echo number_format($item['subtotal'], 2) ?>
                            </td>
                            <!-- <td>
                              
                            <?php 
                            // cancle image.
                            $path = "<img src='http://localhost/codeigniter_cart/images/cart_cross.jpg' width='25px' height='20px'>";
                            echo anchor('shopping/remove/' . $item['rowid'], $path); ?>
                            </td> -->
                     <?php endforeach; ?>
                    </tr>
                    <tr class="border_cart_tbl_row">
                        <td colspan="4"><b class="ordr_tl">Order Total: $<?php 
                        
                        //Grand Total.
                        echo number_format($grand_total, 2); ?></b></td>
                        
                        <?php // "clear cart" button call javascript confirmation message ?>
                        <!-- <td colspan="5" align="right"><input type="button" class ='fg-button teal' value="Clear Cart" onclick="clear_cart()"> -->
                            
                            <?php //submit button. ?>
                            <!-- <input type="submit" class ='fg-button teal' value="Update Cart"> -->
                            <?php echo form_close(); ?>
                            
                            <!-- "Place order button" on click send "billing" controller  -->
                            <!-- <td></td> -->
                    </tr>

            </table>
        </div>
        <!-- <h5 class="group-head pull-right"> --><input type="button" class ='btn btn-primary btn-xs cart_btn_sidebar' value="Continue to cart" ui-sref="app.cart" ng-click="app.settings.chatFolded = !app.settings.chatFolded; menuChatToggle('chat',app.settings.chatFolded) "><!-- </h5> -->
        <?php endif; ?>
    </div>
</perfect-scrollbar>
<script>
$(document).ready(function()
{
    $(".chat-wrapper").css({"height":$(window).height(),"overflow-y":"auto"});
});
</script>
<!--  -->