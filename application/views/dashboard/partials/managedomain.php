<!-- WWW logo start -->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		 <div class="www_icon_wrp">
			<svg version="1.1" class="domain-ic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="4 -4 25 25" style="enable-background:new 4 -4 25 25;" xml:space="preserve"><g id="Layer_1_1_" style="display:none;"><g id="XMLID_19_" style="display:inline;"><path id="XMLID_80_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M8.4,14.9l6.1-6.3"></path><path id="XMLID_78_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M24.7,14.9l-5.9-6.3"></path><path id="XMLID_76_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M25,3l-8.4,7.6L8,3"></path><path id="XMLID_74_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M25.5,15.5h-18v-13h18
			V15.5z"></path></g></g><polygon points="10.7,11 12,6 11.4,6 10.3,10 8.9,6 8.2,6 6.8,10 5.8,6 5,6 6.4,11 7,11 8.4,7 10,11 "></polygon><polygon points="16.8,6 16.1,6 14.7,10 13.7,6 13,6 14.4,11 15,11 16.4,7 17.9,11 18.6,11 19.9,6 19.3,6 18.2,10 "></polygon><polygon points="27.3,6 26.2,10 24.8,6 24.1,6 22.7,10 21.7,6 21,6 22.4,11 23,11 24.4,7 25.9,11 26.6,11 27.9,6 "></polygon><path d="M16.4,1c2.683,0,5.181,1.373,6.681,3.673l0.838-0.546C22.233,1.542,19.423,0,16.4,0c-2.916,0-5.759,1.581-7.419,4.127 l0.838,0.546C11.296,2.407,13.818,1,16.4,1z"></path><path d="M16.4,16c-2.684,0-5.182-1.373-6.681-3.674l-0.838,0.547C10.566,15.457,13.376,17,16.4,17c3.054,0,5.867-1.583,7.524-4.235 l-0.848-0.529C21.603,14.593,19.107,16,16.4,16z"></path><g id="Layer_3" style="display:none;"><g id="XMLID_17_" style="display:inline;"><path id="XMLID_20_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M16,1.6
			c3,0,5.5,2.5,5.5,5.5S19,12.6,16,12.6s-5.5-2.5-5.5-5.5S13,1.6,16,1.6z"></path><path id="XMLID_18_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M20,13.9v5l-4-1.8l-4,1.8 v-5c-2.4-1.4-4-4-4-6.9c0-4.4,3.6-7.9,8-7.9s8,3.5,8,7.9C24,9.9,22.4,12.5,20,13.9z"></path></g></g></svg>
		</div>
		<div class="www_icon_wrp_domains_txt">
			<span>Domains</span>
			</div>
		</div>
	</div>
</div>
<!-- WWW logo start -->
			<div class="add-domains-panel">
                <div class="">
                    <div class="add-domains-header">
                        Get a Domain Name for Your Site
                        <div class="replace-free-site-url">Replace <b><?php echo base_url();?></b> <br> with your own site address</div>
                    </div>
                </div>
                <div class="actions-area">
                   <a ui-sref="app.searchdomain"> <div class="action-button-container">
                        <div class="action-icon-container">
                            <div class="buy-domain-icon"><img src="../img/buy_new_domain.png" alt="buy-domain" class="img-responsive"></img>  </div>
                        </div>
                        <div class="action-text-container">
                            <div class="action-header">Buy a new domain name</div>
                        </div>
                        <div class="action-arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                    </div></a>

                    <div class="action-button-container">
                        <div class="action-icon-container">
                            <div class="connect-domain-icon"> <img src="../img/connect_to_already_have_domain.png" alt="Connect-domain" class="img-responsive"></img> </div>
                        </div>
                        <div class="action-text-container">
                            <div class="action-header">Connect a domain you already own</div>
                        </div>
                        <div class="action-arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>

                    </div>
                </div>
            </div>

<!--  -->

<!-- Style for domain page -->
<style type="text/css">

</style>