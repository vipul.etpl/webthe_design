<?php /* Smarty version Smarty-3.1.21, created on 2016-05-26 07:29:27
         compiled from "/home/etpl2012/public_html/whmcs/templates/six/includes/panel.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18411689765746ec27d94c58-82184433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d290e61c79e1653a9453b48855d8bc8b2105928' => 
    array (
      0 => '/home/etpl2012/public_html/whmcs/templates/six/includes/panel.tpl',
      1 => 1464185232,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18411689765746ec27d94c58-82184433',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'type' => 0,
    'headerTitle' => 0,
    'bodyContent' => 0,
    'bodyTextCenter' => 0,
    'footerContent' => 0,
    'footerTextCenter' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5746ec27df9c16_98301108',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5746ec27df9c16_98301108')) {function content_5746ec27df9c16_98301108($_smarty_tpl) {?><div class="panel panel-<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
    <?php if (isset($_smarty_tpl->tpl_vars['headerTitle']->value)) {?>
        <div class="panel-heading">
            <h3 class="panel-title"><strong><?php echo $_smarty_tpl->tpl_vars['headerTitle']->value;?>
</strong></h3>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bodyContent']->value)) {?>
        <div class="panel-body<?php if (isset($_smarty_tpl->tpl_vars['bodyTextCenter']->value)) {?> text-center<?php }?>">
            <?php echo $_smarty_tpl->tpl_vars['bodyContent']->value;?>

        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['footerContent']->value)) {?>
        <div class="panel-footer<?php if (isset($_smarty_tpl->tpl_vars['footerTextCenter']->value)) {?> text-center<?php }?>">
            <?php echo $_smarty_tpl->tpl_vars['footerContent']->value;?>

        </div>
    <?php }?>
</div>
<?php }} ?>
