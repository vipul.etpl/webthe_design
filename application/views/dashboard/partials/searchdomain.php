<!-- WWW logo start -->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		    <div class="www_icon_wrp">
			<svg version="1.1" class="domain-ic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="4 -4 25 25" style="enable-background:new 4 -4 25 25;" xml:space="preserve"><g id="Layer_1_1_" style="display:none;"><g id="XMLID_19_" style="display:inline;"><path id="XMLID_80_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M8.4,14.9l6.1-6.3"></path><path id="XMLID_78_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M24.7,14.9l-5.9-6.3"></path><path id="XMLID_76_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M25,3l-8.4,7.6L8,3"></path><path id="XMLID_74_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M25.5,15.5h-18v-13h18
			V15.5z"></path></g></g><polygon points="10.7,11 12,6 11.4,6 10.3,10 8.9,6 8.2,6 6.8,10 5.8,6 5,6 6.4,11 7,11 8.4,7 10,11 "></polygon><polygon points="16.8,6 16.1,6 14.7,10 13.7,6 13,6 14.4,11 15,11 16.4,7 17.9,11 18.6,11 19.9,6 19.3,6 18.2,10 "></polygon><polygon points="27.3,6 26.2,10 24.8,6 24.1,6 22.7,10 21.7,6 21,6 22.4,11 23,11 24.4,7 25.9,11 26.6,11 27.9,6 "></polygon><path d="M16.4,1c2.683,0,5.181,1.373,6.681,3.673l0.838-0.546C22.233,1.542,19.423,0,16.4,0c-2.916,0-5.759,1.581-7.419,4.127 l0.838,0.546C11.296,2.407,13.818,1,16.4,1z"></path><path d="M16.4,16c-2.684,0-5.182-1.373-6.681-3.674l-0.838,0.547C10.566,15.457,13.376,17,16.4,17c3.054,0,5.867-1.583,7.524-4.235 l-0.848-0.529C21.603,14.593,19.107,16,16.4,16z"></path><g id="Layer_3" style="display:none;"><g id="XMLID_17_" style="display:inline;"><path id="XMLID_20_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M16,1.6
			c3,0,5.5,2.5,5.5,5.5S19,12.6,16,12.6s-5.5-2.5-5.5-5.5S13,1.6,16,1.6z"></path><path id="XMLID_18_" style="fill:none;stroke:#2B5672;stroke-linejoin:round;stroke-miterlimit:10;" d="M20,13.9v5l-4-1.8l-4,1.8 v-5c-2.4-1.4-4-4-4-6.9c0-4.4,3.6-7.9,8-7.9s8,3.5,8,7.9C24,9.9,22.4,12.5,20,13.9z"></path></g></g></svg>
		    </div>
			<div class="www_icon_wrp_domains_txt">
			<span>Domains</span>
			</div>
			<div class="back_to_domain">
			  <a href="#/app/managedomain" class="fake-blue-anchor btn btn-success btn-xs"><i class="fa fa-angle-double-left"></i> Back to Domains</a>
			</div>
			
		</div>
	</div>
</div>
<!-- WWW logo start -->
<div class="search-domains-upper-panel-wrapper">
        <div class="search-domains-upper-panel">

            <!-- <div class="go-to-domains-link-container"> -->
              
            <!-- </div> -->

            <div class="search-domains-header-container">
                <div class="add-domains-header">
                    Get a Domain Name for Your Site <br>
                    <div class="replace-free-site-url">Replace &nbsp<b><?php echo base_url();?></b><br>with your own site address</div>
                </div>

            </div>
			
			
			
		    <div class="domain-search-container" ng-controller="getdomainsearchlist">
		        <!-- <form name="searchForm" novalidate="" class=""> -->
		            <!-- <div class="container"> -->
		            	<div class="row">
		            		<!-- <div class="col-lg-2">
		            		 </div>  -->

		            		<div class="col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10 col-offset-lg-1 col-lg-10">
		            			<!-- <input type="text" class="form-control ser_in" placeholder="Find a great domain for your site" name="domainName" id="domainName">
		                		 <button class=" btn btn-primary fake-blue-anchor">Search</button> -->
		                		 <h4>{{domainMsg}}</h4>
		                		<div class="domainbox_wrapp devlpr_css_for_domainbox">

									<form name="domainSearch">
									 <div class="domainbox" style="font-family:Verdana; font-size:13px">
									  
									  <div class="domainSearch_with_username">
									    <div class="hidden_input"> 
									     <input name="code" ng-model="domainname.code"  type="hidden"/>
									    </div>
									   							    
									    <div class="www_wrapp"><strong>www.</strong></div>
									    
									     <div class="domain_name_after_www">
									      <input name="domainName" id="domainName" type="text" class="form-control" ng-model="domainname.domainName" size="30">
									     </div>
									     <div class="hidden_input">
									      <input type ="hidden" name="do" ng-model="domainname.do" id="do"> 
									     </div>
										 <div class="domain_search_btn_wrapp">
										   <!--<input type="button" class="btn btn-primary btn-orange" name="Submit" value="Search" ng-click="senddomainrequest()"><i class="fa fa-search"></i></input>-->
											<button class="btn btn-primary btn-orange" ng-click="senddomainrequest()"><i class="fa fa-search"></i>&nbsp;Search</button>
										 </div>
									   


									   </div>
                                        <div class="clearfix"></div>
									   <!---spinloader wrapp---->  
								<div class="spinloader_wrp" ng-show="spinloader">
								<div class="clearfix"></div>
									<div class="col-md-12 col-lg-12 alert alert-success spinloader_alert">
										Please wait &nbsp; <i class="fa-li fa fa-spinner fa-spin" style="position:static"></i>
									</div>
								</div>
								<!---spinloader End----> 
									   
									   
									   
									   
                                      <div class="clearfix"></div>
									  <div class="all_checkbox_wrapp">

									  <div class="checkbox_single">

									  	<label ng-repeat="role in roles" class="single_chk_outer">
										  <span class="single_chk_input icheck icheck-primary">
										  	<input class="" type="checkbox" checklist-model="user.roles" checklist-value="role"><i></i>
										  </span>
										  <span class="single_chk_name">
										  {{role}}	
										  </span>
										   
										</label>
                                       </div>
                                       <div class="clearfix"></div>
                                       <div class="hidden_input">
										<input type="hidden" name="ext" value="{{user.roles | json}}" id="ext"> 
									   </div>

									  </div>
									  <div>
									  	<div class="all_btn_after_checkbox_wrapp">
									  	<div class="single_btn_of_group">
									  		<button ng-click="checkAll()" class="btn btn-primary"><!-- <i class="fa fa-check-square-o"></i> -->Check all</button>
									  	</div>
									  	<div class="single_btn_of_group">	
								            <button ng-click="uncheckAll()" class="btn btn-primary"><!-- <i class="fa fa-square-o"></i> -->Uncheck all</button>
								          </div>
									  	<!-- <div class="single_btn_of_group">	
								          	<button ng-click="checkFirst()" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Check first</button>
									  	</div> -->
									  

									  	</div>
									  </div>
									  </div>
									</form><br><br>

<div class="clearfix"></div>
                                  </div><!---domainbox_wrapp-End---->
								
								
								<div class="avli_domain" ng-show="domainList">
									<h4>Total Available Domain - {{jsonData.arrayAvDomains.length}}</h4>
									<div class="col-lg-12 avi_res avl_domn-wr_12" ng-repeat='avalDomain in jsonData.arrayAvDomains' id="domainlist{{$index}}">
									<!-- <span class="single_chk_input icheck icheck-primary"> -->
										<input name="box_{{$index}}" disabled = "disabled" class="chkBoxval domainck{{$index}}" type="checkbox" id ="{{$index}}" value="{{avalDomain}}">
									<!-- <i></i>
									</span> -->

										{{avalDomain}}<span id="domainprice{{$index}}"><i class="fa fa-usd"></i></span> <button class="btn btn-primary martp10 pull-right btn-xs view_prz_btn" ng-click="chkPriceVal('{{avalDomain}}','{{$index}}')"><i class="fa fa-eye"></i> &nbsp;View price</button>
										  
										  <!-- {{extPrice}} <p ng-repeat="extPrice in priceData track by $index">{{extPrice}}</p> -->
										<input type="hidden" name="domainNames" value="">
										<input type="hidden"  name="cnt" value="{{jsonData.arrayAvDomains.length}}">
										 
									</div>
									<div class="col-lg-12 add_cartbtntp">
										<button class="btn btn-primary" ng-click="addDomainTocart()"><i class="fa fa-cart-plus fa-sm"></i>Add to cart</button>
										<!-- Modal -->
											<div class="modal fade" id="domainRegisterSetp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											  <div class="modal-dialog modal-lg" role="document">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											        <h4 class="modal-title" id="myModalLabel">Registration of - <strong> {{domainNameVal}} </strong> </h4>
											      </div>
											      <div class="modal-body">
													        <div class="content-wrapper"  ng-init="app.settings.pagetitle = 'Domain Registration';">
		   
								<div class="col-lg-12">
								        <section class="box ">
								           <!--  <header class="panel_header">
								                <h2 class="title pull-left">Form Wizard with validations</h2>
								                <div class="actions panel_actions pull-right">
								                    <i class="box_toggle material-icons">expand_more</i>
								                    <i class="box_setting material-icons" ng-click="section_modal()">mode_edit</i>
								                    <i class="box_close material-icons">close</i>
								                </div>
								            </header> -->
								            <div class="content-body">
								                <div class="row">
								                    <div class="col-md-12 col-sm-12 col-xs-12 form-wizard nav-pills">
								                        <div class="col-md-12 col-sm-12 col-xs-12">
								                            <uib-progressbar value="steps.percent" class="progress-xs primary" type="success"></uib-progressbar>
								                        </div>
								                        <uib-tabset class="tab-container" ng-init="steps={percent:20, step1:true, step2:false, step3:false, step4:false, step5:false, step1stat:false, step2stat:false, step3stat:false, step4stat:false, step5stat:false }">
								                            <uib-tab active="steps.step1" select="steps.percent=20" class=" {{steps.step1stat ? 'complete' : ''}} ">
								                                <uib-tab-heading class="tabhead">General</uib-tab-heading>
								                                <h4 class='margin-top-30'>Basic Information</h4>
								                                <form name="step1" class="form-validation col-md-8 padding-left-0">
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Full Name</label>
								                                        <div class="controls">
								                                            <input type="text" placeholder="Full Name" class="form-control" name="fullname" id="fullaname" ng-model="domainRegisterInfo.fullname" size="32" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Firstname</label>
								                                        <div class="controls">
								                                            <input type="text" name="firstname" placeholder="First Name" id="firstname" size="32" class="form-control" ng-model="domainRegisterInfo.firstname" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Lastname</label>
								                                        <div class="controls">
								                                            <input type="text" name="lastname" placeholder="First Name" id="lastname" size="32" class="form-control" ng-model="domainRegisterInfo.lastname" required>
								                                        </div>
								                                    </div>
								                                     <div class="form-group">
								                                        <label class="form-label" for="field-1">Company title (Optional)</label>
								                                        <div class="controls">
								                                            <input type="text" name="titlos" placeholder="Company title" id="titlos" size="32" class="form-control" ng-model="domainRegisterInfo.titlos" required>
								                                        </div>
								                                    </div>
								                                     <div class="form-group">
								                                        <label class="form-label" for="field-1">Email</label>
								                                        <div class="controls">
								                                            <input type="email" name="emailText" placeholder="First Name" id="emailText" size="32" class="form-control" ng-model="domainRegisterInfo.emailText" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Address</label>
								                                        <div class="controls">
								                                            <input type="text" name="address1" placeholder="First Name" id="address1" size="32" class="form-control" ng-model="domainRegisterInfo.address1" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">State</label>
								                                        <div class="controls">
								                                            <input type="text" name="stateProvince" placeholder="First Name" id="stateProvince" size="32" class="form-control" ng-model="domainRegisterInfo.stateProvince" required>
								                                        </div>
								                                    </div>
								                                     <div class="form-group">
								                                        <label class="form-label" for="field-1">City</label>
								                                        <div class="controls">
								                                            <input type="text" name="city" placeholder="First Name" id="city" size="32" class="form-control" ng-model="domainRegisterInfo.city" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Postcode</label>
								                                        <div class="controls">
								                                            <input type="text" name="postcode" placeholder="First Name" id="postcode" size="32" class="form-control" ng-model="domainRegisterInfo.postcode" required>
								                                        </div>
								                                    </div>
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Country</label>
								                                        <div class="controls">
								                                        	<select ng-model="domainRegisterInfo.country" class="form-control" name="country" id="country" size="1">
															           <option value="SH">Αγ. Ελένη</option>
																	<option value="PM">Άγ. Πέτρος (Σεν Πιέρ) και Μικελόν</option>
																	<option value="LC">Αγία Λουκία</option>
																	<option value="VC">Άγιος Βικέντιος και Γρεναδίνες</option>
																	<option value="ST">Άγιος Θωμάς και Πρίγκιπας (Σάο Τομέ και Πρίντσιπε)</option>
																	<option value="SM">Άγιος Μαρίνος</option>
																	<option value="KN">Άγιος Χριστόφορος (Σαιντ Κιτς) και Νέβις</option>
																	<option value="AO">Αγκόλα</option>
																	<option value="AZ">Αζερμπαϊτζάν</option>
																	<option value="EG">Αίγυπτος</option>
																	<option value="ET">Αιθιοπία</option>
																	<option value="HT">Αϊτή</option>
																	<option value="CI">Ακτή Ελεφαντοστού</option>
																	<option value="AL">Αλβανία</option>
																	<option value="DZ">Αλγερία</option>
																	<option value="AS">Αμερικανικές Σαμόα</option>
																	<option value="TP">Ανατολικό Τιμόρ</option>
																	<option value="AI">Ανγκουίλα</option>
																	<option value="AD">Ανδόρα</option>
																	<option value="AQ">Ανταρκτική</option>
																	<option value="AG">Αντίγκουα και Μπαρμπούντα</option>
																	<option value="AR">Αργεντινή</option>
																	<option value="AM">Αρμενία</option>
																	<option value="AW">Αρούμπα</option>
																	<option value="AU">Αυστραλία</option>
																	<option value="AT">Αυστρία</option>
																	<option value="AF">Αφγανιστάν</option>
																	<option value="VU">Βανουάτου</option>
																	<option value="VA">Βατικανό</option>
																	<option value="BE">Βέλγιο</option>
																	<option value="VE">Βενεζουέλα</option>
																	<option value="BM">Βερμούδες</option>
																	<option value="VN">Βιετνάμ</option>
																	<option value="BO">Βολιβία</option>
																	<option value="BA">Βοσνία Ερζεγοβίνη</option>
																	<option value="BG">Βουλγαρία</option>
																	<option value="BR">Βραζιλία</option>
																	<option value="IO">Βρετανικά Εδάφη Ινδικού Ωκεανού</option>
																	<option value="FR">Γαλλία</option>
																	<option value="GF">Γαλλική Γουιάνα</option>
																	<option value="PF">Γαλλική Πολυνησία</option>
																	<option value="DE">Γερμανία</option>
																	<option value="GE">Γεωργία</option>
																	<option value="GI">Γιβραλτάρ</option>
																	<option value="YU">Γιουγκοσλαβία</option>
																	<option value="GM">Γκάμπια</option>
																	<option value="GA">Γκαμπόν</option>
																	<option value="GH">Γκάνα</option>
																	<option value="GU">Γκουάμ</option>
																	<option value="GP">Γουαδελούπη</option>
																	<option value="GT">Γουατεμάλα</option>
																	<option value="GY">Γουιάνα</option>
																	<option value="GN">Γουινέα</option>
																	<option value="GW">Γουινέα-Μπισάου</option>
																	<option value="GD">Γρενάδα</option>
																	<option value="GL">Γροιλανδία</option>
																	<option value="DK">Δανία</option>
																	<option value="CZ">Δημοκρατία της Τσεχίας</option>
																	<option value="CD">Δημοκρατία του Κονγκό (Ζαΐρ)</option>
																	<option value="DO">Δομινικανή Δημοκρατία</option>
																	<option value="EH">Δυτική Σαχάρα</option>
																	<option value="SV">Ελ Σαλβαδόρ</option>
																	<option value="CH">Ελβετία</option>
																	<option selected="selected" value="GR">Ελλάδα</option>
																	<option value="ER">Ερυθραία</option>
																	<option value="EE">Εσθονία</option>
																	<option value="ZM">Ζάμπια</option>
																	<option value="ZW">Ζιμπάμπουε</option>
																	<option value="AE">Ηνωμένα Αραβικά Εμιράτα</option>
																	<option value="US">Ηνωμένες Πολιτείες</option>
																	<option value="GB">Ηνωμένο Βασίλειο</option>
																	<option value="JP">Ιαπωνία</option>
																	<option value="IN">Ινδία</option>
																	<option value="ID">Ινδονησία</option>
																	<option value="JO">Ιορδανία</option>
																	<option value="IQ">Ιράκ</option>
																	<option value="IR">Ιράν</option>
																	<option value="IE">Ιρλανδία</option>
																	<option value="GQ">Ισημερινή Γουινέα</option>
																	<option value="EC">Ισημερινός</option>
																	<option value="IS">Ισλανδία</option>
																	<option value="ES">Ισπανία</option>
																	<option value="IL">Ισραήλ</option>
																	<option value="IT">Ιταλία</option>
																	<option value="KZ">Καζαχστάν</option>
																	<option value="CM">Καμερούν</option>
																	<option value="KH">Καμπότζη</option>
																	<option value="CA">Καναδάς</option>
																	<option value="QA">Κατάρ</option>
																	<option value="CF">Κεντροαφρικανική Δημοκρατία</option>
																	<option value="KE">Κένυα</option>
																	<option value="CN">Κίνα</option>
																	<option value="KG">Κιργιστάν</option>
																	<option value="KI">Κιριμπάτι</option>
																	<option value="CO">Κολομβία</option>
																	<option value="KM">Κομόρες</option>
																	<option value="CG">Κονγκό</option>
																	<option value="KR">Κορέα</option>
																	<option value="CR">Κόστα Ρίκα</option>
																	<option value="CU">Κούβα</option>
																	<option value="KW">Κουβέιτ</option>
																	<option value="HR">Κροατία</option>
																	<option value="CY">Κύπρος</option>
																	<option value="KP">Λαϊκή Δημοκρατία της Κορέας</option>
																	<option value="LA">Λάος</option>
																	<option value="LS">Λεσότο</option>
																	<option value="LV">Λετονία</option>
																	<option value="BY">Λευκορωσία</option>
																	<option value="LB">Λίβανος</option>
																	<option value="LR">Λιβερία</option>
																	<option value="LY">Λιβύη</option>
																	<option value="LT">Λιθουανία</option>
																	<option value="LI">Λίχτενσταϊν</option>
																	<option value="LU">Λουξεμβούργο</option>
																	<option value="YT">Μαγιότ</option>
																	<option value="MG">Μαδαγασκάρη</option>
																	<option value="MO">Μακάο</option>
																	<option value="MY">Μαλαισία</option>
																	<option value="MW">Μαλάουι</option>
																	<option value="MV">Μαλδίβες</option>
																	<option value="ML">Μάλι</option>
																	<option value="MT">Μάλτα</option>
																	<option value="MA">Μαρόκο</option>
																	<option value="MQ">Μαρτινίκα</option>
																	<option value="MU">Μαυρίκιος</option>
																	<option value="MR">Μαυριτανία</option>
																	<option value="MX">Μεξικό</option>
																	<option value="MM">Μιανμάρ</option>
																	<option value="UM">Μικρά απομονωμένα νησιά Ηνωμένων Πολιτειών</option>
																	<option value="FM">Μικρονησία</option>
																	<option value="MN">Μογγολία</option>
																	<option value="MZ">Μοζαμβίκη</option>
																	<option value="MD">Μολδαβία</option>
																	<option value="MC">Μονακό</option>
																	<option value="MS">Μονσεράτ</option>
																	<option value="BD">Μπανγκλαντές</option>
																	<option value="BB">Μπαρμπάντος</option>
																	<option value="BS">Μπαχάμες</option>
																	<option value="BH">Μπαχρέιν</option>
																	<option value="BZ">Μπελίσε</option>
																	<option value="BJ">Μπενίν</option>
																	<option value="BW">Μποτσουάνα</option>
																	<option value="BF">Μπουρκίνα Φάσο</option>
																	<option value="BI">Μπουρούντι</option>
																	<option value="BT">Μπουτάν</option>
																	<option value="BN">Μπρουνέι</option>
																	<option value="NA">Ναμίμπια</option>
																	<option value="NR">Ναούρου</option>
																	<option value="NZ">Νέα Ζηλανδία</option>
																	<option value="NC">Νέα Καληδονία</option>
																	<option value="NP">Νεπάλ</option>
																	<option value="BV">Νησί Μπουβέ</option>
																	<option value="NF">Νησί Νόρφολκ</option>
																	<option value="CX">Νησί Χριστουγέννων</option>
																	<option value="MP">Νησιά Βόρειες Μαριάννες</option>
																	<option value="KY">Νησιά Καϊμάν</option>
																	<option value="CC">Νησιά Κόκος (Κίλινγκ)</option>
																	<option value="CK">Νησιά Κουκ</option>
																	<option value="MH">Νησιά Μάρσαλ</option>
																	<option value="WF">Νησιά Ουόλις και Φουτούνα</option>
																	<option value="SJ">Νησιά Σβάλμπαρντ και Γιαν Μάγεν</option>
																	<option value="SB">Νησιά Σολομώντα</option>
																	<option value="TC">Νησιά Ταρκ και Κάικος</option>
																	<option value="FO">Νησιά Φερόες</option>
																	<option value="FK">Νησιά Φόκλαντ (Μαλβίνας)</option>
																	<option value="HM">Νησιά Χερντ και Μακντόναλντ</option>
																	<option value="NE">Νίγηρας</option>
																	<option value="NG">Νιγηρία</option>
																	<option value="NI">Νικαράγουα</option>
																	<option value="NU">Νιούε</option>
																	<option value="NO">Νορβηγία</option>
																	<option value="ZA">Νότια Αφρική</option>
																	<option value="TF">Νότια Γαλλικά Εδάφη</option>
																	<option value="GS">Νότια Γεωργία και Νότια Νησιά Σάντουιτς</option>
																	<option value="DM">Ντομίνικα</option>
																	<option value="NL">Ολλανδία</option>
																	<option value="AN">Ολλανδικές Αντίλλες</option>
																	<option value="OM">Ομάν</option>
																	<option value="HN">Ονδούρα</option>
																	<option value="HU">Ουγγαρία</option>
																	<option value="UG">Ουγκάντα</option>
																	<option value="UZ">Ουζμπεκιστάν</option>
																	<option value="UA">Ουκρανία</option>
																	<option value="UY">Ουρουγουάη</option>
																	<option value="PK">Πακιστάν</option>
																	<option value="PW">Παλάου</option>
																	<option value="PA">Παναμάς</option>
																	<option value="PG">Παπούα Νέα Γουινέα</option>
																	<option value="PY">Παραγουάη</option>
																	<option value="VG">Παρθένα νησιά (Βρετανικά)</option>
																	<option value="VI">Παρθένα νησιά (ΗΠΑ)</option>
																	<option value="PE">Περού</option>
																	<option value="PN">Πίτκερν</option>
																	<option value="PL">Πολωνία</option>
																	<option value="PT">Πορτογαλία</option>
																	<option value="PR">Πουέρτο Ρίκο</option>
																	<option value="CV">Πράσινο Ακρωτήριο</option>
																	<option value="MK">Πρώην Γιουκοσλαβική Δημοκρατία της Μακεδονίας (F.Y.R.O.M.)</option>
																	<option value="RE">Ρεϊνιόν</option>
																	<option value="RW">Ρουάντα</option>
																	<option value="RO">Ρουμανία</option>
																	<option value="RU">Ρωσία</option>
																	<option value="WS">Σαμόα</option>
																	<option value="SA">Σαουδική Αραβία</option>
																	<option value="SN">Σενεγάλη</option>
																	<option value="SC">Σεϋχέλλες</option>
																	<option value="SG">Σιγκαπούρη</option>
																	<option value="SL">Σιέρα Λεόνε</option>
																	<option value="SK">Σλοβακία</option>
																	<option value="SL">Σλοβενία</option>
																	<option value="SI">Σλοβενία</option>
																	<option value="SO">Σομαλία</option>
																	<option value="SZ">Σουαζιλάνδη</option>
																	<option value="SD">Σουδάν</option>
																	<option value="SE">Σουηδία</option>
																	<option value="SV">Σουηδία</option>
																	<option value="SR">Σουρινάμ</option>
																	<option value="LK">Σρι Λάνκα</option>
																	<option value="SY">Συρία</option>
																	<option value="TW">Ταϊβάν</option>
																	<option value="TH">Ταϊλάνδη</option>
																	<option value="TZ">Τανζανία</option>
																	<option value="TJ">Τατζικιστάν</option>
																	<option value="JM">Τζαμάικα</option>
																	<option value="DJ">Τζιμπουτί</option>
																	<option value="TG">Τόγκο</option>
																	<option value="TK">Τοκελάου</option>
																	<option value="TO">Τόνγκα</option>
																	<option value="TV">Τουβάλου</option>
																	<option value="TR">Τουρκία</option>
																	<option value="TM">Τουρκμενιστάν</option>
																	<option value="TT">Τρινιντάντ και Τομπάγκο</option>
																	<option value="TD">Τσαντ</option>
																	<option value="TN">Τυνησία</option>
																	<option value="YE">Υεμένη</option>
																	<option value="PH">Φιλιππίνες</option>
																	<option value="FI">Φινλανδία</option>
																	<option value="FJ">Φίτζι</option>
																	<option value="CL">Χιλή</option>
																	<option value="HK">Χονγκ Κονγκ</option>
															         </select>
								                                   </div>
								                                 </div>
								                                 <div class="form-group">
								                                        <label class="form-label" for="field-1">Phone number</label>
								                                        <div class="controls">
								                                            <input type="text" name="phoneNum" placeholder="First Name" id="phoneNum" size="32" class="form-control" ng-model="domainRegisterInfo.phoneNum" required>
								                                        </div>
								                                    </div>
								                                   <div class="form-group">
								                                        <label class="form-label" for="field-1">Domain Password</label>
								                                        <div class="controls">
								                                            <input type="password" name="litepsd" placeholder="First Name" id="litepsd" size="32" class="form-control" ng-model="domainRegisterInfo.litepsd" required>
								                                        </div>
								                                    </div> 
								                                     
						                                    <div class="">
						                                        <button type="submit" ng-disabled="step1.$invalid" class="btn btn-primary" ng-click="steps.step2=true">Next</button>
						                                    </div>
								                                </form>
								                            </uib-tab>
								                            <uib-tab ng-disabled="step1.$invalid" active="steps.step2" select="steps.percent=40; steps.step1stat=true" class=" {{steps.step2stat ? 'complete' : ''}}">
								                                <uib-tab-heading class="tabhead">Name-servers</uib-tab-heading>
								                                <!-- <h4 class='margin-top-30'>Profile Information</h4> -->
								                                <form name="step2" class="form-validation  col-md-8 padding-left-0">
								                                	<div class="form-group">
								                                        <label class="form-label" for="field-1">Name Servers</label>
								                                        <div class="controls">
								                                            <input name="nameserversbutton" type="radio" id="et1" onClick="return TrHide(true);" value="1">
						  													I want to set nameservers too
						  													<input name="nameserversbutton" type="radio" id="et1" onClick="return TrHide(false);" value="1" checked="checked">
						  													I dont want to set nameservers
						  													<div style="display:none" id="ns"> Fill the nameservers: (Optional)<br>
						  													<div class="row">
						  														<!-- NS1 -->
						  														<div class="col-md-6 col-lg-6">
						  														Name Server <input class="form-control" name="ns1" type="text" />
						  														</div>
						  														<div class="col-md-6 col-lg-6">
						  															Ip <input class="form-control" name="ip1" type="text"  />
						  														</div>
						  														<!-- End NS1 -->
						  														<!-- NS2 -->
						  														<div class="col-md-6 col-lg-6">
																				Name Server <input name="ns2" class="form-control" type="text" />  

						  														</div>
						  														<div class="col-md-6 col-lg-6">
						  														Ip <input class="form-control" name="ip2" type="text" />
						  														</div>
						  														<!-- End-NS2 -->
						  														<!-- NS3 -->
						  														<div class="col-md-6 col-lg-6">
																				Name Server <input name="ns3" type="text" class="form-control" /> 

						  														</div>
						  														<div class="col-md-6 col-lg-6">
						  														Ip <input name="ip3" type="text" class="form-control" />
						  														</div>
						  														<!-- End NS3 -->
						  														<!-- NS3 -->
						  														<div class="col-md-6 col-lg-6">
																				Name Server <input name="ns4" type="text" class="form-control" /> 

						  														</div>
						  														<div class="col-md-6 col-lg-6">
						  														Ip <input class="form-control" name="ip4" type="text" />
						  														</div>
						  														<!-- End NS3 -->
						  													</div>
																		</div>
								                                        </div>
								                                    </div>
								                                   
								                                    <div class="">
								                                        <button type="button" class="btn btn-primary" ng-click="steps.step1=true">Prev</button>
								                                        <button type="submit" ng-disabled="step2.$invalid" class="btn btn-primary" ng-click="steps.step3=true">Next</button>
								                                    </div>
								                                </form>
								                            </uib-tab>
								                            <uib-tab disabled="step2.$invalid" active="steps.step3" select="steps.percent=60; steps.step2stat=true" class=" {{steps.step3stat ? 'complete' : ''}}">
								                                <uib-tab-heading class="tabhead">Payment</uib-tab-heading>
								                                <!-- <h4 class='margin-top-30'>Portfolio Information</h4> -->
								                                <form name="step3" class="form-validation  col-md-8 padding-left-0">
								                                	 <div class="form-group">
								                                        <form  action="payment.php" method="post" name="totalform">
													                           <select name="menu2" class="form-control" onchange="mysubmit()">
													                            <option value="">[Select Payment way]</option>
																				<option value="1">Credit card</option> 
													                            <option value="2">Bank Deposit</option>
													                          </select>
																			  <input type="hidden" name="domainNames" value="" />
																			  <input type="hidden" name="description" value="" />
																			  <input type="hidden" name="newurl" value="" />
																			  <input type="hidden" name="customer_id" value="" />
																			  <input type="hidden" name="registration_id" value=""  />
																			  </form>
								                                    </div>
								                                    
								                                    <div class="">
								                                        <button type="button" class="btn btn-primary" ng-click="steps.step2=true">Prev</button>
								                                        <button type="submit" ng-disabled="step3.$invalid" class="btn btn-primary" ng-click="steps.step4=true">Next</button>
								                                    </div>
								                                </form>
								                            </uib-tab>
								                            <uib-tab disabled="step3.$invalid" active="steps.step4" select="steps.percent=80; steps.step3stat=true" class=" {{steps.step4stat ? 'complete' : ''}}">
								                                <uib-tab-heading class="tabhead">Overview</uib-tab-heading>
								                                <!-- <h4 class='margin-top-30'>User Information</h4> -->
								                                <form name="step4" class="form-validation  col-md-8 padding-left-0">
								                                    <div class="form-group">
								                                        <label class="form-label" for="field-1">Email</label>
								                                        <div class="controls">
								                                            <input type="email" name="email" class="form-control" ng-model="email" required>
								                                        </div>
								                                    </div>
								                                    <div class="">
								                                        <button type="button" class="btn btn-primary" ng-click="steps.step3=true">Prev</button>
								                                        <button type="submit" ng-disabled="step4.$invalid" class="btn btn-primary" ng-click="steps.step5=true">Next</button>
								                                    </div>
								                                </form>
								                            </uib-tab>
								                            <uib-tab disabled="step4.$invalid" active="steps.step5" select="steps.percent=100; steps.step4stat=true" class=" {{steps.step5stat ? 'complete' : ''}}">
								                                <uib-tab-heading class="tabhead">T&C</uib-tab-heading>
								                                <!-- <h4 class='margin-top-30'>User Settings</h4> -->
								                                <form name="step5" class="form-validation  col-md-8 padding-left-0">
								                                    <div class="form-group">
								                                        <label class="form-label" for="formfield1">Terms</label>
								                                        <div class="controls">
								                                            <label class="form-label checkbox icheck">
								                                                <input type="checkbox" ng-model="txtagree" required><i></i> I agree to terms & conditions
								                                            </label>
								                                        </div>
								                                    </div>
								                                    <div class="">
								                                        <button type="button" class="btn btn-primary" ng-click="steps.step4=true">Prev</button>
								                                        <button type="submit" ng-disabled="step5.$invalid" class="btn btn-primary" ng-click="steps.percent=100;steps.step5=true; steps.step5stat=true;submiteUserdata()">Finish</button>
								                                    </div>
								                                </form>
								                            </uib-tab>
								                        </uib-tabset>
								                    </div>
								                </div>
								            </div>
								        </section>
								    </div>
								</div>

											      </div>
											     <!--  <div class="modal-footer">
											        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											        <button type="button" class="btn btn-primary">Save changes</button>
											      </div> -->
											    </div>
											  </div>
											</div>
									</div>
								</div>
								<div class="not_avli_domain" ng-show="domainList">
									<h4>Not Available Domain - {{jsonData.arrayNotAvDomains.length}}</h4>
									<div class="col-lg-2 not_available_msg_wrp" ng-repeat='noAvalDomain in jsonData.arrayNotAvDomains'>
										<!-- <input type="checkbox" checklist-model="" checklist-value="noAvalDomain"> -->
										<i class="fa fa-frown-o"></i> <span class="not_available_msg"> &nbsp;{{noAvalDomain}}</span>
									</div>
								</div>
								
								<!-- {{jsonData.arrayAvDomains}}<br>
								{{jsonData.arrayNotAvDomains}} -->
		            		</div>
		            		<!-- <div class="col-lg-2">

		            		</div> -->
		            	</div>
		               
		            <!-- </div> -->
		            <div role="alert">
		                
		            </div>
		        <!-- </form> -->
		    </div>
        </div>
	</div>
