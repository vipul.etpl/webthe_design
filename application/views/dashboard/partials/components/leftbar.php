<!-- SIDEBAR - START -->
<!-- MAIN MENU - START -->
<perfect-scrollbar class="page-sidebar-wrapper" id="main-menu-wrapper" wheel-propagation="true" suppress-scroll-x="true" min-scrollbar-length="20" menuheight>
    <!-- USER INFO - START -->
    <div class="profile-info" ng-show="app.settings.menuProfile">
        <div class="profile-image col-md-12 col-sm-12 col-xs-12 text-center">
            <a ui-sref="app.ui.profile">
                <img src="<?php echo base_url().''.$imgs;?>" class="img-responsive">
                <span class="profile-status available"></span>
            </a>
        </div>
        <div class="profile-details col-md-12 col-sm-12 col-xs-12">
            <h3>
                    <a ui-sref="app.ui.profile"><?php echo ucfirst($this->session->userdata('username'));?></a>

                    <!-- Available statuses: online, idle, busy, away and offline -->
                </h3>
            <!-- <p class="profile-title">Online</p> -->
        </div>
    </div>
    <!-- USER INFO - END -->
    <!-- nav -->

    <nav ui-nav ng-include="'<?php echo base_url()?>index.php/dashboard/nav'"></nav>
    <ul class="wraplist wrapper-menu">
        <li>
            <a target="_blank" href = "<?php echo base_url()?>index.php/ant_tool_page" class="auto">
            <i class="material-icons">work</i>
            <span class="title">Start Designing</span>
            <!-- <span class="arrow material-icons">keyboard_arrow_right</span>  <span class="label label-accent">NEW</span> -->
        </a>
        </li>

        <li ui-sref-active="active">
            <a ui-sref="app.manage" class="nosub">
            <i class="material-icons">view_quilt</i>
            <span class="title">Create a Website</span>
            <!-- <span class="arrow material-icons">keyboard_arrow_right</span>  <span class="label label-accent">NEW</span> -->
        </a>
        </li>
    </ul>
    <!-- nav -->
    <!-- <div class="sidebar-graphs col-md-12">
        <h5>
              Milestone
              <span class="pull-right">50%</span>
            </h5>
        <uib-progressbar value="50" class="progress progress-sm accent"></uib-progressbar>
        <h5>
              Time Consumed
              <span class="pull-right">65%</span>
            </h5>
        <uib-progressbar value="65" class="progress progress-sm warning"></uib-progressbar>
    </div> -->
</perfect-scrollbar>
<!-- MAIN MENU - END -->
<!--  SIDEBAR - END -->
