// app.js
var routerApp = angular.module('routerApp', ['ui.router']);
var base_url = "http://"+window.location.hostname+"/";

routerApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: base_url+"index.php/home/loadInitialHomeView"
        })
        
        .state('login', {
           
             url: '/login',
            templateUrl: base_url+"index.php/home/signinPage"     
        }).state('signup', {
            
             url: '/signup',
            templateUrl: base_url+"index.php/home/signupPage"     
        }).state('password', {
           
             url: '/password',
            templateUrl: base_url+"index.php/home/password"     
        }).state('forgotpassword', {
           
             url: '/forgotpassword',
            templateUrl: base_url+"index.php/forgotpassword/forgot_pass"     
        });
        
});

routerApp.controller('LoginFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.authError = null;
    $scope.login = function() {
      $scope.authError = null;
      // Try to login
       $http({
          method  : 'POST',
          url     : base_url+'index.php/login/loginme',
          data    : $scope.user, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            if (data['noUserFound']) {
              $scope.authError =  'Invalid Email/Username entered please try again.';
            }else{
               window.location = "http://"+data+"."+window.location.hostname+"/index.php/home/#/password";
               console.log(data);
            }
            
          });
    };
    // Aishvarya - Chkpwd For each user
    $scope.chkUserPwd = function() {
      $scope.authError = null;
      // Try to login
       $http({
          method  : 'POST',
          url     : base_url+'index.php/login/chkUserPwd',
          data    : $scope.user, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            if (data['noUserFound']) {
              $scope.authError =  'Invalid Password entered please try again.';
            }else{
               window.location = "http://"+window.location.host+"/index.php/dashboard";
               //console.log(data);
            }
            
          });
    };
  }])
;

routerApp.controller('RegisterFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.register = function() {
      $scope.authError = null;
      // Try to create
      if($("#username").val().length < 4){
        alert("username have at least 4 characters");
      }
      else
      {

      $http({
          method  : 'POST',
          url     : base_url+'index.php/signup/register_user',
          data    : $scope.user, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         }).success(function(data) {
            if (data['error'] == 101) {
              $scope.authError = 'Email already exist.';
            }else if (data['error'] == 102) {
              $scope.authError = 'Username already exist.';

            }else if (data['error'] == 103) {
              $scope.authError = 'Name must contain SPACE between full name';

            }else if (data['error'] == 103) {
              $scope.authError = 'Unknow error.';
            }else if (data['error'] == 110) {
              $scope.authError = 'One or more of the special characters found';
            }
            else if (data['error'] == 111) {
              $scope.authError = 'Invalid email';
            }
            else{
              //alert('dashboard');
              console.log(data);
               window.location = "http://"+data+"."+window.location.hostname+"/index.php/dashboard";
            }
          });
       }
        };
     }]);

// Aish.. Create function to change location ae per current location...
// 06-02-2015
function homeUrl(url){
  window.location = "http://"+url;
}

function dashboardUrl(username){
  window.location = "http://"+username+"."+window.location.hostname+"/index.php/dashboard";
}

routerApp.controller('ForgotpwdFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.forgotpwdlink = function() {
      $scope.lodingspin = true;
      $scope.authError = null;
      // Try to create
      $http({
          method  : 'POST',
          url     : base_url+'index.php/forgotpassword/reset_password_email',
          data    : $scope.user, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         }).success(function(data) {
            //console.log(data['error']);
            $scope.lodingspin = false;

            if (data['error'] == 106) {
              $scope.authError = 'There is no Happy Edit account associated with this email address.';
            }
            else{
               //window.location = "http://"+data+"."+window.location.hostname+"/index.php/dashboard";
               $scope.authError = 'A reset link sent to your email address.';
            }
          });
        };
     }]);
