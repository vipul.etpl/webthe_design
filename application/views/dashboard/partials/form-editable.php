<div class='content-wrapper' ng-init="app.settings.pagetitle = 'Edit Profile';">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ng-controller="FormXeditableCtrl" ng-init="getUserId('<?php echo $this->session->userdata('user_id');?>')">
        <section class="box "> 
            <header class="panel_header">
                <h2 class="title pull-left">User Information</h2> 
                <div class="actions panel_actions pull-right">
                    <i class="box_toggle material-icons">expand_more</i>
                    <!-- <i class="box_setting material-icons" ng-click="section_modal()">mode_edit</i> -->
                    <i class="box_close material-icons">close</i>
                </div>
            </header>
            <div class="content-body padding-bottom-0">
                <div class="row">
                   <!-- <span> -->
                    <div ng-if="msg != null " class="alert alert-warning alert-dismissible" role="alert" ng-show="editsMsg">
                      <button type="button" class="close" ng-click="closeAlertMsg()"><span aria-hidden="true">&times;</span></button>
                      <strong>{{msg}}</strong></div>
                   <!-- </span> -->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <div class="controls"> <a editable-email="userData.email" onbeforesave="updateUserEmail('<?php echo $this->session->userdata('user_id');?>')">{{ userData.email || 'empty' }}</a>

                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">First Name</label>
                            <div class="controls"> <a editable-text="userData.first_name" onbeforesave="updateUserFirstname('<?php echo $this->session->userdata('user_id');?>')"  e-title="First Name" e-min="18">{{ userData.first_name || 'empty' }}</a></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Last Name</label>
                            <div class="controls"> <a editable-text="userData.last_name" onbeforesave="updateUserLastname('<?php echo $this->session->userdata('user_id');?>')" e-min="18">{{ userData.last_name || 'empty' }}</a></div>
                        </div>
                       <!--  <div class="form-group">
                            <label class="form-label">Username</label>
                            <div class="controls"> <a editable-text="userData.username" e-step="10">{{ userData.username || 'empty' }}</a></div>
                        </div> -->
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
    
</div>
