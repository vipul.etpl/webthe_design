<!-- START TOPBAR -->
<div class='logo-area'>
    <a href="http://happyedit.com"> <img src="<?php echo base_url()?>img/logo.png" style="padding: 0px; height: 40px;"></a>
</div>
<div class='quick-area'>
    <div class='pull-left slantit'>
        <ul class="info-menu left-links list-inline list-unstyled">
            <li class="sidebar-toggle-wrap">
                <a href data-toggle="sidebar" class="sidebar_toggle" ng-click="app.settings.menuFolded = !app.settings.menuFolded; menuChatToggle('menu',app.settings.menuFolded) ">
                    <i class="material-icons">menu</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="pull-left pagetitle">
        <span class='line'></span>
        <h1>{{app.settings.pagetitle}}</h1>
        <div></div>
    </div>
    <div class="pull-right quickleft" style=''>
        <ul class="info-menu right-links list-inline list-unstyled">
            <li>
                <a href="<?php echo base_url()?>index.php/dashboard" ng-click="refreshDashboard()" class="btn btn-primary">Dashboard</a> 
            </li>
        </ul>
    </div>
    
    
</div>
<!-- END TOPBAR -->
