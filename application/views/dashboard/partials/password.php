<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<div class="login_page " style="margin-top:-50%">
    <div class="login-wrapper" ng-controller="LoginFormController">
        <div id="login" class="login loginpage col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
            <h1><a href="#" title="Login Page" tabindex="-1">Happy Edit - Login Page</a></h1>
                <form name="form" class="form-validation">
                   <div class="user_login_error error">   {{authError}} </div>
                      <div class="text-danger text-center message" ng-show="authError">
                      </div>
                        <p>
                            <input type="password" name="pwd" id="user_login" class="input" ng-model="user.password" placeholder="Password" required/></label>
                        </p>
                        
                        <p class="submit">
                            <button type="submit" ng-click="chkUserPwd()" ng-disabled='form.$invalid' class="btn btn-accent btn-block">Sign In</button>
                        </p>
                </form>
           <!--  <p id="nav">
                <a class="pull-left" ui-sref="forgotpassword" title="Password Lost and Found">Forgot password?</a>
               
				<a href="<?php echo base_url(); ?>index.php/signup/twitter" class="social-log-reg">
				  <img class="img-circle " title="Tiwtter" width="25" height="25" src="<?= base_url('img/t.png'); ?>">
			    </a>
				<a class="pull-right" ui-sref="signup" href="#/signup" title="Sign Up">Sign Up</a>
            </p> -->
        </div>
    </div>
</div>    
<script>
$(document).ready(function()
{
$("#user_login").focus();
}); 
</script>



