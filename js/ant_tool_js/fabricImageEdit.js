var	canvas = new fabric.Canvas("myCanvas");
	canvas.renderAll();
	//start code for image edit
  var imageArrse=[];
function placeImage()
{
	$("#imgLoader").click();
//var cropFlag=false
document.getElementById('imgLoader').onchange = function(e)
{
   canvas.clear().renderAll();
var reader = new FileReader();
  reader.onload = function (event){
    var imgObj = new Image();
    imgObj.src = event.target.result; 
    window.imageArrse[0]=event.target.result;
    imgObj.onload = function () {
     var image = new fabric.Image(imgObj);
      image.set({
            angle: 0,
            height:390,
            width:605
      });
      image.transformMatrix = [1, 0, 0, 1, 0, 0];
      canvas.centerObject(image);
      //image.scaleX = image.scaleY = 0.0;
      canvas.add(image);
      canvas.renderAll();
    }
  }
  reader.readAsDataURL(e.target.files[0]);
 // console.log(e.target.files[0]);
 //window.imageArrse[0]=e.target.files[0];
 $(".toggleFunctions").show();
 $("#imgLoader").val("");
 $("#imgLoader").attr("disabled",false);
}
 //canvas.setActiveObject(canvas.item(0));
 
}

function saveCanvasImage()
{
  $("#loadingPopups1").fadeIn();
  var usrIds=$("#userId").val();
  canvas.deactivateAll().renderAll();  
  $.ajax({
     url: '../script/saveDesign/saveFabric.php?usrId='+usrIds,
     type: 'POST',
     data: {
         data: canvas.toDataURL('image/png')
     },
     complete: function(data, status)
     {
      if(status== "success")
      {
        $("#waitTexts").text("Image Saved").css("color","green");
        setTimeout(function(){$("#loadingPopups1").fadeOut();},1000); 
       //console.log(data.responseText);
       $(".rightPart").append("<img src='../canvasPng/"+data.responseText+"' class='savedImgSize' >");
      }     
     }

 });
}

$('#crop').on('click', function (event) {

    var eLeft = el.get('left');
    var eTop = el.get('top');
    var left = eLeft - object.left;
    var top = eTop - object.top;

    left *= 1;
    top *= 1;

    var eWidth = el.get('width');
    var eHeight = el.get('height');
    var eScaleX = el.get('scaleX');
    var eScaleY = el.get('scaleY');
    var width = eWidth * 1;
    var height = eHeight * 1;
    object.clipTo = function (ctx) {

        ctx.rect(-(eWidth / 2) + left, -(eHeight / 2) + top, parseInt(width * eScaleX), parseInt(eScaleY * height));


    }



    for (var i = 0; i < $("#layers li").size(); i++) {
        canvas.item(i).selectable = true;
    }
    disabled = true;

    canvas.remove(el);
    lastActive = object;
    canvas.renderAll();



});

var el;
//rectangle for ruleof thirds
var rect1 = new fabric.Rect({ width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:0});
var rect2 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:201});
var rect3 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:402});

var rect4 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:131, left:0});
var rect5 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:131, left:201});
var rect6 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:131, left:402});

var rect7 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:262, left:0});
var rect8 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:262, left:201});
var rect9 = new fabric.Rect({  width: 200, height: 130, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:262, left:402});
//end of rects of rule of thirds
//rects for golden sections
  var rect11 = new fabric.Rect({ width: 250, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:0});
var rect21 = new fabric.Rect({  width: 100, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:251});
var rect31 = new fabric.Rect({  width: 250, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:352});

var rect41 = new fabric.Rect({  width: 250, height: 105, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:141, left:0});
var rect51 = new fabric.Rect({  width: 100, height: 105, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:141, left:251});
var rect61 = new fabric.Rect({  width: 250, height: 105, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:141, left:352});

var rect71 = new fabric.Rect({  width: 250, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:246, left:0});
var rect81 = new fabric.Rect({  width: 100, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:246, left:251});
var rect91 = new fabric.Rect({  width: 250, height: 140, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:246, left:352});
//end of rects for golden sections
//rects for center lines
 var rect12 = new fabric.Rect({ width: 300, height: 195, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:0});
var rect22= new fabric.Rect({  width: 300, height: 195, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:0, left:301});

var rect32 = new fabric.Rect({  width: 300, height: 195, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:196, left:0});
var rect42 = new fabric.Rect({  width: 300, height: 195, fill: 'rgba(0,0,0,0.3)', stroke: '#ccc', top:196, left:301});

//end of rects for center lines

function myCrop(cropType) {
    canvas.remove(el);
    if (canvas.getActiveObject()) {

        object = canvas.getActiveObject();

        switch(cropType)
        {
          case "startCrop":el = new fabric.Rect({
                            fill: 'rgba(0,0,0,0.6)',
                            originX: 'left',
                            originY: 'top',
                            stroke: '#ccc',
                            strokeDashArray: [2, 2],
                            opacity: 1,
                            width: 1,
                            height: 1,
                            borderColor: '#36fd00',
                            cornerColor: 'green',
                            hasRotatingPoint: false
                             });
                            break;
          case "ruleOfThirds": el = new fabric.Group([ rect1, rect2, rect3, rect4, rect5, rect6, rect7, rect8, rect9], {  originX: 'left', originY: 'top',strokeDashArray: [2, 2], opacity: 1, width: 1, height: 1, hasRotatingPoint: false });
                              break;

          case "goldenSections": el = new fabric.Group([ rect11, rect21, rect31, rect41, rect51, rect61, rect71, rect81, rect91], {  originX: 'left', originY: 'top',strokeDashArray: [2, 2], opacity: 1, width: 1, height: 1, hasRotatingPoint: false });
                              break;
          case "centerLines": el = new fabric.Group([ rect12, rect22, rect32, rect42], {  originX: 'left', originY: 'top',strokeDashArray: [2, 2], opacity: 1, width: 1, height: 1, hasRotatingPoint: false });
                              break;
        }

        el.left = canvas.getActiveObject().left;
        selection_object_left = canvas.getActiveObject().left;
        selection_object_top = canvas.getActiveObject().top;
        el.top = canvas.getActiveObject().top;
        el.width = canvas.getActiveObject().width * canvas.getActiveObject().scaleX;
        el.height = canvas.getActiveObject().height * canvas.getActiveObject().scaleY;


        canvas.add(el);
        canvas.setActiveObject(el);
        for (var i = 0; i < $("#layers li").size(); i++) {
            canvas.item(i).selectable = false;
        }
    } else {
        alert("Please select a layer");
    }

}

//perspectiv code
$("#perspectiveMode").click(function()
{
  $("#filterDiv").hide();
  $("#filterMode").show();
  $("#perspectiveMode").hide();
  $("#perspectiveDiv").show();
});
$("#filterMode").click(function()
{
  $("#perspectiveDiv").hide();
  $("#perspectiveMode").show();
   $("#filterMode").hide();
  $("#filterDiv").show();  
});

 
//var activeImg=canvas.getActiveObject();
function update() {
    canvas.renderAll();
    var matrix = canvas.getActiveObject().transformMatrix;
    document.getElementById('a-val').innerHTML = matrix[0];
    document.getElementById('b-val').innerHTML = matrix[1];
    document.getElementById('c-val').innerHTML = matrix[2];
    document.getElementById('d-val').innerHTML = matrix[3];
    //document.getElementById('tx-val').innerHTML = matrix[4];
    //document.getElementById('ty-val').innerHTML = matrix[5];
  }


  document.getElementById('a').onchange = function() {
    if($("#a").val()==0)
    {}
  else
  {
    canvas.getActiveObject().transformMatrix[0] = parseFloat(this.value, 10);
    update();
  }
  };
  document.getElementById('b').onchange = function() {
   canvas.getActiveObject().transformMatrix[1] = parseFloat(this.value, 10);
    update();
  };
  document.getElementById('c').onchange = function() {
    canvas.getActiveObject().transformMatrix[2] = parseFloat(this.value, 10);
    update();
  };
  document.getElementById('d').onchange = function() {
    if($("#d").val()==0)
    {}
  else
  {
    canvas.getActiveObject().transformMatrix[3] = parseFloat(this.value, 10);
    update();
  }    
  };
  $("#deleteImages").click(function()
  {
      canvas.getActiveObject().remove();
      $("#imgLoader").attr("disabled",false);
  });
  /*document.getElementById('tx').onchange = function() {
    canvas.getActiveObject().transformMatrix[4] = parseFloat(this.value, 10);
    update();
  };
  document.getElementById('ty').onchange = function() {
    canvas.getActiveObject().transformMatrix[5] = parseFloat(this.value, 10);
    update();
  };*/
//end of perspective code