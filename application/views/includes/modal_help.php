    <div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="helpLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="helpLabel">Help</h3>
        </div> <!-- /modal-header -->
        <div class="modal-body">
            <h4>Create Account</h4>
            <p>Create a new account for accessing the user area. You can fill the registration form and click submit button to proceed. After have an account, you can use your <code>Username</code> and <code>Password</code> for login.</p><hr/>
            <h4>Login</h4>
            <p>This is login menu for accessing user area. <code>Username</code> and <code>Password</code> are required.</p><hr/>
            <h4>Logout</h4>
            <p>This is logout menu for exit the application (user area) and destroy the active session.</p><hr/>
            <h4>View Account Detail</h4>
            <p>This menu display the current user account information.</p><hr/>

            <hr/>
        </div> <!-- /modal-body -->
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div> <!-- /modal-footer -->
    </div> <!-- /modal -->