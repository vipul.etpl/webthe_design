<div class='content-wrapper' ng-controller="TrendingImgCtrl" ng-init="app.settings.pagetitle = 'Dashboard';getExtName(myViewsBy['id'])  ">
<div class="col-lg-12"> 
           <section class="box "> 
            <header class="panel_header">
                <h2 class="title pull-left">All Projects</h2>
                <div class="actions panel_actions pull-right">
                    <i class="box_toggle material-icons">expand_more</i>
                    <i class="box_setting material-icons" ng-click="section_modal()">mode_edit</i>
                    <i class="box_close material-icons">close</i>
                </div>
                <!-- <select class="btn btn-primary" ng-model="myViewsBy" ng-change="chnageViewLook(myViewsBy)" style="color:#fff;">
                    <option value="" selected>View By</option>
                    <option  value="htmls" >HTML</option>
                    <option value="pngs">PNG</option>
                    <option value="gifs">GIF</option>
                  </select>  -->
<!--                   <select class="btn btn-primary btn_dropdwn" ng-model="myViewsBy" ng-change="chnageViewLook(myViewsBy['id'])" ng-options="ext.name for ext in extnameVal track by ext.id"><span class="fa fa-caret"></span></select>
 -->            </header>
            <div class="content-body padding-bottom-0" >
                        
                <div class="row" ng-switch on="myViewsBy['id']" > 
                    <div class="col-md-12 col-sm-12 col-xs-12" ng-switch-when="htmls">
                        <input type="text" class="form-control marg_bot_15" ng-change="filter()" placeholder="Search" ng-model="query.project_name">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="list == 101"> 
                                  No data found  
                            </div>
                        <div class="row" ng-show="noData != 105">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 dash_thumb_cust_wr" ng-repeat="data in filtered = (list | filter:query | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                <div class="music_genre music-media">
                                    <div class="thumb folderImg">
                                        <!-- <div class="folderImgs">{{data.file_1st_letter}}</div> -->
                                    <div class="overlay" ng-click="openFolderPopup('{{ data.project_name }}')" ><a href="javascript:void(0);"><i class="material-icons">folder</i></a></div>
                                    </div>
                                    <div class="song-info">
                                        <input type="hidden" value="{{data.project_name}}" id="pName{{data.id}}">
                                        <input type="hidden" value="{{data.id}}" id="pId">
                                        <input type="hidden" value="10" id="pPrice">
                                        <h4 class="gray_ddd_bg"><a ui-sref="app.blo-media">{{ data.project_name | capitalize}}</a><i class="fa fa-trash-o pull-right" aria-hidden="true" title="Delete Project" ng-click="deleteProject('{{data.project_name}}')" style="cursor:pointer;font-size:20px;color: #ff0000;margin:2px 3px 0 0"></i></h4>
                                        <p class="gray_eee_bg"><strong>{{noDataMsg}}{{dateMsg}}&nbsp;</strong>{{data.project_date}} <i ng-if="data.is_purchase == 0" class="fa fa-cart-plus fa-lg pull-right" ng-click="addTocart('{{data.id}}')"></i>
                                            <div ng-if="data.is_purchase == 1">
                                        <a class="btn btn-success" href="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.project_name}}/{{data.project_name}}.zip"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                        </div> 
                                        </p>
                                        <!-- <a ng-href="<?php echo base_url()?>index.php/Products/buy/{{data.id | encodeBase64}}/{{data.imgName | encodeBase64}}"><span class="btn btn-info" ng-if="data.is_pdf_dwnload == 0"><i class="glyphicon glyphicon-save-file"></i>Save as PDF</span></a> -->
                                        <a  ng-click="generatePdf('{{data.id | encodeBase64}}')"><span class="btn btn-success convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 0"><i class="fa fa-file-pdf-o"></i>Convert as PDF <!-- <i class="fa fa-spinner fa-spin" ng-show="spin"></i> --></span></a>
                                        <a target="_blank" href="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.project_name | encodeBase64}}.pdf">
                                            <span class="btn btn-primary convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 1"><i class="fa fa-file-pdf-o"></i>Download as PDF</span></a>
                                    </div>

                                </div>
                                  
                            </div>

                        </div>
                        <div class="row" ng-show="stripForPdf">
                            <div class="col-lg-12 col-xs-12 col-sm-12">
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    We have created PDF, Please <a target="_blank" href="{{dwnloadLink}}" class="alert-link">click</a> here to view
                                </div>
                            </div>
                        </div> 
                           
                        <div class="row" >
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData == 105"> 
                                  No data found  
                            </div>
                            <div class="col-md-12" ng-show="filteredItems == 0">
								<div class="col-md-12">
								<h4>No data found</h4>
								</div>
								</div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30"> 
                            <!-- {{filteredItems }} -->
                                <!--<uib-pagination total-items="totalItems" ng-model="currentPage"></uib-pagination>-->
                               <!-- <uib-pagination boundary-links="true" total-items="totalItems" page="currentPage" on-select-page="setPage(page)" items-per-page="entryLimit" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>-->
                                <!-- <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div> -->
                             <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
            </uib-pagination>
                            </div>
                           
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30" ng-show="filteredItems == 0">
                                 <h4>No data found</h4>
                            </div> -->
                        </div>


                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12" ng-switch-when="pngs">
                        <input type="text" class="form-control marg_bot_15" ng-change="filter()" placeholder="Search" ng-model="query.imgName">
                        <div class="row" ng-show="noData != 105">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 dash_thumb_cust_wr" ng-repeat="data in filtered = (list | filter:query | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                <div class="music_genre music-media">
                                    <div class="thumb">
                                    <img class="img-responsive" ng-src="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.imgPath}}">
                                    <div class="overlay"><a ui-sref="preview.fullScreenPreview({p_name:(data.imgName | encodeBase64)})"><i class="material-icons">remove_red_eye</i></a></div>
                                    </div>
                                    <div class="song-info">
                                        <input type="hidden" value="{{data.imgName}}" id="pName{{data.id}}">
                                        <input type="hidden" value="{{data.id}}" id="pId">
                                        <input type="hidden" value="10" id="pPrice">
                                        <h4 class="gray_ddd_bg"><a ui-sref="app.blo-media">{{ data.imgName | capitalize}}</a></h4>
                                        <p class="gray_eee_bg"><strong>{{noDataMsg}}{{dateMsg}}&nbsp;</strong>{{data.date}}</p>
                                        <!-- <a ng-href="<?php echo base_url()?>index.php/Products/buy/{{data.id | encodeBase64}}/{{data.imgName | encodeBase64}}"><span class="btn btn-info" ng-if="data.is_pdf_dwnload == 0"><i class="glyphicon glyphicon-save-file"></i>Save as PDF</span></a> -->
                                       <!-- <a  ng-click="generatePdf('{{data.id | encodeBase64}}')"><span class="btn btn-success convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 0"><i class="fa fa-file-pdf-o"></i>Convert as PDF <!-- <i class="fa fa-spinner fa-spin" ng-show="spin"></i> </span></a>-->
                                        <!--<a target="_blank" href="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.imgName | encodeBase64}}.pdf">
                                            <span class="btn btn-primary convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 1"><i class="fa fa-file-pdf-o"></i>Download as PDF</span></a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData == 105"> 
                                  No data found  
                            </div>
                            <div class="col-md-12" ng-show="filteredItems == 0">
                                <div class="col-md-12">
                                <h4>No data found</h4>
                                </div>
                                </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30"> 
                            <!-- {{filteredItems }} -->
                                <!--<uib-pagination total-items="totalItems" ng-model="currentPage"></uib-pagination>-->
                               <!-- <uib-pagination boundary-links="true" total-items="totalItems" page="currentPage" on-select-page="setPage(page)" items-per-page="entryLimit" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>-->
                                <!-- <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div> -->
                             <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
            </uib-pagination>
                            </div>
                           
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30" ng-show="filteredItems == 0">
                                 <h4>No data found</h4>
                            </div> -->
                        </div>


                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" ng-switch-when="gifs">
                        <input type="text" class="form-control marg_bot_15" ng-change="filter()" placeholder="Search" ng-model="query.imgName">
                        <div class="row" ng-show="noData != 105">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 dash_thumb_cust_wr" ng-repeat="data in filtered = (list | filter:query | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                <div class="music_genre music-media">
                                    <div class="thumb">
                                    <img class="img-responsive" ng-src="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.imgPath}}">
                                    <div class="overlay"><a ui-sref="preview.fullScreenPreview({p_name:(data.imgName | encodeBase64)})"><i class="material-icons">remove_red_eye</i></a></div>
                                    </div>
                                    <div class="song-info">
                                        <input type="hidden" value="{{data.imgName}}" id="pName{{data.id}}">
                                        <input type="hidden" value="{{data.id}}" id="pId">
                                        <input type="hidden" value="10" id="pPrice">
                                        <h4 class="gray_ddd_bg"><a ui-sref="app.blo-media">{{ data.imgName | capitalize}}</a></h4>
                                        <p class="gray_eee_bg"><strong>{{noDataMsg}}{{dateMsg}}&nbsp;</strong>{{data.date}}</p>
                                        <!-- <a ng-href="<?php echo base_url()?>index.php/Products/buy/{{data.id | encodeBase64}}/{{data.imgName | encodeBase64}}"><span class="btn btn-info" ng-if="data.is_pdf_dwnload == 0"><i class="glyphicon glyphicon-save-file"></i>Save as PDF</span></a> -->
                                       <!-- <a  ng-click="generatePdf('{{data.id | encodeBase64}}')"><span class="btn btn-success convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 0"><i class="fa fa-file-pdf-o"></i>Convert as PDF <!-- <i class="fa fa-spinner fa-spin" ng-show="spin"></i> </span></a>-->
                                        <!--<a target="_blank" href="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{data.imgName | encodeBase64}}.pdf">
                                            <span class="btn btn-primary convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 1"><i class="fa fa-file-pdf-o"></i>Download as PDF</span></a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                           
                        <div class="row" >
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData == 105"> 
                                  No data found  
                            </div>
                            <div class="col-md-12" ng-show="filteredItems == 0">
                                <div class="col-md-12">
                                <h4>No data found</h4>
                                </div>
                                </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30"> 
                            <!-- {{filteredItems }} -->
                                <!--<uib-pagination total-items="totalItems" ng-model="currentPage"></uib-pagination>-->
                               <!-- <uib-pagination boundary-links="true" total-items="totalItems" page="currentPage" on-select-page="setPage(page)" items-per-page="entryLimit" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>-->
                                <!-- <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div> -->
                             <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
            </uib-pagination>
                            </div>
                           
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30" ng-show="filteredItems == 0">
                                 <h4>No data found</h4>
                            </div> -->
                        </div>


                    </div>
                   <!--  <div class="col-md-12 col-sm-12 col-xs-12">
                        <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- Add to cart success popup  -->
<div class="modal fade addToCartPopup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style="width: 350px;">
      <div class="modal-body">
        One item was added in your cart

      </div>
     <!-- <div class="modal-footer">
        <input type ="hidden" value="1" id="addTocartReload">
        <button type="button" class="btn btn-primary" ng-click="reloadCartPage()">Done</button>
      </div>-->
    </div>
  </div>
</div>
<!-- End - Add to cart success popup  -->
<!--files popup-->
        <div class="modal fade filesPopup" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" style="">
          <div class="modal-dialog" role="document" style="width: 85%;">
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Project's files</h4>
              </div>
              <div class="modal-body" style="height: 600px;overflow:auto;">
                    <div class="col-md-12 col-sm-12 col-xs-12 animate-switch">
                            <input type="text" class="form-control marg_bot_15" ng-change="filter()" placeholder="Search" ng-model="query2.imgName">
                            <div class="row" ng-show="noData != 105">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 dash_thumb_cust_wr" ng-repeat="data in filtered = (list2 | filter:query2 | orderBy : predicate :reverse) | startFrom:(currentPage2-1)*entryLimit2 | limitTo:entryLimit2">
                                    <div class="music_genre music-media">
                                        <div class="thumb">
                                        <img class="img-responsive" ng-src="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{foldernames}}/{{data.imgPath}}">
                                        <div class="overlay" ng-click="removePoups()"><a ui-sref="preview.fullScreenPreview({p_name:(data.imgName | encodeBase64)})"><i class="material-icons">remove_red_eye</i></a></div>
                                        </div>
                                        <div class="song-info">
                                            <input type="hidden" value="{{data.imgName}}" id="pName{{data.id}}">
                                            <input type="hidden" value="{{data.id}}" id="pId">
                                            <input type="hidden" value="10" id="pPrice">
                                            <h4 class="gray_ddd_bg"><a ui-sref="app.blo-media">{{ data.imgName | capitalize}}</a></h4>
                                            <p class="gray_eee_bg"><strong>{{noDataMsg}}{{dateMsg}}&nbsp;</strong>{{data.date}}</p>
                                            <!-- <a ng-href="<?php echo base_url()?>index.php/Products/buy/{{data.id | encodeBase64}}/{{data.imgName | encodeBase64}}"><span class="btn btn-info" ng-if="data.is_pdf_dwnload == 0"><i class="glyphicon glyphicon-save-file"></i>Save as PDF</span></a> -->
                                            <a  ng-click="generatePdf('{{data.id | encodeBase64}}','{{foldernames}}')"><span class="btn btn-success convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 0"><i class="fa fa-file-pdf-o"></i>Convert as PDF <!-- <i class="fa fa-spinner fa-spin" ng-show="spin"></i> --></span></a>
                                            <a target="_blank" href="<?php echo base_url()?>saveImg/<?php echo base64_encode($this->session->userdata('user_id'))?>/{{foldernames}}/{{data.imgName | encodeBase64}}.pdf">
                                            <span class="btn btn-primary convrt_dwnl_btn" ng-if="data.is_pdf_dwnload == 1"><i class="fa fa-file-pdf-o"></i>Download as PDF</span></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row" ng-show="stripForPdf">
                                <div class="col-lg-12 col-xs-12 col-sm-12">
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        We have created PDF, Please <a target="_blank" href="{{dwnloadLink}}" class="alert-link">click</a> here to view
                                    </div>
                                </div>
                            </div>
                               
                            <div class="row" >
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData2 == 105"> 
                                      No data found  
                                </div>
                                <div class="col-md-12" ng-show="filteredItems2 == 0">
                                    <div class="col-md-12">
                                    <h4>No data found</h4>
                                    </div>
                                    </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30"> 
                                <!-- {{filteredItems }} -->
                                    <!--<uib-pagination total-items="totalItems" ng-model="currentPage"></uib-pagination>
                                    <uib-pagination boundary-links="true" total-items="totalItems" page="currentPage" on-select-page="setPage(page)" items-per-page="entryLimit" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>-->
                                    <!-- <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div> -->
                                <uib-pagination
     boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems2"ng-model="currentPage2"items-per-page="entryLimit2"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;">
                </uib-pagination>
                                </div>
                               
                                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-top-30 padding-bottom-30" ng-show="filteredItems == 0">
                                     <h4>No data found</h4>
                                </div> -->
                            </div>
                        </div>
              </div>
            </div>
          </div>
        </div>
<!--files popup-->
</div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ng-controller="pdfDesignList">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">PDF Design list</h2>
                <!-- <input type ="text" ng-model="search" class="form-control" placeholder="Search" >  -->
                <div class="actions panel_actions pull-right">
                    <i class="box_toggle material-icons">expand_more</i>
                    <!-- <i class="box_setting material-icons" ng-click="section_modal()">mode_edit</i> -->
                    <i class="box_close material-icons">close</i>
                </div>
            </header>
            <div class="content-body padding-bottom-15">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData == 105"> 
                                  No data found  
                            </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 pdf_dsgn_and_payment_dtls_table" ng-if="noData!=105">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <!-- <th>Last Name</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="data in list | filter:search">
                                    <th>{{$index+1}}</th>
                                    <td>{{data.imgName | capitalize}}</td>
                                    <!-- <td>the Bird</td> -->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ng-controller="transactionList">
        <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Payment details</h2> 
                <div class="actions panel_actions pull-right">
                    <i class="box_toggle material-icons">expand_more</i>
                    <!-- <i class="box_setting material-icons" ng-click="section_modal()">mode_edit</i> -->
                    <i class="box_close material-icons">close</i>
                </div>
            </header>
            <div class="content-body padding-bottom-15">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="noData == 105"> 
                                  No data found  
                            </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 pdf_dsgn_and_payment_dtls_table" ng-if="noData!=105">
                        <table class="table table-striped ">
                            <thead>
                                <tr> 
                                    <th>#</th>
                                    <th>Design Name</th>
                                    <th>Txn ID</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="data in list">
                                    <th scope="row">{{$index+1}}</th>
                                    <td>{{data.project_name | capitalize}}</td>
                                    <td>{{data.txn_id}}</td>
                                    <td><span class="label label-success">{{data.payment_status}}</span>
                                    </td>
                                </tr>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>    
</div>
