/**
 *
 * Responsive website using AngularJS
 * http://www.script-tutorials.com/responsive-website-using-angularjs/
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2013, Script Tutorials
 * http://www.script-tutorials.com/
 */

'use strict';

// optional controllers

function HomeCtrl($scope, $http) {
	
}

function logiCtrl($scope, $http) {
	$scope.chkAdminInof = function () {
		 var data = 'admin';
		 window.location = "http://"+data+"."+window.location.hostname+"/index.php/admin/dashboard";
		
	}
}

function ProjectCtrl($scope, $http) {}

function MailCtrl($scope, $http, $timeout) {}

function GeneralCtrl($scope, $http, $timeout) {}

function IconsCtrl($scope, $http, $timeout) {}

function SliderCtrl($scope, $http, $timeout) {}

function MorrisCtrl($scope, $http) {}

function EditorsCtrl($scope, $http) {}

function AboutCtrl($scope, $http, $timeout) {}

function AdvancedCtrl($scope, $http, $timeout) {}

function GeneralElementCtrl($scope, $http, $timeout) {}

function DataTableCtrl($scope, $http, $timeout) {}

function TableCtrl($scope, $http, $timeout) {}

function ButtonCtrl($scope, $http, $timeout) {}

function TyphographyCtrl($scope, $http, $timeout) {}

function CalendarCtrl($scope, $http, $timeout) {}

function InvoiceCtrl($scope, $http, $timeout) {}

function MasonryCtrl($scope, $http, $timeout) {}

function MapCtrl($scope, $http, $timeout) {}

function ErrorCtrl($scope, $http, $timeout) {}

function TimeLineCtrl($scope, $http, $timeout) {}

function BlankCtrl($scope, $http, $timeout) {}

function BlogListCtrl($scope, $http, $timeout) {}

function BlogDetailCtrl($scope, $http, $timeout) {}

function FloatCtrl($scope, $http, $timeout) {}

function ShopCtrl($scope, $http, $timeout) {}

function ShopDetailCtrl($scope, $http, $timeout) {}

function ShopListCtrl($scope, $http, $timeout) {}

function PetaCtrl($scope, $http, $timeout) {}

app.controller('ProjectList',['$scope','$http','$timeout', function($scope,$http,$timeout)
{
   $scope.infovars=false;
    $http.get(base_url+'admin/project/get_project_list').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 8; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData = $scope.list;
      }); 

      $scope.deleteThisProject = function(proid)
      {
        var confirmdeletes=confirm("Do you want delete this project ?");
        if(confirmdeletes = true)
        {
        }
        else
        {
          $http.get(base_url+'admin/project/delete_this_project/'+proid).success(function(response) {//$scope.userdata = response;
          //$scope.list = response;  
          window.location.reload();
          });
        }
      	
      } 

     $scope.openProjectFile = function(proname,userIds)
     {
        var parameters=proname+"_"+userIds;
        $("#projectFiles").modal('show');
        $("#myModalLabel").text(proname+"'s files");
        $http.get(base_url+'admin/project/get_project_files_list/'+parameters).success(function(response) {//$scope.userdata = response; 
        $scope.list3 = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 8; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData = $scope.list;
      }); 
     } 
     $scope.makeBestDesigns = function()
      {
        var values = $('input:checkbox:checked.group1').map(function () {
                      return this.value;
                    }).get();
        var maindata=values.toString();
         $http.post(base_url+'admin/project/best_design/'+maindata).success(function(response) {//$scope.userdata = response;
            $scope.infovars=true;
            $scope.infomsgs="Your selection is saved";
            $timeout(function(){$scope.infovars=false;},3000);
           });
      
      } 
}]);

app.controller('UserList',['$scope','$http', function($scope,$http)
{
      $http.get(base_url+'admin/user/get_user_list').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 12; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData = $scope.list;  
      });

      $scope.deleteThisUser = function(userid)
      {
      	 $http.get(base_url+'admin/user/delete_this_user/'+userid).success(function(response) {//$scope.userdata = response;
	        //$scope.list = response;  
	        window.location.reload();
	      });
      }

      $scope.userDetails = function(userids)
      {
        $("#userDetail").modal('show');
        $http.get(base_url+'admin/user/user_details/'+userids).success(function(response) {
          $scope.userlist = response;
        });
      }
}]);

app.controller('logiCtrl',['$scope','$http', function($scope,$http)
{
    $scope.chkAdminInof = function()
    {
      var username=$("#adminUser").val();
      var passsword=$("#adminPass").val();
      var parametres=username+"_"+passsword;
     $http.get(base_url+'admin/login/user_login/'+parametres).success(function(response) {//$scope.userdata = response;
          
          if(response == "101")
          {
           window.location=base_url+"admin/project"; 
          }
          else
          {
            $scope.errors="Some error occurs !";
          }
       });
    }
}]);
app.controller('FileCost',['$scope','$http','$timeout', function($scope,$http,$timeout)
{
  $scope.infovar=false;
  $scope.filecost = function()
  {
    var price=$("#filePrice").val();
     $http.post(base_url+'admin/project/file_price/'+price).success(function(response) {//$scope.userdata = response;
        $scope.infovar=true;
        $scope.infomsg="Your file cost is updated";
        $timeout(function(){$scope.infovar=false;},3000);
        $("#filePrice").val("");
       });

  }
  
}]);

app.controller('CalculatePrice',['$scope','$http','$timeout', function($scope,$http,$timeout)
{
  $scope.loaderImgs = true;
  //$scope.infovar=false;
  $scope.displayPrice = function(proname,userid)
  {
      //alert(proname);
      var parametres=proname+"_"+userid;
     $http.get(base_url+'admin/project/get_project_price/'+proname).success(function(response) {//$scope.userdata = response;
         $scope.pricedata = response;
         $scope.loaderImgs = false;
       });
     $http.get(base_url+'admin/project/getBase64String/'+proname+'/'+userid).success(function(response) {//$scope.userdata = response;
        $scope.imgData = response;
       });

  }
  
}]);
