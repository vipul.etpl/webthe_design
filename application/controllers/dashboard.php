<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('login_model'); 
        $this->load->model('userprofile_model'); 
        $this->load->model('signup_model');
        $this->load->library('paypal_lib');
        $this->load->library('cart');

        $errors = array();
        $data = array();
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        

    }

	public function index(){
       // echo $this->session->userdata('user_id');
        if($this->session->userdata('user_id')!='')
            {
                $this->dashboard_view();         
            }else
                {
                    // redirect('happyedit.com'); 
                    echo "<script>window.location = 'http://happyedit.com/#/login'</script>"; 
                }
	   } 

	public function dashboard_view(){ 
        if($this->session->userdata('user_id')=='')
            {
               // redirect('happyedit.com');   
                    echo "<script>window.location = 'http://happyedit.com/#/login'</script>"; 

            }else
                {
                    $data = array();
                    $data['title'] = "Happy Edit - Dashboard";
                    $udata = array();
                    $udata['is_email'] = $this->login_model->is_email_verified($this->session->userdata('user_id'));
                    $this->load->view('includes/dashboard/page_start',$data); 
                    $this->load->view('dashboard/dashboard_view',$udata);
                    $this->load->view('includes/dashboard/page_end');
	            }
            }

/*
* Aishvarya Shrivastava - 27/01/2016
* Angular JS 
* UserDashboard
* Don't change any thing before discussion 
*
*/
    public function loadInitialView(){
        $this->load->view('dashboard/partials/app');
    }

    public function loadUserDashView(){
        // $data = array();
        // $data['title'] = "Happy Edit - Dashboard";
        // $this->load->view('includes/dashboard/page_start',$data); 
        $this->load->view('dashboard/partials/app_dashboard');
        // $this->load->view('includes/dashboard/page_end');
    }

    //Angular JS topbar layout
    public function topbar(){
         $data['imgs']=$this->userprofile_model->chkProfileImg();
        $this->load->view('dashboard/partials/components/topbar', $data); 
    }

    public function previewTopbar(){
        $this->load->view('dashboard/partials/components/previewTopbar');
    }

    public function leftbar(){
        $data['imgs']=$this->userprofile_model->chkProfileImg();
        $this->load->view('dashboard/partials/components/leftbar', $data);
    }

     public function leftbarHospital(){
        $this->load->view('dashboard/partials/components/leftbar.hospital');
    }

    public function leftbarUniversity(){
        $this->load->view('dashboard/partials/components/leftbar.university');
    }

    public function footer(){
        $this->load->view('dashboard/partials/components/footer');
    }
    public function rightbar(){
        $this->load->view('dashboard/partials/components/rightbar');
    }
    public function nav(){
        $this->load->view('dashboard/partials/components/nav');
    }

    public function uiProfile(){
        $data['imgs']=$this->userprofile_model->chkProfileImg();
        $this->load->view('dashboard/partials/ui-profile',$data);
        
    }
    public function formEditable()
    {
        $this->load->view('dashboard/partials/form-editable');
        
    }
    public function manageDomain()
    {
        $this->load->view('dashboard/partials/managedomain');
        
    }
    public function searchdomain()
    {
        $this->load->view('dashboard/partials/searchdomain');
        
    }

    public function searchDomainList($doaminData)
    {
        $data = array();
        $data['search'] = $doaminData;
        $this->load->view('dashboard/partials/searchdomain',$data);
        
    }


/*
* Aishvarya Shrivastava - 28/01/2016
* Angular JS 
* UserDashboard
* Get design details as per DB 
*
*/
    public function getPngImgThroughId(){
        $imgList = $this->userprofile_model->getPngImgThroughId($this->session->userdata('user_id'));
        echo json_encode($imgList);
    }
     public function getPngImgThumbname(){
        $imgList = $this->userprofile_model->getPngImgThumbname($this->session->userdata('user_id'));
        echo json_encode($imgList);
    }
   public function deleteproject($projname)
   {
        $imgList = $this->userprofile_model->deletemyproject($this->session->userdata('user_id'), $projname);
        
        //rmdir($path);
        echo json_encode($imgList);
   }
    public function getPngImgFileThumbname($projectNamesfiles){
        $imgList = $this->userprofile_model->getPngImgFileThumbname($projectNamesfiles);
        echo json_encode($imgList);
    }

    public function getPdfStatuslist()
      {
        $imgList = $this->userprofile_model->getPdfStatuslist($this->session->userdata('user_id'));
            echo json_encode($imgList);
      }
      public function getTransactionList()
      {
        $imgList = $this->userprofile_model->getTransactionList();
            echo json_encode($imgList);
      }

/*
* Aishvarya Shrivastava - 29/01/2016
* Payment Success on  
* UserDashboard
* And Store Transaction info on DB 
*
*/

function payment_success(){
        //get the transaction data 
        $paypalInfo = $this->input->post();
         
        if (count($this->cart->contents())>0) {
            // for ($i=1; $i <= count($this->cart->contents()); $i++) { 
            $i=0;
            $j = 0;
            $data = array();
            foreach ($this->cart->contents() as $key => $value) {
                $data[$i]['user_id'] = $this->session->userdata('user_id');
                $data[$i]['product_id'] = $value['id'];
                $data[$i]['txn_id'] = $paypalInfo["txn_id"];
                $data[$i]['payment_gross'] = $paypalInfo["payment_gross"];
                $data[$i]['currency_code'] = $paypalInfo["mc_currency"];
                $data[$i]['payer_email'] = $paypalInfo["payer_email"];
                $data[$i]['payment_status'] = $paypalInfo["payment_status"];
                $i++;     
               $paypalURL = $this->paypal_lib->paypal_url;     
              $result = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
            //insert the transaction data into the database
            // $status = $this->userprofile_model->insertTransaction($data);
            // if ($status) {
                $updateStatus = $this->userprofile_model->updateImageStatusValue($value['id']);
                if ($this->zipTest($value['name'])) {
                    if ($this->sendMailFuncationToUser($value['name'])) {
                             
                    }else{
                    echo "Internal Server Error";
                    }
                }else{
                    echo "Internal Server Error";
                }
                
            // }else{
            //     echo "Some Problem";
            // }

         }
         // echo "<pre>";
          $status = $this->userprofile_model->insertTransaction($data);
          if ($status) {
             $this->cart->destroy();  
            redirect('dashboard');   
          }else{
            echo "Internal Server Error";
          }       
        }else{
           echo "Internal Server Error"; 
        }
        
    }


         public function test(){
            $getBase64TxtFileVal = $this->userprofile_model->getBase64TxtFileVal('1');
            print_r($getBase64TxtFileVal[0]['imgName']);
         }
         function pdf($p_id,$projectName)
            {
                 $this->load->helper('pdf_helper'); 
                 $data = array();
                    tcpdf();
                    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                    $obj_pdf->SetCreator(PDF_CREATOR);
                    $title = "Happy Edit";
                    $obj_pdf->SetTitle($title);
                    $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
                    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                    $obj_pdf->SetDefaultMonospacedFont('helvetica');
                    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                    $obj_pdf->SetFont('helvetica', '', 9);
                    $obj_pdf->setFontSubsetting(false);
                    $obj_pdf->AddPage();
                    ob_start();
                    $getBase64TxtFileVal = $this->userprofile_model->getBase64TxtFileVal(base64_decode($p_id));
                     
                    //$myfile = file_get_contents("C:\\xampp\htdocs\happyEdit\saveImg".base64_encode($this->session->userdata('user_id'))."/".$getBase64TxtFileVal[0]['imgName'].".txt");
                    $explodeHtml = explode('.', $getBase64TxtFileVal[0]['imgName']);
                   // echo  $explodeHtml[0];
                    // echo base64_encode($explodeHtml[0]);
                    // exit();
                     $myfile = file_get_contents(base_url()."saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/index2.txt");
                    //echo $explodeHtml[0].".txt";
                    // echo $myfile;exit;
                    $data = str_replace("data:image/png;base64,", "", $myfile);
                    //echo  $data;exit;
                    $content = base64_decode($data);
                   // echo $content;exit;
                    //ob_end_clean();
                    $obj_pdf->Image('@'.$content, '', '', 0,65, '', '', 'B', false, 300, 'C');
                    //$obj_pdf->writeHTMLCell(0, 10, '', '', $content, 1, 1, 0, true, '', true);
                    //$filename= "Invoice.pdf"; 
                     $filelocation = base_url()."saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_encode('index2').".pdf";  //for Linunx
                     //$filelocation = "C:\\xampp";  //For Windows OS...
                     //$fileNL = $filelocation."/".$filename;
                    $obj_pdf->Output($filelocation, 'F');
                    $updateStatus = $this->userprofile_model->updatePdfStatusValue(base64_decode($p_id));
                    if ($updateStatus) {
                        //return 107;
                        $errors['msg'] = 107; 
                        $errors['link'] = base_url()."saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_encode('index2').".pdf";
                        echo json_encode($errors);
                    }else{
                        //return 108;
                        $errors['msg'] = 108; 
                        echo json_encode($errors);
                    }

                 //$this->load->view('pdfreport',$data);
            }
         public function full_screen_preview(){
                //ob_flush();
                 $this->load->view('dashboard/partials/fullScreenPreview');
                  //echo '<script>window.location.reload(true);</script>';
                // 
                 //Header('Location: '.$_SERVER['PHP_SELF']);

         }
        
         public function htmlPreview($htmlFilename)
         {
            // echo base64_decode($htmlFilename);
            // exit();
            $projectName=$_COOKIE['myProjects'];
            if(file_exists("/home/happyedit/public_html/saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".html"))
                {
                        $myfile = file_get_contents("/home/happyedit/public_html/saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".html");
                        echo $myfile;        
                }else if(file_exists("/home/happyedit/public_html/saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".png")){
                    $imgUrl = base_url()."saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".png";
                    echo '<img src = "'.$imgUrl.'">';
                    //echo "/home/happyedit/public_html/saveImg/".base64_encode($this->session->userdata('user_id'))."/".base64_decode($htmlFilename).".png";
                }else if(file_exists("/home/happyedit/public_html/saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".gif")){
                    $imgUrl = base_url()."saveImg/".base64_encode($this->session->userdata('user_id'))."/".$projectName."/".base64_decode($htmlFilename).".gif";
                    echo '<img src = "'.$imgUrl.'">';
                }
                else{
                    echo "fileNotFound";
                }
            

             
         }    

         public function pageNotFound()
         {
             $this->load->view('dashboard/partials/ui-404');
         }  

// This function is use for search domain name through papaki API
public function getDomainInformation()
    {
       
        error_reporting(0);
        require ("papakiGoldResellers/includes/config.php");
        require_once('papakiGoldResellers/papaki.php');
        if($_REQUEST["do"]!="newdomain"){
            require("papakiGoldResellers/usablewebLib.php"); 
            ini_set("max_execution_time", 200);
            $search = new PapakiDomainNameSearch;  
            $search->PapakiDomainNameSearch($_REQUEST["formData"],$_REQUEST['ext']);  
            $search->use_curl = true;
            $search->apikey = $Papaki_apikey;
            $search->requestURL = $papaki_Post_url;
            //$search->getExtensions($_REQUEST['ext']);
            $search->exec_request_for(_TYPE_DS,false,$_REQUEST['ext']);
             echo json_encode($search);
        }
    }
// Add to cat process

    function add()
    {
              // Set array for send data.
        $insert_data = array(
            'id' => $_REQUEST['pId'],
            'name' => $_REQUEST['pName'],
            'price' => $_REQUEST['pPrice'],
            'qty' => 1
        );      

                 // This function add items into cart.
        $cart_insert_data = $this->cart->insert($insert_data);
        if ($cart_insert_data) {
            echo "100";
        }else{
            echo "200";
        }
       
    }

    function addDomain()
    {
        $my_insert_data=array();
        $myDts=json_decode($_REQUEST['myData'],true);

        for ($i=0; $i < count($myDts); $i++) 
        { 
             $insert_data = array('id' =>$myDts[$i]['pId'],'name' =>$myDts[$i]['pName'],'price' =>$myDts[$i]['pPrice'],'qty' =>1 );
             array_push($my_insert_data,  $insert_data);
        }
        //print_r($my_insert_data); 
        $cart_insert_data = $this->cart->insert($my_insert_data);
        if ($cart_insert_data) {
            echo "100";
        }else{
            echo "200";
        }
       
    }
    
        function remove($rowid) {
                    // Check rowid value.
        if ($rowid==="all"){
                       // Destroy data which store in  session.
            $this->cart->destroy();
            echo 100;
        }else{
                    // Destroy selected rowid in session.
            $data = array(
                'rowid'   => $rowid,
                'qty'     => 0
            );
                     // Update cart data, after cancle.
            $this->cart->update($data);
            echo 100;
        }
        
                 // This will show cancle data in cart.
        // redirect('shopping');
    }
    
        function update_cart(){
                
                // Recieve post values,calcute them and update
                $cart_info =  $_POST['cart'] ;
        foreach( $cart_info as $id => $cart)
        {   
                    $rowid = $cart['rowid'];
                    $price = $cart['price'];
                    $amount = $price * $cart['qty'];
                    $qty = $cart['qty'];
                    
                        $data = array(
                'rowid'   => $rowid,
                                'price'   => $price,
                                'amount' =>  $amount,
                'qty'     => $qty
            );
             
            $this->cart->update($data);
        }
        redirect('shopping');        
    }   
        function billing_view(){
                // Load "billing_view".
        $this->load->view('billing_view');
        }
        
            public function save_order()
    {
          // This will store all values which inserted  from user.
        $customer = array(
            'name'      => $this->input->post('name'),
            'email'     => $this->input->post('email'),
            'address'   => $this->input->post('address'),
            'phone'     => $this->input->post('phone')
        );      
                 // And store user imformation in database.
        $cust_id = $this->billing_model->insert_customer($customer);

        $order = array(
            'date'          => date('Y-m-d'),
            'customerid'    => $cust_id
        );      

        $ord_id = $this->billing_model->insert_order($order);
        
        if ($cart = $this->cart->contents()):
            foreach ($cart as $item):
                $order_detail = array(
                    'orderid'       => $ord_id,
                    'productid'     => $item['id'],
                    'quantity'      => $item['qty'],
                    'price'         => $item['price']
                );      

                            // Insert product imformation with order detail, store in cart also store in database. 
                
                 $cust_id = $this->billing_model->insert_order_detail($order_detail);
            endforeach;
        endif;
          
                // After storing all imformation in database load "billing_success".
                $this->load->view('billing_success');
    }

    public function cartView(){
        $this->load->view('dashboard/partials/shopping_view');
    }

    public function getPriceValWidDomainExt($domainname)
    {
            $extName = explode('.', $domainname, 2);
            $price_data = $this->userprofile_model->getPriceValWidDomainExt($extName[1]);
            echo json_encode($price_data[0]);
            // echo $extName[1];
    }

    public function orderpage(){
        $uDta['myData']=$this->signup_model->get_user_details($this->session->userdata('user_id'));
        $this->load->view('dashboard/partials/orderpage',$uDta);

    }
    public function ticketsRequests()
    {
        $AcquirerID=$_REQUEST['AcquirerID'];
        $MerchantID=$_REQUEST['MerchantID'];
        $PosID=$_REQUEST['PosID'];
        $UserName=$_REQUEST['UserName'];
        $Passwords=$_REQUEST['Passwords'];
        $MerchantReference=$_REQUEST['MerchantReference'];
        $RequestType=$_REQUEST['RequestType'];
        $ExpirePreauth=$_REQUEST['ExpirePreauth'];
        $Amount=$_REQUEST['Amount'];
        $CurrencyCode=$_REQUEST['CurrencyCode'];
        $Installments=$_REQUEST['Installments'];
        $Bnpl=$_REQUEST['Bnpl'];
        $soapPass=md5('TI122345');

        $trans_details['datas']=array('AcquirerID'=>$AcquirerID,'MerchantID'=>$MerchantID,'PosID'=>$PosID,'UserName'=>$UserName,'MerchantReference'=>$MerchantReference,'CurrencyCode'=>'el-GR');
       
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <IssueNewTicket xmlns="http://piraeusbank.gr/paycenter/redirection">
                                 <Request>
                                    <Username>HA668974</Username>
                                    <Password>'.$soapPass.'</Password>
                                    <MerchantId>2143781356</MerchantId>
                                    <PosId>2138523534</PosId>
                                    <AcquirerId>14</AcquirerId>
                                    <MerchantReference>Paymentsuccessfully</MerchantReference>
                                    <RequestType>02</RequestType>
                                    <ExpirePreauth>0</ExpirePreauth>
                                    <Amount>10.00</Amount>
                                    <CurrencyCode>978</CurrencyCode>
                                    <Installments>0</Installments>
                                    <Bnpl>0</Bnpl>
                                    <Parameters>0</Parameters>
                                  </Request>
                                </IssueNewTicket>
                              </soap:Body>
                            </soap:Envelope>';
           $headers = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: http://piraeusbank.gr/paycenter/redirection/IssueNewTicket", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
             
            $url = "https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx";

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $UserName.":".$soapPass); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


            // converting
            $response = curl_exec($ch);   
            print_r($response);
           
            //exit;
            curl_close($ch);
            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);
            //echo json_encode(array("status"=>0)); 
            //$this->load->helper('xml');
            //$xmlss=xml_convert($response);
            //print_r($xmlss);
            echo '<html>
                    <head></head>
                    <body>
                    <form action="https://paycenter.piraeusbank.gr/redirection/pay.aspx" method="POST">
                    <input name="AcquirerId" type="hidden" value="14" />
                    <input name="MerchantId" type="hidden" value="2143781356" />
                    <input name="PosId" type="hidden" value="2138523534" />
                    <input name="User" type="hidden" value="HA668974" />
                    <input name="LanguageCode" type="hidden" value="el-GR" />
                    <input name="MerchantReference" type="hidden" value="Paymentsuccessfully" />
                    <input name="ParamBackLink" type="hidden" value="http://aish.happyedit.com/index.php/dashboard#/app/orderpage" />
                    <input type="submit" value="Check out" />
                    </form>
                    </body>
                    </html>';
             //redirect('dashboard/payment_success_form');

            // convertingc to XML
            /*$parser = simplexml_load_string($response2);
           
            $json = json_encode($parser);
            $result = json_decode($json , true);
            $status = $result["SaleKeyedResponse"]['SaleKeyedResult']['ApprovalStatus'];
            switch($status)
            {
                case "APPROVED" :
                echo json_encode(array("status" => "1","ApprovalStatus" => $result["SaleKeyedResponse"]['SaleKeyedResult']['ApprovalStatus'] ,"AuthorizationCode" => $result["SaleKeyedResponse"]['SaleKeyedResult']['AuthorizationCode'], "Token" =>  $result["SaleKeyedResponse"]['SaleKeyedResult']['Token'],"Message" => "Payment Sucess","paymentrow"=> $postData['paymentrow']));
                exit;
                case "DECLINED" :
                echo json_encode(array("status" => "0","ApprovalStatus" => $result["SaleKeyedResponse"]['SaleKeyedResult']['ApprovalStatus'] ,"AuthorizationCode" => $result["SaleKeyedResponse"]['SaleKeyedResult']['AuthorizationCode'], "Token" =>  $result["SaleKeyedResponse"]['SaleKeyedResult']['Token']));
                exit;
                default :
                echo json_encode(array("status" => "0","ApprovalStatus" => $result["SaleKeyedResponse"]['SaleKeyedResult']['ApprovalStatus'] ,"AuthorizationCode" => $result["SaleKeyedResponse"]['SaleKeyedResult']['AuthorizationCode'], "Token" =>  $result["SaleKeyedResponse"]['SaleKeyedResult']['Token'],"paymentrow"=> $postData['paymentrow']));
            }
            exit; */
    }
    public function payment_success_form()
    {
        $this->load->view('dashboard/partials/payment_success_form');
    }
    public function success_pay()
    {
        echo "Payment successfully";
    }
    public function failed_pay()
    {
        echo "Payment failed";
    }
    public function reffer_pay()
    {
        echo "Payment refferer";
    }

    // Create Zip folder as per the car list
public function create_zip($files = array(),$destination = '',$overwrite = false) {
    //if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }
    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $zip->addFile($file,basename($file));
        }
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
        //close the zip -- done!
        $zip->close();
        //check to make sure the file exists
        return file_exists($destination);
     }
       else
         {
             return false;
         }
    }

    public function zipTest($folderName){
        $filepath = glob('/home/happyedit/public_html/saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$folderName.'/*.html');
        if (count($filepath)>0) {
            $files_to_zip = $filepath;
        }
         $savePath = 'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$folderName.'/'.$folderName.'.zip';
        //if true, good; if false, zip creation failed
        $result = $this->create_zip($files_to_zip,$savePath);
        if ($result) {
            return true;
        }else{
            return false;
            //print_r($files_to_zip);
        }
    }

    public function sendMailFuncationToUser($folderName)
    {
         
          $EmailTo = $this->session->userdata('email');
          $EmailFrom = "sale@happyedit.com";
          $EmailSubject = "The Email Subject";
          $separator = md5(time());
          // carriage return type (we use a PHP end of line constant)
          $eol = PHP_EOL;
          // attachment name
          $filename = 'saveImg/'.base64_encode($this->session->userdata("user_id")).'/'.$folderName.'/'.$folderName.'.zip';
          //$filename = "saveImg/MTc=/myArchive.zip";//store that zip file in ur root directory
          $attachment = chunk_split(base64_encode(file_get_contents($filename)));
          // main header
          $headers  = "From: ".$EmailFrom.$eol;
          $headers .= "MIME-Version: 1.0".$eol; 
          $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";
          // no more headers after this, we start the body! //
          $body = "--".$separator.$eol;
          $body .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
          $body .= "PFA with this mail.".$eol;
          $message = '';
          // message
          $body .= "--".$separator.$eol;
          $body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
          $body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
          $body .= $message.$eol;

          // attachment
          $body .= "--".$separator.$eol;
          $body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
          $body .= "Content-Transfer-Encoding: base64".$eol;
          $body .= "Content-Disposition: attachment".$eol.$eol;
          $body .= $attachment.$eol;
          $body .= "--".$separator."--";
          $subject = 'Sales Notification';
          // send message
          if (mail($EmailTo, $subject, $body, $headers)) {
          $mail_sent=true;
          // echo "mail sent";
          return true;
          } else {
          $mail_sent=false;
          // echo "Error,Mail not sent";
          return false;

         }

    }

    
}
