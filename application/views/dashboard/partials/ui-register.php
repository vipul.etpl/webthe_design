<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<div class="login_page" >
<div class="register-wrapper" ng-controller="RegisterFormController">
    <div id="register" class="login loginpage col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
        <h1><a href="javascript:void(0);" title="Login Page" tabindex="-1">Happy Edit - Sign Up Page  </a></h1>

        <form  name="form" class="form-validation ui-register_form">
          <div class="text-danger wrapper text-center" ng-show="authError" style="float:none !important;">
              {{authError}}
          </div>
		  <div class="register_frm_scroll_outer">
            
			<p class="block-input_reg">
                    <span class="input_icon_reg"><i class="fa fa-pencil"></i></span>
                    <input type="text" name="name" id="user_name" class="input"  ng-model="user.name" placeholder="Name" required /></label>
            </p>
            <p class="block-input_reg">
                    <span class="input_icon_reg"><i class="fa fa-envelope"></i></span>
                    <input type="email" name="email" id="user_login" class="input" ng-model="user.email" placeholder="Email" required /></label>
            </p>
            <p class="block-input_reg">
                   <span class="input_icon_reg"><i class="fa fa-user"></i></span>
                   <input type="text" name="username" id="username" class="input" ng-model="user.username" placeholder="Username" required /></label>
            </p>
            <p class="block-input_reg">
                    <span class="input_icon_reg"><i class="fa fa-unlock-alt"></i></span>
                    <input type="password" name="pwd" id="user_pass" class="input" ng-model="user.password" placeholder="Password" required/></label>
            </p>
		
			</div>
            <p class="forgetmenot">
                            <label class="icheck icheck-white" for="rememberme">
                                <input type="checkbox" ng-model="user.chkbox" value="forever" name="rememberme" checked id="rememberme" required>
                                <i></i> <a data-toggle="modal" class="termsandcondition" data-target="#termsandCond" > I agree to Terms and Conditions</a>
                            </label>
                            <!-- Modal -->
                            <div class="modal fade" id="termsandCond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    Terms and Conditions
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                  </div>
                                  <div class="modal-body">
                                    Terms and Conditions information is not available.
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn btn-primary">Read more</button>
                                  </div>
                                </div>
                              </div>
                            </div>
            </p>
            <p class="submit">
            <button type="submit" name="wp-submit" id="wp-submit" class="btn btn-accent btn-block btn-reg-adj" ng-click="register()" ng-disabled='form.$invalid'>Sign Up</button>
            </p>
        </form>

        <div id="nav" class="tiwtterR">
		<div class="SignIn_reg_wr left_tiwtterR">
		   <a ui-sref="login" href="#/login" title="Sign In"><span><i class="fa fa-sign-in"></i></span> Sign In</a>
		</div>
            <!-- <a class="pull-left" ui-sref="forgotpassword"  title="Password Lost and Found">Forgot password?</a> -->
		<div class="SignUp_reg_wr right_tiwtterR">	
		<span><i class="fa fa-sign-out"></i></span>	Sign up with - <a href="<?php echo base_url(); ?>index.php/signup/twitter" class="social-log-reg">
			  <img class="img-circle " title="Twitter" width="25" height="25" src="<?= base_url('img/t.png'); ?>">
			</a>
        </div>
		  
        </div>
		
        <div class="clearfix"></div> 
       

    </div>
</div>
</div>




