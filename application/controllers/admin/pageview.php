<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Pageview extends CI_Controller{    

  function  __construct() {
    parent::__construct();
    $errors = array();
    $data = array();
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
    //$this->load->model('product');
  }

  public function pageName($pagename='')
  {
  		switch ($pagename) {
        case 'login':
         $this->adminLogin();
          break;
         case 'dashboard':
         $this->dashboard_view();
          break; 
        default:
          # code...
          echo "default";
          break;
      }
  }

  public function dashboard_view(){ 
    $data = array();
    $data['title'] = "Happy Edit - Admin Dashboard";
    $udata = array();
    $this->load->view('includes/admin/page_start',$data); 
    $this->load->view('admin/dashboard_view',$udata);
    $this->load->view('includes/admin/page_end');
  }

  private function adminLogin()
  {
    $data = array();
    $data['title'] = "Happy Edit - Admin Login";
    $this->load->view('includes/admin/page_start',$data); 
    $this->load->view('admin/partials/login');
    $this->load->view('includes/admin/page_end'); 
  }

  public function sidebar()
  {
    $this->load->view('admin/partials/components/sidebar'); 
  }
  public function header()
  {
    $this->load->view('admin/partials/components/header'); 
  }

  public function profile()
  {
    $this->load->view('admin/partials/components/profile'); 
    
  }

  public function dashboardMainContent()
  {
    $this->load->view('admin/partials/dashboard'); 
    
  }

   public function shopContent()
  {
    $this->load->view('admin/partials/shop'); 
    
  }
   public function timelineContent()
  {
    $this->load->view('admin/partials/timeline');
    
  }
  public function generalelement()
  {
    $this->load->view('admin/partials/generalelement');
    
  }
   public function best_design()
  {
    $this->load->view('admin/partials/best_design');
    
  }
}    