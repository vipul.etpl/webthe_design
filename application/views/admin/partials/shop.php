<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Dashboard</a>
    </li>
    <li class="active">Projects</li>
</ol>
<div class="row" ng-controller="ProjectList">
    <div class="col-md-12">
        <div class="row">
            <input type="text" class="form-control" placeholder="Search projects.." ng-model="projectSearch.project_name" style="margin: 0 0 12px;">

            <div class="col-sm-3 col-lg-3 col-md-3" ng-repeat="data in list | filter:projectSearch">
                <div class="thumbnail no-padding" ng-controller="CalculatePrice" ng-init="displayPrice(data.project_name,data.user_id)">
                    <img class="img-responsive" src="{{imgData}}" alt="">
                    <div class="caption">
                      <div class="" >
                        
                        <h4 class="pull-right"><b>$<img src="<?php echo base_url();?>img/loading-small.gif" ng-show="loaderImgs">{{pricedata}}</b>
                      </div>
                        </h4>
                        <h4><a href="javascript:void(0);" ng-click="openProjectFile('{{data.project_name}}','{{data.user_id}}')">{{data.project_name}}</a>
                        </h4>
                        <p><b>User:</b> <a href="#">{{data.first_name}} {{data.last_name}}</a></p>
                         <p><b>Date:</b> {{data.project_date}}</p>
                    </div>
                    <div class="ratings pad btn btn-danger" ng-click="deleteThisProject({{data.id}})" style="width:100%;border-radius:0;">
                        <i class="fa fontello-folder-delete"></i>&nbsp;Delete                 
                    </div>
                </div>

            </div>
             <p ng-show="(list | filter:projectSearch).length == 0" style="font-size: 16px;text-align: center;">No data found</p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding-bottom-30" ng-show="list == 101"> 
                  No data found  
            </div>

        </div>
        <!-- <uib-pagination
 boundary-links="true"direction-links="true"rotate="true"total-items="filteredItems"ng-model="currentPage"items-per-page="entryLimit"max-size="5"class="pagination-sm"on-select-page="setPage(page)"previous-text="&laquo;" next-text="&raquo;"items-per-page="entryLimit">
        </uib-pagination>
 -->
    </div>
    <div class="modal fade" id="projectFiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:90%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body" style="overflow-y: auto; height: 600px;">
             <div class="col-lg-2 col-md-2 col-sm-2" ng-repeat="data in list3">
                <img src="<?php echo base_url(); ?>index.php/saveImg/<?php echo base64_encode($this->session->userdata('user_id')); ?>/{{data.project_name}}/{{data.imgPath}}" style="width:100px;height:100px;">
             </div>
          </div>
          
        </div>
      </div>
    </div>

</div>


