<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller{
    public function __construct(){
        parent::__construct();  
        $errors = array();
        $data = array();
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        error_reporting(0);
    }
   public function index(){
       $data = array();
       $data['title'] = "Happy Edit - Sign Up";
       $data['name'] = "";
       $data['email'] = "";
       $this->signup($data);                       
   }

   public function signup($data){
       $this->load->view('includes/default_header', $data);                                                  
       $this->load->view('social/signup_with_fb');
       $this->load->view('signup/signup_view', $data);
       $this->load->view('includes/modal_help');
       $this->load->view('includes/page_end');
   }
   
   // Signup with facebook - Aishvarya_Shrivastava
   public function facebook(){
        $this->load->model('signup_model');
        $data = array();
        $data = $this->signup_model->db_signup();                                    
        $this->session->set_userdata($data);
        redirect('dashboard');
   }
   
   // Signup with twitter - Aishvarya_Shrivastava
   public function twitter(){
       $this->load->model('signup_model');
       $this->signup_model->twitter_ope(); 
   }
   
   // Twitter Responce page 
   public function twittersu(){
      // Register user using Twitter and do login
       $this->load->model('signup_model');
       $this->signup_model->twitter_register();  
   }
   
   // Get email - Aishvarya_Shrivastava
   public function twitter_email(){ 
       $this->load->model('signup_model');                            
       if($this->signup_model->check_social_id('twitter_id', $this->session->userdata('twitter_id'))){
                 $udata = array();
                 $udata = $this->signup_model->get_user_details($this->signup_model->get_user_id_using_social_id('twitter_id', $this->session->userdata('twitter_id')));
                 $this->session->set_userdata($udata);
                 echo "<script>
                 var username = '".$this->session->userdata('username')."'+'.';
                 var dashboard_url = 'http://'+username+window.location.hostname+'/index.php/dashboard';
                 window.location.assign(dashboard_url);
                 </script>";
                 // redirect('dashboard');  
       }else{
           $this->twitter_get_email();
       }                            
   }
   
   // Register User with Twitter
   public function twitter_register_user(){
       $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean|callback_validate_email');
       if($this->form_validation->run() == FALSE) {
           $this->twitter_get_email(); 
       }else{
            $this->load->model('signup_model');
            $data = array();
            $data["id"] = $this->session->userdata('twitter_id');
            $data["name"] = $this->session->userdata('name');
            $data["first_name"] = $this->session->userdata('first_name');
            $data["last_name"] = $this->session->userdata('last_name');  
            $data["email"] = $this->input->post('email');    
            $data["username"] = $this->session->userdata('username');
            // Check if user already registered with this Twitter ID
            if($this->signup_model->check_social_id('twitter_id', $this->session->userdata('twitter_id'))){
                 $udata = array();
                 $udata = $this->signup_model->get_user_details($this->signup_model->get_user_id_using_social_id('twitter_id', $this->session->userdata('twitter_id')));
                 $this->session->set_userdata($udata);
                 // redirect('dashboard');  
                 echo "<script>
                 var username = '".$this->session->userdata('username')."'+'.';
                 var dashboard_url = 'http://'+username+window.location.hostname+'/index.php/dashboard';
                 window.location.assign(dashboard_url);
                 </script>";
            }else{
                $udata = array();  
                $udata = $this->signup_model->twitter_store_login_data($data);
                $this->session->set_userdata($udata);
                 if (!file_exists('saveImg/'.$this->session->userdata('user_id').'')) {
                mkdir('saveImg/'.base64_encode($this->session->userdata('user_id')).'', 0777, true);
                //echo $this->session->userdata('username');  
                echo "<script>
                 var username = '".$this->session->userdata('username')."'+'.';
                 var dashboard_url = 'http://'+username+window.location.hostname+'/index.php/dashboard';
                 window.location.assign(dashboard_url);
                 </script>";          

                  }else{
                      $errors['error'] = 104; 
                      echo json_encode($errors);  
                  }
                
                // redirect('dashboard'); 
            }
       }
   }
   
   public function twitter_get_email(){
       $data = array();
       $data['title'] = "Happy Edit - Twitter Sign Up";
       $this->load->view('includes/home/page_start', $data);
       $this->load->view('twitter_get_email/twitter_get_email_view', $data);
       $this->load->view('includes/home/page_end', $data);
   }
   
   // To Reigister the
   public function register_user(){ 
        if ($this->validate_email($_REQUEST['email']) == false) {
            $errors['error'] = 101; 
            echo json_encode($errors);        
        }else if(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL))
        {
          $errors['error'] = 111;
          echo json_encode($errors);
        }else if ($this->validate_username($_REQUEST['username']) == false) {
            $errors['error'] = 102; 
            echo json_encode($errors);        
        }else if ($this->space_check($_REQUEST['name']) == false) {
            $errors['error'] = 103; 
            echo json_encode($errors); 
        }else if ($this->specialCharVal($_REQUEST['username']) == false) {
            $errors['error'] = 110; 
            echo json_encode($errors); 
        }
        else {
            $this->load->model('signup_model');
            $login_data = array();
            $login_data["name"] = $_REQUEST['name'];
            $login_data["email"] = $_REQUEST['email'];
            $login_data["password"] = $_REQUEST['password'];
            $login_data["username"] = $_REQUEST['username'];
            $udata = array();
            $udata = $this->signup_model->store_login_data($login_data);
            $this->session->set_userdata($udata);
            if (!file_exists('saveImg/'.$this->session->userdata('user_id').'')) {
                mkdir('saveImg/'.base64_encode($this->session->userdata('user_id')).'', 0777, true);
                echo $this->session->userdata('username');            

            }else{
                $errors['error'] = 104; 
                echo json_encode($errors);  
            }
            
        }
   }
   
   public function specialCharVal($value)
   {
     if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $value))
        {
            return false;
        }else{
            return true;
        }
   }

   // Register user using Google plus
   public function google($user_id, $google_id){
     $this->load->model('signup_model');         
     $salt = $this->signup_model->get_salt_by_google_id($google_id);
     $this->load->model('login_model');
     $user_id = $this->login_model->current_id_to_validate($user_id, $salt);
     if($this->signup_model->check_google_id($google_id, $user_id)){                                                                
         $udata = array();
         $udata = $this->signup_model->get_user_details($user_id);
         $this->session->set_userdata($udata);
         redirect('dashboard'); 
     }    
   }

   // Check email is exist
    public function validate_email($email){
        $this->load->model('signup_model');
        if($this->signup_model->check_email_exist($email)){
           //$this->form_validation->set_message("validate_email", "Email already exist.");
           return false;
        }else{
          return true;
        }
    }

    // Check username is exist
    public function validate_username($username){
        $this->load->model('signup_model');
        if($this->signup_model->check_username_exist($username)){
           //$this->form_validation->set_message("validate_email", "Email already exist.");
           return false;
        }else{
          return true;
        }
    }

    // Check Name
    public function space_check($str) {
     $pos = strrpos($str, " ");
     if ($pos === false) { // note: three equal signs
      // not found...
      $this->form_validation->set_message('space_check', 'Name must contain SPACE between full Name');
      return FALSE;
    }
    else  {
      return true;
    }
  }
}