<div class="full-width-video__element">
    <video preload="auto" poster="video.jpg" muted="" loop="" class="full-width-video__player" autoplay="">
        <source type="video/webm" src=""></source>
        <source type="video/mp4" src="http://microlancer.lancerassets.com/v2/services/ec/cf7ba0a2ad11e4a0d19fbd01f431c7/medium_video_studio-bg.mp4"></source>
    </video>
</div>
<div class="login_page " style="margin-top:-50%">
    <div class="login-wrapper" ng-controller="ForgotpwdFormController">
        <div id="login" class=" login loginpage forgotpwd_form_wrp col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
            <h1><a href="javascript:void(0);" title="Login Page" tabindex="-1">Happy Edit - Forgot Password Page</a></h1>
                <form name="form" class="form-validation forgotpwd_form">
                    
                      <div class="text-danger text-center message">
                      {{authError}}
                      </div>
                        <p> 
						    <span class="input_icon_login"><i class="fa fa-envelope"></i></span>
                            <input type="email" name="email" id="user_login" class="input" ng-model="user.email" placeholder="Email" required autofocus/></label>
                        </p>
                       
                        <p class="submit">
                            <button type="submit" ng-click="forgotpwdlink()" ng-disabled='form.$invalid' class="btn btn-accent btn-block">Send link <i class="fa fa-spinner fa-spin" ng-show="lodingspin"></i></button>
                        </p>
                </form>
            <!-- <div class="alert alert-success" ng-show="authError">
            <p>A reset link sent to your email address. &nbsp;<a ui-sref="login" style='color:#ffffff;font-weight:normal;'>Click to Sign in now</a></p>
          </div> -->
        </div>
    </div>
</div>
<script>
$(document).ready(function()
{
$("#user_login").focus();
}); 
</script>






