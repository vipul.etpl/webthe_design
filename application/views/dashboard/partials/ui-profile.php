<div class='content-wrapper' ng-init="app.settings.pagetitle = 'Profile';">
    <div class="col-lg-12" ng-controller="loginUserInformation" ng-init="getUserInformation('<?php echo $this->session->userdata('user_id');?>')"> 
        <section class="box nobox"> 
            <div class="content-body">
                <div class="row white-bg-and-space">
                    <div class="col-md-2 col-sm-3 col-xs-12 padding-right-0">
                        <div class=" uprofile-wrapper">
                            <div class="uprofile-image">
                                <img ng-src="{{myCroppedImage}}" ng-show="myCropPic" id="cropImgs"/>
                                <img src="<?php echo base_url().'/'.$imgs;?>" ng-show="myDefaultPic" class="img-responsive">
                                <div class="form-group" style="opacity:0.0;margin: -48px 0 0;">
                                    <label for="fileInput" class="form-label">File input</label>
                                    <input type="file" id="fileInput" accept="image/png,image/jpeg,image/jpg"> 
                                </div>
                                
                                <img class="br_bt" src="<?php echo base_url()?>img/camera.png" onclick="$('#fileInput').click()">
                                <div class="cam_bg"></div>

                            </div>
                            <div class="uprofile-name">
                                <h3 class="uprofile-name_bg_new">
                                    <a href="javascript:void(0);" class="disp_block" ><?php echo ucfirst($this->session->userdata('username'));?>  <span class="uprofile-status online"></span></a>
                                    <!-- Available statuses: online, idle, busy, away and offline -->
                                   
                                </h3>
                                <!-- <p class="uprofile-title">Web Developer</p> -->
                                <!-- <p class="text-light profile-text">Hello, I am John. I love working on web. I do photoshop, php, wordpress, css, html, ajax etc.</p> -->
                               <!--  <p class="margin-top-30">
                                    <a href="#" class="btn btn-default btn-md text-light"><i class="fa fa-dribbble icon-xs"></i></a>
                                    <a href="#" class="btn btn-default btn-md text-light"><i class="fa fa-google-plus icon-xs"></i></a>
                                    <a href="#" class="btn btn-default btn-md text-light"><i class="fa fa-twitter icon-xs"></i></a>
                                    <a href="#" class="btn btn-default btn-md text-light"><i class="fa fa-facebook icon-xs"></i></a>
                                </p> -->
                             
                                <!-- < a class="btn btn-md btn-default" style='width:150px;'>Follow</a> -->
                                <!-- <div class="margin-top-30">
                                    <div class="col-xs-4 bg-white">
                                        <strong class="uprofile-count">760</strong>
                                        <span>Follower</span>
                                    </div>
                                    <div class="col-xs-4 bg-white">
                                        <strong class="uprofile-count">280</strong>
                                        <span>Following</span>
                                    </div>
                                    <div class="col-xs-4 bg-white">
                                        <strong class="uprofile-count">4104</strong>
                                        <span>Tweets</span>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="uprofile-buttons margin-0 padding-top-0 text-center">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-9 col-xs-12 padding-left-0">
                       <div class="uprofile-content ">
                            <div class="clearfix"></div>
                            <div class="uprofile_wall_posts col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
								<div class="user_prof_intrnl_wrp">
								<div class="Profile_heading_wrapp">
								<h4 class="user_detail_heading">User Profile Details</h4>
								</div>
                                     <div class="form-group border-bottom_ddd">
                                        <label class="form-label" style="font-weight: normal !important;"><span class="fa fa-envelope"></span> Email</label>
                                        <div class="controls email">{{ userData.email || 'empty' }}</div>
                                    </div>
                                    <div class="form-group border-bottom_ddd">
                                        <label class="form-label" style="font-weight: normal !important;"><span class="fa fa-pencil-square-o"></span> Full Name</label>
                                        <div class="controls">{{ userData.first_name || 'empty' }}&nbsp;{{ userData.last_name || 'empty' }}</div>
                                    </div>
                                    <!-- <div class="form-group border-bottom_ddd">
                                        <label class="form-label"><span class="fa fa-user"></span> Username</label>
                                        <div class="controls">{{ userData.username || 'empty' }}</div>
                                    </div> -->
									<div class="form-group">
                                        <label class="form-label"></label>
                                        <div class="controls"><a class="pull-left btn btn-primary" ui-sref="app.form.editable">Edit info</a></div>
                                    </div>
									
                                    
                                    </div>
                                </div>
                             </div>
                            </div>
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
        
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="">
                                        <div class="bg-light" style="height:320px">
                                            <img-crop image="myImage" result-image="myCroppedImage" result-image-size="160" area-type="{{cropType}}"></img-crop>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <button class="btn btn-primary" ng-model="cropType" uib-btn-radio="'circle'" ng-show="myBtn1">Circle</button>
                                <button class="btn btn-primary" ng-model="cropType" uib-btn-radio="'square'" ng-show="myBtn2">Square</button>
                                 <button class="btn btn-primary" ng-show="myBtn3" onclick="uploadPic()">Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
function uploadPic()
{
    var a=$("#cropImgs").attr("src");
    $.ajax({
        url:"../script/saveDesign/saveProfile.php?useId=<?php echo $this->session->userdata('user_id');?>",
        type:'POST',
        data:{
            data:a
        },
         complete: function(data, status)
     {
      if(status== "success")
      {
        alert("image save successfully");
        window.location.reload();
       //console.log(data.responseText);
      }     
     }
    })
}

</script>

