'use strict';
app.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});

app.filter('encodeBase64', function(){
   return function(text){
   return btoa(text);
   }
});

app.filter('decodeBase64', function(){
   return function(text){
   return atob(text);
   }
});

app.filter('trust', ['$sce',function($sce) {
  return function(value, type) {
    return $sce.trustAs(type || 'html', value);
  }
}]);

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.filter('startFrom', function() {
    return function(input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.directive("initFromForm", function ($parse) {
    return {
      link: function (scope, element, attrs) {
        var attr = attrs.initFromForm || attrs.ngModel || element.attrs('name'),
        val = attrs.value;
        $parse(attr).assign(scope, val)
      }
    };
  });

app.directive('ngInitial', function() {
  return {
    restrict: 'A',
    controller: [
      '$scope', '$element', '$attrs', '$parse', function($scope, $element, $attrs, $parse) {
        var getter, setter, val;
        val = $attrs.ngInitial || $attrs.value;
        getter = $parse($attrs.ngModel);
        setter = getter.assign;
        setter($scope, val);
      }
    ]
  };
});
angular.module('app').controller('AppCtrl', ['$scope',
    function($scope) {

        var menufold = false; 
        var screenWidth = window.innerWidth;
        if (screenWidth < 767){
            var menufold = true; 
        }

        $scope.app = {
            name: 'Happy Edit- Dashboard',
            version: '3.0.0',
            type: 'general', // general,hospital,university,music,crm,blog,socialmedia,freelancing,ecommerce
            color: {
                primary: '#673AB7',
                accent: '#FF6E40',
                info: '#26C6DA',
                success: '#46be8a',
                warning: '#fdb45d',
                danger: '#F44336',
                secondary: '#a9a9a9',
                text: '#767676'
            },
            settings: {
                menuProfile: true,
                menuFolded: menufold,
                chatFolded: true,
                layoutBoxed: false,
                searchFocus: false,
                pagetitle: 'Slant \\ AngularJS',
            }
        }
        $scope.menuChatToggle = function(type, value) {
            if (type == "menu" && !value) {
                $scope.app.settings.chatFolded = true;
            }
            if (type == "chat" && !value) {
                $scope.app.settings.menuFolded = true;
            }
        }
        $scope.changeMenuHeight = function() {
            //console.log($scope.settings.menuProfile);
            if ($scope.app.settings.menuFolded == true) {
                var navHeight = angular.element("#main-content section.wrapper .content-wrapper").innerHeight() + 90;
            } else {
                var navHeight = $(window).innerHeight() - 60;
            }
            //console.log(navHeight);
            angular.element("#main-menu-wrapper").height(navHeight);
        }
        $scope.$watch('app.settings.menuFolded', function() {
            $scope.changeMenuHeight();
        });
        $scope.$on('$viewContentLoaded', function(next, current) {
            angular.element(document).ready(function() {
                $scope.changeMenuHeight();
            });
        });
        $scope.ElementInView = function(inview, event, addclass, removeclass) {
            var id = event.inViewTarget.id;
            /*console.log(event);  */
            if (inview && id != "") {
                if (addclass != "") {
                    $("#" + id).addClass(addclass);
                } else {
                    $("#" + id).removeClass(removeclass);
                }
            }
            return false;
        }
        $scope.testLines = [];
        for (var i = 20; i >= 0; i--) {
            $scope.testLines.push(i);
        };
        $scope.lineInView = function(index, inview, inviewpart, event) {
            /*console.log(inview+" "+index+" "+inviewpart+" "+event);    */
            /*console.log(event.inViewTarget.id);  */
            return false;
        }
        $scope.refreshDashboard = function()
        {
          window.location=base_url+"index.php/dashboard";
          //window.location.reload();
        }
    }
]);

app.controller('TrendingImgCtrl',function ($scope, $http,$timeout) {
  $scope.extnameVal = [
    {id:'htmls', name:'HTML'},
    {id:'pngs', name:'PNG'},
    {id:'gifs', name:'GIF'}
  ];
  $scope.myViewsBy = $scope.extnameVal[0];
   $scope.getExtName = function () {
        $http.get(base_url+'index.php/dashboard/getPngImgThumbname/').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 12; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData = $scope.list;
        //$scope.ispurchase = response.is_purchase;
            
      });    

    }
    $scope.openFolderPopup = function(foldername)
    {
      $(".filesPopup").modal('show');
      $scope.displayFiles(foldername);
      $scope.foldernames =foldername;

      var fcookie='myProjects';
      document.cookie=fcookie+"=" + foldername;
    }
    $scope.removePoups = function()
    {
      $(".filesPopup").modal('hide');
    }
    $scope.displayFiles = function (foldernames) {
     //alert("call this function");
      $http.get(base_url+'index.php/dashboard/getPngImgFileThumbname/'+foldernames).success(function(response) {//$scope.userdata = response;
        $scope.list2 = response;
        $scope.currentPage2 = 1; //current page
        $scope.entryLimit2 = 12; //max no of items to display in a page
        $scope.filteredItems2 = $scope.list2.length; //Initially for no filter  
        $scope.totalItems = $scope.list2.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData2 = $scope.list2;
            
      });
//console.log(foldernames);
      // $.ajax({
      //     type:"GET",
      //     url:base_url+'index.php/dashboard/getPngImgFileThumbname/',
      //     data:"foldernames="+foldernames,
      //     success:function(results)
      //     {
      //       $scope.list2 = results;
      //     }
      // });
    }
    $scope.chnageViewLook = function (valName) {
      $http.get(base_url+'index.php/dashboard/getPngImgThumbname/').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 12; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.itemsPerPage = $scope.entryLimit;
        $scope.noData = $scope.list;
            
      });
    }    //$scope.spin = false;
// $http.get(base_url+'index.php/dashboard/getPngImgThroughId').success(function(response) {//$scope.userdata = response;
//         $scope.list = response;
//         $scope.currentPage = 1; //current page
//         $scope.entryLimit = 8; //max no of items to display in a page
//         $scope.filteredItems = $scope.list.length; //Initially for no filter  
//         $scope.totalItems = $scope.list.length;
//         $scope.itemsPerPage = $scope.entryLimit;
//         $scope.noData = $scope.list;
            
//     });

   $scope.deleteProject = function(proname)
   {
        var confirms=confirm("Do you want to delete this project ?");
        if(confirms==true)
        {
           $http.get(base_url+'index.php/dashboard/deleteproject/'+proname).success(function(response) {//$scope.userdata = response;
            window.location.reload();
          });
        }
        else{}   
   }
   
    
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
    $scope.generatePdf = function(p_id,projectName) {
        //alert(p_id);
        $scope.spin = true;
        $http.get(base_url+'index.php/dashboard/pdf/'+p_id+'/'+projectName).success(function(response) {//$scope.userdata = response;
             $scope.spin = false;
             $scope.createPdfbuttonId = p_id;
             $scope.stripForPdf = true;
             $scope.pdfinfo = response;
             $scope.dwnloadLink = $scope.pdfinfo['link'];
             //alert(response);
            setTimeout(function(){ location.reload(); }, 5000);
            });
    };
    $scope.addTocart = function(id){
      $scope.addTocartData = {
      'pName' : $("#pName"+id).val(),
      'pId' : id,
      'pPrice' : $("#pPrice").val()
    };  
    $http({
          method  : 'POST',
          url     : base_url+'index.php/dashboard/add',
          data    : $scope.addTocartData, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            // $scope.
             // console.log(data); 
             $('.addToCartPopup').modal('show');
             setTimeout(function(){$('.addToCartPopup').modal('hide');window.location.reload();},2000);
            
          });    
      }
      $scope.refreshMyCart = function()
      {
        window.location.reload();
      }
      // Relaod page 
      $scope.reloadCartPage = function(){
          if ($("#addTocartReload").val() == 1) {
              location.reload();
           };
      }
  });

app.controller('pdfDesignList',function ($scope, $http,$timeout) {
$http.get(base_url+'index.php/dashboard/getPdfStatuslist').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 8; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.noData = $scope.list;

    });

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() {
            $scope.filteredItems = $scope.filtered.length;
        }, 5);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
  });

app.controller('transactionList',function ($scope, $http,$timeout) {
$http.get(base_url+'index.php/dashboard/getTransactionList').success(function(response) {//$scope.userdata = response;
        $scope.list = response;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 8; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
        $scope.noData = $scope.list;
    });

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() {
            $scope.filteredItems = $scope.filtered.length;
        }, 5);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
  });

$('.updateUserdata').on('click', function () {
            alert("submited");
      });

app.controller('logout', function($scope, $http) {
$scope.logoutHomeUrl = function(url){
      $http.get(base_url+'index.php/login/logout').success(function(data) {
          window.location = "http://"+url;
      });   
    }
});

app.controller('loginUserInformation', function($scope, $http) {
  $scope.myImage='';
    $scope.myCroppedImage='';
    $scope.cropType="circle";
    $scope.myBtn1=false;
    $scope.myBtn2=false;
    $scope.myBtn3=false;
    $scope.myDefaultPic=true;
    $scope.myCropPic=false;

    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
          $scope.myBtn1=true;
          $scope.myBtn2=true;
          $scope.myBtn3=true;
          $scope.myDefaultPic=false;
          $scope.myCropPic=true;
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

$scope.getUserInformation = function(user_id){
      $http.get(base_url+'index.php/userprofile/getUserInformation/'+user_id).success(function(response) {
            $scope.userData = response;
          });
    }
});

app.controller('getdomainsearchlist', function($scope, $http) {
    $scope.domainname = {};
    $scope.domainRegisterInfo = {};
    $scope.spinloader = false;
    $scope.domainList = false;

$scope.getUserInformation = function(user_id){

      $http.post(base_url+'index.php/dashboard/getDomainInformation').success(function(response) {
            $scope.domainData = response;
          });
    }
$scope.senddomainrequest = function  () {
  if($("[type=checkbox]:checked").length==0)
  {
    alert("Please select at least one domain");
  }
  else
  {
    $scope.domainname = {
      'formData' : $("#domainName").val(),
      'ext' : $("#ext").val()
    };
   $scope.extArray = [];
   $scope.priceData = [];
   $scope.dmaintxt = $("#domainName").val();
   $scope.Users = [{ext:'gr',price:'100'},{ext:'in',price:'200'},{ext:'com',price:'400'}];
   $scope.Employments = $("#ext").val();
   $scope.doaminStatus = checkDomain2($scope.dmaintxt);
   if ($scope.doaminStatus) {
    $scope.spinloader = true;
    $http({
          method  : 'POST',
          url     : base_url+'index.php/dashboard/getDomainInformation',
          data    : $scope.domainname, //forms user object
          headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
         })
          .success(function(data) {
            // for(var i=0;i<data.arrayAvDomains.length;i++){
            //   // $(".domainck"+i).attr("disabled","disabled");
            //   // document.getElementById(i).disabled = true;
            //   for (var j = 0; j < data.arrayAvDomains.length; j++) {
            //         if ($scope.Users[j].ext == data.arrayAvDomains[i].split('.').pop()) {
            //              // $scope.priceData.push($scope.Users[j].price);
            //              // console.log(data.arrayAvDomains[i].split('.').pop());
            //             }
            //             // else{

            //             // }
            //   $scope.priceData.push($scope.Users[j].price);
                  
            //         }
            //   $scope.extArray.push(data.arrayAvDomains[i].split('.').pop());

            // }
            $scope.priceData;
            $scope.extArray;  
              
            $scope.jsonData = data;  
            $scope.spinloader = false;
            $scope.domainList = true;
          });
   }else{
    console.log('false');

   }
   }
}  
// Get price for each domain
$scope.chkPriceVal = function(domianName,id) {
   $http.get(base_url+'index.php/dashboard/getPriceValWidDomainExt/'+domianName).success(function(response) {
        $scope.domainExtPrice = response;
        $("#domainprice"+id).attr("class","label label-default priceTag").text($scope.domainExtPrice['price']);
        $(".domainck"+id).removeAttr("disabled");
        
     });
}

// ENd - Get price for each domain
// Code for create chek list ----
 $scope.roles = [
    '.gr', 
    '.eu', 
    '.com.gr', 
    '.org.gr',
    '.net.gr',
    '.gov.gr',
    '.com',
    '.net',
    '.org',
    '.info',
    '.mobi',
    '.name',
    '.ac',
    '.cc','.io','.sh','.tv','.bz','.ws','.ms','.gs','.in','.fm','.la'
  ];
  $scope.user = {
    roles: [".gr"]
  };
   // console.log($scope.user.roles);
  $scope.checkAll = function() {
    $scope.user.roles = angular.copy($scope.roles);
  };
  $scope.uncheckAll = function() {
    $scope.user.roles = [];
  };
  $scope.checkFirst = function() {
    $scope.user.roles.splice(0, $scope.user.roles.length); 
    $scope.user.roles.push('.gr');
  };

// End of chk list.....

// Function for register domian ..
$scope.addDomainTocart = function  () {
  $scope.chkDomainlist = domainSelectionProcess();
  if ($scope.chkDomainlist) {
    //$scope.domainNameVal = $(".chkBoxval:checked").val();
            var allVals = [];
            var i;
            $scope.domainInformation = {};
            $(".chkBoxval:checked").each(function () {
              var domainPriceVal = $(this).parent().find("span").text();
                allVals.push({pName:$(this).val(), pPrice:domainPriceVal,pId:$(this).val()});
            });
             //for(i=0;i<allVals.length;i++){
                var jsonString = JSON.stringify(allVals);
            // console.log(jsonString);return false;
               $http({
                  method  : 'POST',
                  dataType:'json',
                 url     : base_url+'index.php/dashboard/addDomain/',
                 data:{'myData':jsonString}
                 }).success(function(data) {
                    // alert(data);
                      if (data = '100') {
                          $scope.domainMsg = 'You have added - '+allVals.length+'items into your cart'; 
                             $scope.spinloader2 = false;    
                             location.reload();              
                      }
                  });  
                  //}
              // alert('Thanks!');    
            }
}



// End of register domain 
});

 

function checkDomain2(nname)
{

var arr = new Array(
'.com','.net','.org','.biz','.coop','.info','.museum','.name', '.pro','.edu','.gov','.int','.mil','.ac','.ad','.ae','.af','.ag', '.ai','.al','.am','.an','.ao','.aq','.ar','.as','.at','.au','.aw', '.az','.ba','.bb','.bd','.be','.bf','.bg','.bh','.bi','.bj','.bm', '.bn','.bo','.br','.bs','.bt','.bv','.bw','.by','.bz','.ca','.cc', '.cd','.cf','.cg','.ch','.ci','.ck','.cl','.cm','.cn','.co','.cr', '.cu','.cv','.cx','.cy','.cz','.de','.dj','.dk','.dm','.do','.dz', '.ec','.ee','.eg','.eh','.er','.es','.et','.fi','.fj','.fk','.fm', '.fo','.fr','.ga','.gd','.ge','.gf','.gg','.gh','.gi','.gl','.gm', '.gn','.gp','.gq','.gr','.gs','.gt','.gu','.gv','.gy','.hk','.hm', '.hn','.hr','.ht','.hu','.id','.ie','.il','.im','.in','.io','.iq', '.ir','.is','.it','.je','.jm','.jo','.jp','.ke','.kg','.kh','.ki', '.km','.kn','.kp','.kr','.kw','.ky','.kz','.la','.lb','.lc','.li', '.lk','.lr','.ls','.lt','.lu','.lv','.ly','.ma','.mc','.md','.mg', '.mh','.mk','.ml','.mm','.mn','.mo','.mp','.mq','.mr','.ms','.mt', '.mu','.mv','.mw','.mx','.my','.mz','.na','.nc','.ne','.nf','.ng', '.ni','.nl','.no','.np','.nr','.nu','.nz','.om','.pa','.pe','.pf', '.pg','.ph','.pk','.pl','.pm','.pn','.pr','.ps','.pt','.pw','.py', '.qa','.re','.ro','.rw','.ru','.sa','.sb','.sc','.sd','.se','.sg', '.sh','.si','.sj','.sk','.sl','.sm','.sn','.so','.sr','.st','.sv', '.sy','.sz','.tc','.td','.tf','.tg','.th','.tj','.tk','.tm','.tn', '.to','.tp','.tr','.tt','.tv','.tw','.tz','.ua','.ug','.uk','.um', '.us','.uy','.uz','.va','.vc','.ve','.vg','.vi','.vn','.vu','.ws', '.wf','.ye','.yt','.yu','.za','.zm','.zw');
var mai = nname;
var val = true;
var dname = mai;
if(dname.length>2 && dname.length<57)
{
    for(var j=0; j<dname.length; j++)
        {
          var dh = dname.charAt(j);
          var hh = dh.charCodeAt(0);
          if((hh > 47 && hh<59) || (hh > 64 && hh<91) || (hh > 96 && hh<123)  || (hh > 944 && hh <970) || (hh > 912 && hh < 938) || hh==45 )
            {
                 if((j==0 || j==dname.length-1) && hh == 45)    
                    {
                        alert("Invalid Domain Name");
                        return false;
                    }
            }
          else{
                alert("Invalid Domain Name");
                return false;
            }
        }
}
else
{
     alert("Invalid Domain Name");
     return false;
}   
return true;
}

function domainSelectionProcess()
{
  if ($(".chkBoxval:checked").length == 0)
  {
    alert("Select a domain name");
    return false;
  }
  else{
    return true; 
  }
   
}

function TrHide(bool){
    if (bool==true){      
      document.getElementById("ns").style.display = 'block';
    }else if(bool==false) {
      document.getElementById("ns").style.display = 'none';
    }else if(bool==3){
      
    }
  }

  // cart page controller ...
  app.controller('cartPage',function ($scope, $http,$timeout) {
      $scope.deleteSelectedRow = function(rowId){
        // confirm("Press a button!")
        var confirmTxt = '';
        if (rowId =='all') {
          confirmTxt = confirm("Are you sure you want to delete all item ?");
        }else{
          confirmTxt = confirm("Are you sure you want to delete this item ?");

        }
        if (confirmTxt == true) {
            // alert(rowId);
            $http.post(base_url+'index.php/dashboard/remove/'+rowId).success(function(response) {
              console.log(response);
              if (response == 100) {
                location.reload();
              }
          });
        };
      }
  });

  // Order page
  // cart page controller ...
  app.controller('orderPage',function ($scope, $http,$timeout) {
    $scope.domainRegisterInfo = {};
    $scope.orderInit = function(user_id){
      $scope.submiteUserdata = function  () {
          // console.log($scope.domainRegisterInfo.fullname);
          // alert($("#fullaname").val());
          $scope.domainRegisterInfo = {
            'fullname': $("#fullaname").val(),
            'firstname': $("#firstname").val(),
            'lastname': $("#lastname").val(),
            'titlos': $scope.domainRegisterInfo.titlos,
            'emailText': $("#emailText").val(),
            'address1': $scope.domainRegisterInfo.address1,
            'stateProvince': $scope.domainRegisterInfo.stateProvince,
            'city': $scope.domainRegisterInfo.city,
            'postcode': $scope.domainRegisterInfo.postcode,
            'country': $scope.domainRegisterInfo.country,
            'phoneNum': $scope.domainRegisterInfo.phoneNum,
          };
      }
      $scope.piraeusBankApi = function () {
         window.location = 'https://paycenter.piraeusbank.gr/redirection/pay.aspx';
        // window.location = "https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx";
        //alert("hello");
       /* var AcquirerID=14,MerchantID=2143781356,PosID=2138523534,UserName="HA668974",Passwords="TI122345",MerchantReference="Payment Successfully Done",RequestType="02";
        var ExpirePreauth=0,Amount=10.00,CurrencyCode=978,Installments=0,Bnpl="0";
         
          $.ajax({  
            type: "POST",
            dataType: "json",
            url : base_url+"index.php/dashboard/ticketsRequests",
            data: {"AcquirerID":AcquirerID,"MerchantID":MerchantID,"PosID":PosID,"UserName":UserName,"Passwords":Passwords,"MerchantReference":MerchantReference,"RequestType":RequestType,"ExpirePreauth":ExpirePreauth,"Amount":Amount,"CurrencyCode":CurrencyCode,"Installments":Installments,"Bnpl":Bnpl},
        }).success(function (json) 
        {    
          //$(".content-body").load(base_url+'dashboard/partials/payment_success_form.php');
          if(json.status==0)
          {
            //window.location="http://aish.happyedit.com/index.php/dashboard/payment_success_form";
            //document.write(json);
          }
        });*/
     } 
     $scope.getUserInformation = function(){
      $http.get(base_url+'index.php/userprofile/getUserInformation/'+user_id).success(function(response) {
            $scope.userData = response;
          });
    }
    $scope.getUserInformation();
  }  

  });
  // End Order page

