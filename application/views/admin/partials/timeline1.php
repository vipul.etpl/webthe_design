<ol class="breadcrumb newcrumb">
    <li>
        <a href="#">
            <span><i class="fa fontello-home-outline"></i>
            </span>Home</a>
    </li>
    <li><a href="#">Dashboard</a>
    </li>
    <li>Users</li>
</ol>

<div class="row">
    <!--   TIMELINE -->
    <div class="col-lg-12" ng-controller="UserList">

        <div class="box bg-transparent">
            <div class="box-body no-padding">
                <ul class="timeline">


                    <!-- timeline item -->
                    <li ng-repeat="data in list">
                        <i class="fa fontello-user-1 bg-yellow"></i>
                        <div class="timeline-item bg-white">
                            <h3 style="padding: 15px 20px 0 20px;" class="timeline-header text-white  no-border">
                                <span class="text-gray"><img src="<?php echo base_url(); ?>saveImg/profile.png">&nbsp;&nbsp;{{data.first_name}} {{data.last_name}}</span>
                                <span class="text-white pull-right fa fa-cogs"></span>
                            </h3>

                            <div class="timeline-body">
                                <p><b>Email: </b>{{data.email}}</p>
                                <p><b>Username: </b>{{data.username}}</p>
                                <p><b>Registered Date: </b>{{data.register_date}}</p>
                            </div>
                            <div class='timeline-footer'>
                                <a class="btn btn-danger btn-xs" ng-click="deleteThisUser({{data.user_id}})"><i class="fa fontello-user-delete"></i>&nbsp;Delete</a>
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                </ul>
            </div>
        </div>
    </div>
    <!--   ./TIMELINE -->
