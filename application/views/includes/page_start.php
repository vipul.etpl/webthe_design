<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title; ?></title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width"> 
        <link rel="shortcut icon" href="<?php echo base_url()?>img/Fevicon.png" />
        <link href="<?=base_url();?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url();?>css/bootstrap.css" rel="stylesheet">
        <link href="<?=base_url();?>css/font-awesome.css" rel="stylesheet">
        <link href="<?=base_url();?>css/font-awesome.min.css" rel="stylesheet">
        <link href="<?=base_url();?>css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="<?=base_url();?>css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">                                                                              
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
        <script src="<?=base_url();?>js/profile.js"></script>
        <script src ="<?=base_url();?>js/profile.js"></script>
        
        <script src ="<?=base_url();?>js/autocomplete/jquery.js"></script>
        <script src ="<?=base_url();?>js/autocomplete/jquery1.js"></script>
        <script src ="<?=base_url();?>js/autocomplete/jquery.watermarkinput.js"></script>
        <script src ="<?=base_url();?>js/autocomplete/jquery.autocomplete.js"></script>
        <script src ="<?=base_url();?>js/password.js"></script>
        <script src ="<?=base_url();?>js/main_script.js"></script>
        
        
    </head>                
    <body style="background-color:white">
    <!--Header for all-->
    <nav class="navbar navbar-default" role="navigation" style="margin-bottom:0px;">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <img class="img-responsive" src="<?= base_url('img/logo.png'); ?> "> -->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <!--  <form class="navbar-form navbar-left" role="search" style="padding-left:30px">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" style="background-color:#deb200">
        </div>
      </form> -->
      <ul class="nav navbar-nav navbar-right" style="padding-right:70px">
      <li><a href="#" class="glyphicon glyphicon-user"></a></li>
        <li><a href="#"><?php echo $this->session->userdata('name'); ?></a></li>

        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!--////Header for all-->
       