<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="image-avatar image">
            <img src="http://api.randomuser.me/portraits/thumb/men/22.jpg" class="img-circle" alt="User Image" />
            <span><i class="fa fa-circle text-success"></i>
            </span>
        </div>
        <div class="info">
            <p class="text-center">
                <span>Hello,
                    <strong>Dave</strong>Mattew</span>
            </p>
            <ul class="profile-list">
                <li>
                    <a href="#nowhere" data-toggle="tooltip" data-placement="top" title="Profile">
                        <span data-toggle="modal" data-target="#myModal"><i class="fontello-user-outline"></i>
                        </span>
                    </a>

                </li>
                <li>
                    <a href="#nowhere" data-toggle="tooltip" data-placement="top" title="Setting">
                        <span data-toggle="modal" data-target="#setting"><i class="fontello-wrench-outline"></i>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" ng-click="logout()" data-toggle="tooltip" data-placement="top" title="Logout">
                        <span><i class="fontello-power-outline"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- SIDEBAR MENU: : style can be found in sidebar.less -->
    <ul id="menu" class="sidebar-menu">
        <li class="active">
            <a href="#!/dashboard" ng-class="{activeSmall:part == 'dashboard'}">
                <i class="fa fontello-desktop-1"></i> 
                <span>Dashboard</span>
            </a>
        </li>
        <!-- <li>
            <a href="#!/mail" ng-class="{activeSmall:part == 'mail'}">
                <i class="fa fontello-mail-1"></i> 
                <span>Mailbox</span>
                <small class="badge pull-right bg-yellow text-white">12</small>
            </a>
        </li> -->

        <li class="treeview">
            <a href="#!/">
                <i class="fa fontello-inbox"></i> 
                <span class="hide-menu">Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="#!/shop" ng-class="{activeSmall:part == 'shop'}">All Project</a>
                </li>
                <li><a href="#!/best_design" ng-class="{activeSmall:part == 'best_design'}">Best Design</a>
                </li>
                <li><a href="#!/generalelement" ng-class="{activeSmall:part == 'generalelement'}">Project Cost</a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fontello-user-1" style="color:#0e6166"></i>
                <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="#!/timeline" ng-class="{activeSmall:part == 'timeline'}">User List</a>
                </li>
            </ul>
        </li>
        
    </ul>
   


</section>
<!-- /.sidebar -->

<script>
/* Sidebar tree view */
(function($) {
    $(".sidebar .treeview").tree();
})(jQuery);

$('#menu').slicknav();
//Activate tooltips
$("[data-toggle='tooltip']").tooltip();

function fix_sidebar() {
    //Make sure the body tag has the .fixed class
    if (!$("body").hasClass("fixed")) {
        return;
    }

    //Add slimscroll
    $(".sidebar").slimscroll({
        height: ($(window).height() - $(".header").height()) + "px",
        color: "rgba(0,0,0,0.3)"
    });
}
</script>
