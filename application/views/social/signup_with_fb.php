<div id="fb-root"></div>   
<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1551627335053085',
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  }, {scope: 'email'});

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {  
           FB.api('/me', function(response) {
              /*console.log('Good to see you, ' + response.name + '.');   */
              //alert(response.name + " " + response.id + " " + response.first_name + " " + response.last_name + " " + response.username + " " + response.email);
              //alert(response.toSource());
             // do_logout();                            
              
            });
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
       do_login();  
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
     do_login();
    }
  }, {scope: 'email'});
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));
  
  // Custome Function
  function do_login(){
    FB.login(function(r){
        if (r.status === 'connected') {  
           window.location.href = 'signup/facebook';   
        } else if (r.status === 'not_authorized') {                                               
           do_login();  
        } else { 
         do_login();
        }
        }, {scope: 'email'});  
  }
  
  function do_logout() {
    FB.logout(function (response) {
        //window.location.reload();
    });
 }
      
 </script> 